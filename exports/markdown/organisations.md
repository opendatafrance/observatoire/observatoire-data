# Odservatoire - Organisations

*897 organisations recensées dont:*
- *65 autres groupements de collectivités territoriales*
- *101 communautés d'agglomération*
- *53 communautés de communes*
- *458 communes*
- *6 communautés urbaines*
- *61 départements*
- *34 délégataires de service public*
- *3 établissements publics territoriaux*
- *20 métropoles*
- *79 organismes associés de collectivité territoriale*
- *16 régions*
- *1 SDE*

## ACIGNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?sort=modified&refine.publisher=Commune+d%27Acigné
- Volumétrie : 4 jeux de données
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 22/05/2019 à 15h58
- Date de dernière mise à jour le 16/03/2020 à 10h59

## ADACL Landes

*Autre groupement de collectivités territoriales*
## ADEUPa

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://opendata.brest-metropole.fr/explore/?sort=modified&refine.publisher=ADEUPa
- Volumétrie : 12 jeux de données

## ADICO SMI OISE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://opendata.adico.fr/fr/dataset?organization=adico
- Volumétrie : 1 jeux de données

## AGEN

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agen.fr/explore/?refine.publisher=Mairie+d%27Agen
- Volumétrie : 1 jeux de données
- Date de création : le 04/09/2017 à 12h02
- Date de dernière mise à jour le 29/01/2018 à 14h05


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bec29278b4c412a07c7ee5f/
- Volumétrie : 95 jeux de données et 271 ressources
- Nombre de vues (tous jeux de données confondus) : 135
- Nombre de téléchargements (toutes ressources confondues) : 51
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `a`
- Types de licences utilisées : `License Not Specified`, `Other (Public Domain)`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/11/2018 à 16h55
- Date de dernière mise à jour le 09/12/2022 à 01h13

## AGENCE DE DEV. TOURISTIQUE DE L'ARDECHE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://opendata.ardeche.fr/group/agence-de-développement-touristique-de-lardèche
- Volumétrie : 3 jeux de données

## AGENCE DE TOURISME DE LA CORSE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://www.data.corsica/explore/?refine.publisher=Agence+de+Tourisme+de+la+Corse
- Volumétrie : 14 jeux de données
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 12/06/2017 à 12h51
- Date de dernière mise à jour le 06/02/2020 à 10h33

## AGENCE DES ESPACES VERTS ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?sort=modified&refine.publisher=Agence+des+espaces+verts+d%27%C3%8Ele+de+France+(AEV)
- Volumétrie : 23 jeux de données et 17251 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 79243
- Types de licences utilisées : `ODbL`, `Licence Ouverte v2.0 (Etalab)`, `Licence ouverte (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 10/06/2013 à 07h54
- Date de dernière mise à jour le 02/12/2022 à 08h51

## AGENCE PUBLIQUE DE GESTION LOCALE PYR. ATLANTIQUES

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c94a888634f415e8c85de26/
- Volumétrie : 1 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/01/2020 à 11h47
- Date de dernière mise à jour le 14/02/2022 à 14h49

## AHAXE-ALCIETTE-BASCASSAN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/ahaxe-alciette-bascassan/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `geojson`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/09/2020 à 11h42
- Date de dernière mise à jour le 07/09/2020 à 11h44

## AIGLUN

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/commune-daiglun-04
- Volumétrie : 4 jeux de données

## AIGUILLES

*Commune*
## AIN

*Département*

### Source **Plateforme territoriale**
- URL : http://departement-ain.opendata.arcgis.com/datasets?source=D%C3%A9partement%20de%20l%27Ain
- Volumétrie : 39 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5851009688ee3870fbc65bb3
- Volumétrie : 223 jeux de données et 1352 ressources
- Nombre de vues (tous jeux de données confondus) : 118
- Nombre de téléchargements (toutes ressources confondues) : 23
- Types de ressources disponibles : `arcgis geoservices rest api`, `web page`, `zip`, `kml`, `geojson`, `csv`, `ogc wms`, `ogc wfs`, `json`, `shp`, `document`, `image`
- Types de licences utilisées : `License Not Specified`, `Other (Public Domain)`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 28/02/2017 à 11h30
- Date de dernière mise à jour le 20/08/2022 à 02h03

## AINCILE

*Commune*
## AINHICE MONGELOS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5f340b1cfbebbe5c7c80d24e/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 13/08/2020 à 11h12
- Date de dernière mise à jour le 13/08/2020 à 11h17

## AINHOA

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/commune-dainhoa/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/10/2019 à 17h19
- Date de dernière mise à jour le 31/07/2020 à 15h16

## AIR BREIZH

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://data.airbreizh.asso.fr/geonetwork/srv/fre/catalog.search#/search?facet.q=type%2Fdataset
- Volumétrie : 29 jeux de données

## AIR PAYS DE LA LOIRE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data.airpl.org/geonetwork/srv/fre/catalog.search#/search?facet.q=type%2Fdataset
- Volumétrie : 28 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff53a3a7292c64a77cd7/
- Volumétrie : 10 jeux de données et 29 ressources
- Nombre de vues (tous jeux de données confondus) : 19
- Nombre de téléchargements (toutes ressources confondues) : 147
- Types de ressources disponibles : `csv`, `xls`, `json`, `xml`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 04h13
- Date de dernière mise à jour le 10/12/2013 à 14h49

## AIRE SUR L'ADOUR

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b9a1371634f417f35dc4a7c/
- Volumétrie : 1 jeux de données et 48 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `zip`, `json`, `shp`, `dbf`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/08/2020 à 06h38
- Date de dernière mise à jour le 14/08/2020 à 06h38

## AIRPARIF

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-airparif-asso.opendata.arcgis.com/datasets
- Volumétrie : 161 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a4381adc751df74bae4627d/
- Volumétrie : 14 jeux de données et 34 ressources
- Nombre de vues (tous jeux de données confondus) : 289
- Nombre de téléchargements (toutes ressources confondues) : 318
- Types de ressources disponibles : `xml`, `geojson`, `pdf`, `json`, `wmts`, `web page`, `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 29/12/2017 à 15h33
- Date de dernière mise à jour le 28/03/2019 à 14h28

## AIX EN PROVENCE

*Commune*

### Source **OpenDataSoft**
- URL : https://aix-en-provence.opendatasoft.com/explore/?sort=modified&refine.publisher=Ville+d%27Aix+en+Provence
- Volumétrie : 2 jeux de données
- Types de licences utilisées : `Domaine public`
- Date de création : le 28/03/2019 à 08h59
- Date de dernière mise à jour le 08/01/2023 à 01h01

## AIX LES BAINS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab9e75188ee387964e77c2b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 27/03/2018 à 08h58
- Date de dernière mise à jour le 27/03/2018 à 08h58

## AIXE SUR VIENNE

*Commune*
## ALBI

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.mairie-albi.fr/dataset?q=
- Volumétrie : 24 jeux de données

## ALLAIRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1d2901c751df5e50300f1c
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/11/2017 à 12h19
- Date de dernière mise à jour le 23/08/2018 à 16h49

## ALPES DE HAUTE PROVENCE

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-des-alpes-de-haute-provence
- Volumétrie : 34 jeux de données

## ALPES MARITIMES

*Département*

### Source **Plateforme territoriale**
- URL : https://data.departement06.fr
- Volumétrie : 24 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e00c7456f444104766e92d6/
- Volumétrie : 31 jeux de données et 89 ressources
- Nombre de vues (tous jeux de données confondus) : 51
- Nombre de téléchargements (toutes ressources confondues) : 91
- Types de ressources disponibles : `xlsx`, `zip`, `shp`, `html`, `csv`, `3dtiles`, `las`, `tab`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/01/2020 à 14h35
- Date de dernière mise à jour le 12/09/2022 à 00h00

## ALPI 40 LANDE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset?sort=score+desc%2C+metadata_modified+desc&organization=agence_landaise_pour_l_informatique&q=alpi&ext_bbox=&ext_prev_extent=-6.0205078125%2C41.77131167976407%2C6.8115234375%2C47.989921667414194
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59b2610dc751df4ea9075aea/
- Volumétrie : 4 jeux de données et 20 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xml`, `csv`, `site internet`, `pdf`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 10/04/2019 à 10h23
- Date de dernière mise à jour le 23/12/2022 à 01h09

## ANDUZE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c49b5498b4c4139d1714d76/
- Volumétrie : 4 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xml`, `xlsx`, `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 29/01/2019 à 12h01
- Date de dernière mise à jour le 22/01/2020 à 11h50

## ANGERS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.angers.fr/explore/?refine.publisher=Ville+d%27Angers
- Volumétrie : 74 jeux de données
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/07/2016 à 08h55
- Date de dernière mise à jour le 09/01/2023 à 09h31

## ANGERS LOIRE METROPOLE

*Communauté urbaine*

### Source **OpenDataSoft**
- URL : https://data.angers.fr/explore/?refine.publisher=Angers+Loire+M%C3%A9tropole
- Volumétrie : 38 jeux de données
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 21/07/2016 à 13h03
- Date de dernière mise à jour le 12/01/2023 à 07h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/538346d6a3a72906c7ec5c36
- Volumétrie : 144 jeux de données et 756 ressources
- Nombre de vues (tous jeux de données confondus) : 778
- Nombre de téléchargements (toutes ressources confondues) : 229
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `gtfs`, `gtfs-rt`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/03/2018 à 08h27
- Date de dernière mise à jour le 06/12/2022 à 05h02

## ANGLET

*Commune*

### Source **Plateforme territoriale**
- URL : https://anglet-opendatapaysbasque.opendatasoft.com/explore/?sort=modified
- Volumétrie : 19 jeux de données

## ANNECY

*Commune*
## ANTIBES JUAN LES PINS

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-dantibes-juan-les-pins
- Volumétrie : 107 jeux de données

## APUR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.apur.org/datasets
- Volumétrie : 99 jeux de données

## ARCADE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/arcade
- Volumétrie : 4 jeux de données

## ARCADI ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=Arcadi
- Volumétrie : 10 jeux de données et 2377 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 74868
- Types de licences utilisées : `ODbL`, `Licence ouverte (Etalab)`
- Date de création : le 27/01/2014 à 09h34
- Date de dernière mise à jour le 12/10/2019 à 06h42

## ARDECHE

*Département*

### Source **Plateforme territoriale**
- URL : https://opendata.ardeche.fr/search/type/dataset
- Volumétrie : 17 jeux de données

## ARDTA

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider%5B%5D=Agence+r%C3%A9gionale+de+d%C3%A9veloppement+des+territoires+d%27Auvergne+%28ARDTA%29
- Volumétrie : 14 jeux de données

## ARGENTEUIL

*Commune*
## ARHANSUS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/arhansus/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/03/2020 à 10h31
- Date de dernière mise à jour le 09/03/2020 à 10h35

## ARIEGE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a90060ec751df1402f350d8/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 05/01/2023 à 09h54
- Date de dernière mise à jour le 05/01/2023 à 09h54

## ARL PACA

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/agence-regionale-du-livre-provence-alpes-cote-dazur
- Volumétrie : 4 jeux de données

## ARMENTIERES

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+d%27ARMENTIERES
- Volumétrie : aucun jeu de données

## ARVIEUX

*Commune*
## ASCARAT

*Commune*
## ASNIERES SUR SEINE

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?refine.publisher=Ville+d%27Asnières-sur-Seine
- Volumétrie : 5 jeux de données et 146 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 7249
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 20/08/2018 à 09h25
- Date de dernière mise à jour le 16/03/2022 à 10h13


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a61b7a7c751df3fd36ac81a
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 16/11/2020 à 16h59
- Date de dernière mise à jour le 16/11/2020 à 17h05

## ASPREMONT(05)

*Commune*
## ASPRES-LES-CORPS

*Commune*
## ASPRES-SUR-BUECH

*Commune*
## ATD 36 INDRE

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aa95464c751df7c5ded72f6/
- Volumétrie : 4 jeux de données et 30 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 01/06/2018 à 17h57
- Date de dernière mise à jour le 12/04/2022 à 14h59

## ATD DE LA CHARENTE

*Organisme associé de collectivité territoriale*
## ATD DE LA DORDOGNE

*Organisme associé de collectivité territoriale*
## ATMO AUVERGNE RHONE ALPES

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmoaura.opendata.arcgis.com/datasets
- Volumétrie : 203 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59a538b688ee386b121578b9/
- Volumétrie : 6 jeux de données et 30 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `zip`, `json`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 09/12/2020 à 06h38
- Date de dernière mise à jour le 10/12/2020 à 06h38

## ATMO BOURGOGNE FRANCHE COMTE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.atmo-bfc.org/opendata
- Volumétrie : 20 jeux de données

## ATMO GRAND EST

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmograndest.opendata.arcgis.com/datasets
- Volumétrie : 142 jeux de données

## ATMO GUYANE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-guyane.opendata.arcgis.com/datasets
- Volumétrie : 22 jeux de données

## ATMO HAUTS DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-hdf.opendata.arcgis.com/datasets
- Volumétrie : 57 jeux de données

## ATMO NORMANDIE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://datas-atmonormandie.opendata.arcgis.com/datasets
- Volumétrie : 59 jeux de données

## ATMO NOUVELLE AQUITAINE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-na.opendata.arcgis.com/datasets
- Volumétrie : 349 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff53a3a7292c64a77cd9/
- Volumétrie : 7 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 36
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 04h57
- Date de dernière mise à jour le 18/09/2013 à 08h47

## ATMO OCCITANIE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-occitanie.opendata.arcgis.com/datasets
- Volumétrie : 38 jeux de données

## ATMO REUNION

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmoreunion.opendata.arcgis.com/datasets
- Volumétrie : 15 jeux de données

## ATMO SUD

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/atmosud
- Volumétrie : 16 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b9fb3678b4c416ff2277bb5/
- Volumétrie : 19 jeux de données et 171 ressources
- Nombre de vues (tous jeux de données confondus) : 43
- Nombre de téléchargements (toutes ressources confondues) : 36
- Types de ressources disponibles : `geojson`, `csv`, `wfs`, `pdf`, `shp`, `zip`, `geotiff`, `wms`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/06/2019 à 10h01
- Date de dernière mise à jour le 16/11/2022 à 00h00

## ATOUMOD

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cc844dd634f41078d8e2c8e/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 17
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 11/07/2022 à 11h46
- Date de dernière mise à jour le 23/11/2022 à 14h54

## AUBE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a216283c751df6333108428/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 13/12/2017 à 16h12
- Date de dernière mise à jour le 30/06/2021 à 15h07

## AUBE EN CHAMPAGNE TOURISME

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54880126c751df081fa3fc15/
- Volumétrie : 1 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 19
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 10/12/2014 à 10h05
- Date de dernière mise à jour le 10/12/2014 à 16h58

## AUBESSAGNE

*Commune*
## AUBIGNAN

*Commune*
## AUCAMVILLE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+d%27Aucamville
- Volumétrie : 4 jeux de données et 19 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 7085
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 23/03/2015 à 13h39
- Date de dernière mise à jour le 15/04/2021 à 15h25

## AUDE

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.aude.fr/explore/?sort=modified&refine.publisher=D%C3%A9partement+de+l%27Aude
- Volumétrie : 28 jeux de données et 12109 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 58940
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 10/07/2020 à 12h50
- Date de dernière mise à jour le 09/01/2023 à 07h43

## AUTERIVE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Commune+d%27Auterive
- Volumétrie : aucun jeu de données

## AUTOLIB VELIB METROPOLE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://opendata.paris.fr/explore/?sort=modified&refine.publisher=Autolib+Velib+M%C3%A9tropole+&refine.publisher=Autolib+Velib+M%C3%A9tropole&disjunctive.theme&disjunctive.publisher&disjunctive.keyword&disjunctive.modified&disjunctive.features
- Volumétrie : 2 jeux de données et 2900 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8464189
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 13/01/2023 à 03h00
- Date de dernière mise à jour le 13/01/2023 à 03h09


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55b10bbcc751df773d10ccc3/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 49
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 08/02/2016 à 16h27
- Date de dernière mise à jour le 08/02/2016 à 16h27

## AUTUN

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb47c2a634f416334276b4f/
- Volumétrie : 4 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 40
- Types de ressources disponibles : `csv`, `xls`, `shx`, `shp`, `prj`, `kml`, `dbf`, `geojson`, `cpg`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 03/10/2018 à 10h28
- Date de dernière mise à jour le 05/10/2018 à 16h08

## AUVERGNE RHONE ALPES

*Région*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=R%C3%A9gion%20Auvergne%20Rh%C3%B4ne-Alpes&from=0
- Volumétrie : 71 jeux de données

## AUVERGNE RHONE ALPES TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider%5B%5D=Comit%C3%A9+R%C3%A9gional+de+D%C3%A9veloppement+touristique+d%E2%80%99Auvergne+%28CRDTA%29
- Volumétrie : 12 jeux de données

## AVIGNON

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/avignon
- Volumétrie : 42 jeux de données

## AYHERRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/ayherre/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `json`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/06/2019 à 15h46
- Date de dernière mise à jour le 05/12/2020 à 18h31

## BAGNERES DE BIGORRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.ha-py.fr/pages/bagneres-de-bigorre/
- Volumétrie : 13 jeux de données

## BAILLARGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/baillargues
- Volumétrie : 26 jeux de données

## BAINS SUR OUST

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1d64a3c751df51a9440eae
- Volumétrie : 9 jeux de données et 18 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `xml`, `pdf`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 28/11/2017 à 14h44
- Date de dernière mise à jour le 25/08/2020 à 09h33

## BALMA

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Balma
- Volumétrie : 4 jeux de données et 140 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 11366
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 14/02/2013 à 23h00
- Date de dernière mise à jour le 15/04/2021 à 15h27

## BANDOL

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-bandol
- Volumétrie : 1 jeux de données

## BAR LE DUC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56b89c46c751df22549f9dea
- Volumétrie : 37 jeux de données et 96 ressources
- Nombre de vues (tous jeux de données confondus) : 16
- Nombre de téléchargements (toutes ressources confondues) : 79
- Types de ressources disponibles : `csv`, `pdf`, `xlsx`, `rss`, `html`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 03/02/2016 à 15h56
- Date de dernière mise à jour le 15/06/2022 à 12h46

## BARAQUEVILLE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59bf94cec751df629f10513d/
- Volumétrie : 1 jeux de données et 23 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `json`, `shp`, `dbf`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/01/2020 à 12h56
- Date de dernière mise à jour le 24/01/2020 à 12h56

## BARATIER

*Commune*
## BARCELONNETTE

*Commune*
## BAS RHIN

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5abcf3c688ee386e18f7f79c/
- Volumétrie : 101 jeux de données et 451 ressources
- Nombre de vues (tous jeux de données confondus) : 122
- Nombre de téléchargements (toutes ressources confondues) : 249
- Types de ressources disponibles : `document`, `json`, `shp`, `zip`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 18/12/2018 à 16h29
- Date de dernière mise à jour le 16/09/2020 à 10h03

## BASSIN DE POMPEY

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://data.bassinpompey.fr/donnees-collectivite
- Volumétrie : 2 jeux de données

## BATZ SUR MER

*Commune*

### Source **OpenDataSoft**
- URL : https://data.paysdelaloire.fr/explore/?disjunctive.diffuseur&disjunctive.gestionnaire&disjunctive.publisher&source=shared&sort=title&refine.publisher=Batz-sur-Mer
- Volumétrie : aucun jeu de données

## BAUDIGNAN

*Commune*
## BAYONNE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.bayonne.fr/information-transversale/open-data-datagouvfr
- Volumétrie : 52 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/datasets/?sort=-created&organization=58d53c4688ee380aeea5e932
- Volumétrie : 64 jeux de données et 78 ressources
- Nombre de vues (tous jeux de données confondus) : 111
- Nombre de téléchargements (toutes ressources confondues) : 122
- Types de ressources disponibles : `csv`, `zip`, `jp2`, `csv/utf8`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 01/10/2018 à 10h18
- Date de dernière mise à jour le 11/01/2023 à 08h53

## BEAULIEU

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/beaulieu
- Volumétrie : 19 jeux de données

## BEAUSSAIS SUR MER

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 283 jeux de données

## BEAUVAIS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/587cebc488ee3805cb9b81a4
- Volumétrie : 3 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 13/03/2017 à 14h09
- Date de dernière mise à jour le 11/01/2023 à 08h49

## BEGANNE

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+B%C3%A9ganne
- Volumétrie : 2 jeux de données et 218 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 18/10/2018 à 18h16
- Date de dernière mise à jour le 07/04/2021 à 07h50

## BEGARD

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 3 jeux de données

## BEGUIOS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/beguios/
- Volumétrie : 2 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `geojson`, `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 15/11/2019 à 12h15
- Date de dernière mise à jour le 10/11/2020 à 15h24

## BERCK SUR MER

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5f06e7cf3a2888d20686160c/
- Volumétrie : 1 jeux de données et 160 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `Other (Public Domain)`
- Date de création : le 09/07/2020 à 16h09
- Date de dernière mise à jour le 09/07/2020 à 16h12

## BERGERAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5dc4192f8b4c41249cc6a390/
- Volumétrie : 3 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `zip`, `pdf`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/11/2019 à 16h03
- Date de dernière mise à jour le 11/03/2020 à 09h59

## BERRY NUMERIQUE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d3f06948b4c410863800078/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 29/07/2019 à 16h49
- Date de dernière mise à jour le 29/11/2022 à 14h56

## BESANCON

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/commune-de-besancon
- Volumétrie : 10 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/539a5b76a3a7293bc272836c/
- Volumétrie : 21 jeux de données et 74 ressources
- Nombre de vues (tous jeux de données confondus) : 23
- Nombre de téléchargements (toutes ressources confondues) : 17
- Types de ressources disponibles : `html`, `xml`, `csv`, `json`, `word`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`, `Other (Public Domain)`
- Date de création : le 07/07/2014 à 09h30
- Date de dernière mise à jour le 11/01/2023 à 08h52

## BESNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Besné
- Volumétrie : 1 jeux de données et 81 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1673
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 16/04/2019 à 13h52
- Date de dernière mise à jour le 19/12/2022 à 08h39

## BLAGNAC

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?sort=modified&refine.publisher=Ville+de+Blagnac
- Volumétrie : 91 jeux de données et 14380 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 120382
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 11/04/2019 à 10h32
- Date de dernière mise à jour le 07/11/2022 à 15h41

## BLOIS

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.blois.agglopolys.fr/pages/accueil/
- Volumétrie : 14 jeux de données

## BOE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/596ca1ecc751df20cc2b38df
- Volumétrie : 14 jeux de données et 114 ressources
- Nombre de vues (tous jeux de données confondus) : 56
- Nombre de téléchargements (toutes ressources confondues) : 36
- Types de ressources disponibles : `json`, `csv`, `xlsx`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 17/07/2017 à 17h33
- Date de dernière mise à jour le 11/01/2023 à 08h52

## BOIS LE ROI

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e7c7ff9dfaefff40aca1f99/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `pdf`, `png`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 26/03/2020 à 11h32
- Date de dernière mise à jour le 26/03/2020 à 12h21

## BOISSY SAINT LEGER

*Commune*

### Source **Plateforme territoriale**
- URL : https://boissy.data4citizen.com/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(boissy)
- Volumétrie : 4 jeux de données

## BONDOUFLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://bondoufle-grandparissud.opendatasoft.com
- Volumétrie : 0 jeux de données

## BONLOC

*Commune*
## BORDEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Ville+de+Bordeaux
- Volumétrie : 85 jeux de données et 509682 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 51665
- Types de licences utilisées : `Licence Ouverte`
- Date de création : le 01/01/2010 à 11h31
- Date de dernière mise à jour le 13/01/2023 à 03h03

## BORDEAUX METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Bordeaux+Métropole
- Volumétrie : 238 jeux de données et 4823367 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1768411
- Types de licences utilisées : `Licence Ouverte`, `GNU General Public License v3.0`
- Date de création : le 03/04/2019 à 15h45
- Date de dernière mise à jour le 13/01/2023 à 03h11


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b0d11f688ee382af4b18c95/
- Volumétrie : 233 jeux de données et 691 ressources
- Nombre de vues (tous jeux de données confondus) : 271
- Nombre de téléchargements (toutes ressources confondues) : 17
- Types de ressources disponibles : `csv`, `json`, `zip`, `geojson`, `shp`, `xml`, `gtfs.zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 20/07/2018 à 16h03
- Date de dernière mise à jour le 11/01/2023 à 08h50

## BOULOGNE BILLANCOURT

*Commune*

### Source **OpenDataSoft**
- URL : https://boulognebillancourt-seineouest.opendatasoft.com/explore/?sort=title&refine.publisher=Ville+de+Boulogne-Billancourt
- Volumétrie : 22 jeux de données et 20631 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 23890
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 26/04/2019 à 08h07
- Date de dernière mise à jour le 19/10/2021 à 07h35


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54d8f2a4c751df1df7467389
- Volumétrie : 22 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 55
- Nombre de téléchargements (toutes ressources confondues) : 18
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 05/05/2019 à 01h40
- Date de dernière mise à jour le 19/10/2021 à 07h35

## BOURG-LES-VALENCE

*Commune*
## BOURGES PLUS AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.bourgesplus.fr/pages/accueil/
- Volumétrie : 30 jeux de données

## BOURGOGNE FRANCHE COMTE

*Région*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/region-bourgogne-franche-comte
- Volumétrie : 24 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5835cdb988ee383e0bc65bb3
- Volumétrie : 37 jeux de données et 88 ressources
- Nombre de vues (tous jeux de données confondus) : 77
- Nombre de téléchargements (toutes ressources confondues) : 53
- Types de ressources disponibles : `pdf`, `xlsx`, `shp`, `geojson`, `xls`, `html`, `csv`, `gtfs`, `wms`, `wfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Creative Commons Attribution Share-Alike`
- Date de création : le 06/07/2020 à 14h43
- Date de dernière mise à jour le 11/01/2023 à 08h42

## BREST METROPOLE

*Métropole*

### Source **Plateforme territoriale**
- URL : https://opendata.brest-metropole.fr/explore/?sort=modified&refine.publisher=Brest+métropole
- Volumétrie : 210 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff5ba3a7292c64a77d19
- Volumétrie : 5 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 30
- Nombre de téléchargements (toutes ressources confondues) : 22
- Types de ressources disponibles : `csv`, `wms`, `siri`, `gtfs-rt`, `gtfs`, `jpeg`, `txt`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Other (Open)`
- Date de création : le 07/05/2014 à 04h27
- Date de dernière mise à jour le 03/01/2023 à 15h12

## BRETAGNE

*Région*

### Source **OpenDataSoft**
- URL : https://data.bretagne.bzh/explore/?sort=modified&refine.publisher=R%C3%A9gion+Bretagne
- Volumétrie : 165 jeux de données et 1625446 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 276222
- Types de licences utilisées : `Licence ouverte : https://www.etalab.gouv.fr/licence-ouverte-open-licence`, `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`, `Licence ODBL: https://opendatacommons.org/licenses/odbl/summary/`
- Date de création : le 01/10/2014 à 09h38
- Date de dernière mise à jour le 13/01/2023 à 02h26


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/559a4970c751df46a9a453ba
- Volumétrie : 402 jeux de données et 1509 ressources
- Nombre de vues (tous jeux de données confondus) : 1021
- Nombre de téléchargements (toutes ressources confondues) : 561
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `xml`, `xlsx`, `xls`, `docx`, `pdf`, `tiff`, `document`, `dot`, `doc`, `zip`, `image`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 23/07/2019 à 14h05
- Date de dernière mise à jour le 06/12/2022 à 01h01

## BRETEIL

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b236d75c751df4eaa3a9b5e/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h40
- Date de dernière mise à jour le 03/01/2023 à 16h04

## BRIE COMTE ROBERT

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b71455e8b4c41290a9c0450/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 17/08/2018 à 15h15
- Date de dernière mise à jour le 17/08/2018 à 15h15

## BROCAS LES FORGES

*Commune*

### Source **OpenDataSoft**
- URL : http://opendata.brocaslesforges.fr/project/jeux-donnees/
- Volumétrie : aucun jeu de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff65a3a7292c64a77d76
- Volumétrie : 109 jeux de données et 327 ressources
- Nombre de vues (tous jeux de données confondus) : 235
- Nombre de téléchargements (toutes ressources confondues) : 73
- Types de ressources disponibles : `csv`, `json`, `shp`, `doc`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Other (Public Domain)`
- Date de création : le 05/05/2019 à 00h43
- Date de dernière mise à jour le 14/09/2019 à 04h57

## BRON

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Bron
- Volumétrie : 4 jeux de données

## BROU SUR CHANTEREINE

*Commune*

### Source **Plateforme territoriale**
- URL : https://brou-sur-chantereine-agglo-pvm.opendata.arcgis.com/
- Volumétrie : 2 jeux de données

## BUS URBAINS THONONAIS (BUT)

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c079a5e8b4c415b9f0f3be2/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 23/12/2020 à 14h24
- Date de dernière mise à jour le 23/12/2020 à 14h24

## BUSTINCE-IRIBERRY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/bustince-iriberry/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 13/08/2020 à 16h29
- Date de dernière mise à jour le 13/08/2020 à 16h37

## CA ANNEMASSE LES VOIRONS AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ca4bacb634f4118ab6dea0f/
- Volumétrie : 7 jeux de données et 31 ressources
- Nombre de vues (tous jeux de données confondus) : 10
- Nombre de téléchargements (toutes ressources confondues) : 18
- Types de ressources disponibles : `gtfs-rt`, `xml`, `xls`, `gtfs`, `xlsx`, `zip`, `gpx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 19/12/2020 à 18h43
- Date de dernière mise à jour le 15/02/2022 à 10h30

## CA ARDENNE METROPOLE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://ardennemetropole.opendatasoft.com/explore/?sort=modified&refine.publisher=Ardenne+Métropole
- Volumétrie : 10 jeux de données

## CA ARLES CRAU CAMARGUE MONTAGNETTE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-arles-crau-camargue-montagnette
- Volumétrie : 17 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff55a3a7292c64a77cde/
- Volumétrie : 1016 jeux de données et 1016 ressources
- Nombre de vues (tous jeux de données confondus) : 72
- Nombre de téléchargements (toutes ressources confondues) : 5669
- Types de ressources disponibles : `csv`, `zip`, `shp`
- Types de licences utilisées : `Other (Attribution)`, `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 10/05/2019 à 19h34
- Date de dernière mise à jour le 01/01/2023 à 01h09

## CA BAR LE DUC SUD MEUSE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56b8a4ffc751df02a29f9deb
- Volumétrie : 66 jeux de données et 366 ressources
- Nombre de vues (tous jeux de données confondus) : 25
- Nombre de téléchargements (toutes ressources confondues) : 96
- Types de ressources disponibles : `csv`, `pdf`, `xlsx`, `rss`, `html`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 03/02/2016 à 16h10
- Date de dernière mise à jour le 15/06/2022 à 12h22

## CA BEAUNE COTE ET SUD

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/beaune-cote-et-sud-communaute-beaune-chagny-nolay
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bec28758b4c412a07c7ee5e/
- Volumétrie : 1 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/11/2018 à 10h26
- Date de dernière mise à jour le 29/12/2021 à 12h08

## CA CANNES PAYS DE LERINS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3f6542c751df03439fad72/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/01/2019 à 15h46
- Date de dernière mise à jour le 11/01/2023 à 08h44

## CA CAUX VALLEE DE SEINE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.cauxseine.fr
- Volumétrie : 149 jeux de données

## CA CHATEAUROUX METROPOLE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.chateauroux-metropole.fr/explore/?sort=title&refine.publisher=CHATEAUROUX+METROPOLE
- Volumétrie : 66 jeux de données et 10665 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 58954
- Date de création : le 28/12/2018 à 15h27
- Date de dernière mise à jour le 20/04/2021 à 10h59


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bf67a998b4c417eae692920/
- Volumétrie : 93 jeux de données et 308 ressources
- Nombre de vues (tous jeux de données confondus) : 51
- Nombre de téléchargements (toutes ressources confondues) : 21
- Types de ressources disponibles : `csv`, `json`, `zip`, `geojson`, `shp`, `bat`, `xlsx`, `pdf`, `ksh`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 08/01/2019 à 12h53
- Date de dernière mise à jour le 05/09/2022 à 15h08

## CA COEUR D'ESSONNE AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a5f18afc751df7bae61b648/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/01/2018 à 13h55
- Date de dernière mise à jour le 30/01/2018 à 13h55

## CA COLMAR

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e44026b8b4c416f93430c66/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 02/09/2020 à 06h38
- Date de dernière mise à jour le 29/11/2021 à 10h09

## CA DE BEZIERS MEDITERRANEE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b6c2e4b8b4c41287663df35/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 19/07/2022 à 14h50
- Date de dernière mise à jour le 26/08/2022 à 13h24

## CA DE BLOIS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.blois.agglopolys.fr/pages/accueil/
- Volumétrie : 31 jeux de données

## CA DE CASTRES MAZAMET

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c9398018b4c412692ef9cf7/
- Volumétrie : 13 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 09/11/2021 à 09h16
- Date de dernière mise à jour le 12/12/2022 à 11h34

## CA DE CHALONS EN CHAMPAGNE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata-agglo-chalons.opendata.arcgis.com/search
- Volumétrie : 18 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5badf1f68b4c412c38533e4e/
- Volumétrie : 104 jeux de données et 701 ressources
- Nombre de vues (tous jeux de données confondus) : 99
- Nombre de téléchargements (toutes ressources confondues) : 102
- Types de ressources disponibles : `web page`, `arcgis geoservices rest api`, `kml`, `geojson`, `zip`, `csv`, `esri rest`, `ogc wms`, `ogc wfs`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 10/11/2018 à 01h00
- Date de dernière mise à jour le 13/10/2022 à 02h12

## CA DE HAGUENAU

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c0f8dfa634f4122be181e6a/
- Volumétrie : 1 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 12/12/2018 à 16h29
- Date de dernière mise à jour le 25/08/2021 à 12h18

## CA DE L'ALBIGEOIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be0257c634f415cecdbb5ff/
- Volumétrie : 6 jeux de données et 15 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `csv`, `zip`, `gpkg`, `sbn`, `prj`, `dbf`, `shp`, `shx`, `sbx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/11/2018 à 13h51
- Date de dernière mise à jour le 14/12/2022 à 11h49

## CA DE L'AUXERROIS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/communaute-dagglomeration-de-lauxerrois
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bec4aea8b4c416395a5e99e/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `gtfsrt.pb`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/01/2019 à 12h03
- Date de dernière mise à jour le 03/11/2022 à 12h35

## CA DE LA PRESQU'ILE DE GUERANDE ATLANTIQUE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Cap+Atlantique
- Volumétrie : 49 jeux de données et 537330 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 44275
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 16/10/2020 à 14h09
- Date de dernière mise à jour le 13/01/2023 à 03h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cb0688e8b4c410c0750754c/
- Volumétrie : 97 jeux de données et 308 ressources
- Nombre de vues (tous jeux de données confondus) : 133
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 15/04/2019 à 16h06
- Date de dernière mise à jour le 21/12/2022 à 01h00

## CA DE LA REGION DIEPPOISE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c1bb6bf634f416b25009c88/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 20/12/2018 à 16h50
- Date de dernière mise à jour le 20/12/2018 à 16h50

## CA DE LA REGION NAZAIRIENNE ET DE L'ESTUAIRE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=CARENE
- Volumétrie : 37 jeux de données et 256061 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 154732
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 23/08/2018 à 07h48
- Date de dernière mise à jour le 13/01/2023 à 03h06

## CA DE LA RIVIERA FRANCAISE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb48e40634f410f9101c8e3/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 04/07/2022 à 08h32
- Date de dernière mise à jour le 20/09/2022 à 10h48

## CA DE LAVAL

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bc4921f8b4c416a683d003f/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 25/08/2022 à 09h14
- Date de dernière mise à jour le 05/09/2022 à 16h47

## CA DE NEVERS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 9 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5943987b88ee381f30d196f7
- Volumétrie : 11 jeux de données et 30 ressources
- Nombre de vues (tous jeux de données confondus) : 43
- Nombre de téléchargements (toutes ressources confondues) : 30
- Types de ressources disponibles : `zip`, `xlsx`, `csv`, `geojson`, `pdf`, `json`, `url`, `kmz`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/09/2017 à 14h01
- Date de dernière mise à jour le 07/11/2022 à 16h33

## CA DE RAMBOUILLET TERRITOIRES

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/580e1e1388ee385e8e13e4cb
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 25/10/2016 à 13h40
- Date de dernière mise à jour le 11/01/2023 à 08h49

## CA DE VALENCIENNES METROPOLE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://valenciennesmetro.opendatasoft.com/explore/?sort=modified&refine.publisher=CAVM
- Volumétrie : 8 jeux de données et 85983 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1250
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte (Etalab)`
- Date de création : le 26/03/2019 à 16h17
- Date de dernière mise à jour le 28/12/2022 à 15h29

## CA DE VESOUL

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cd5862c8b4c412132ddb84c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 23/09/2021 à 09h13
- Date de dernière mise à jour le 14/09/2022 à 11h41

## CA DINAN AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 9 jeux de données

## CA DRACENIE PROVENCE VERDON AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/dracenie-provence-verdon-agglomeration
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5baa4a978b4c412a0da6114c/
- Volumétrie : 4 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `zip`, `csv`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 04/09/2020 à 13h13
- Date de dernière mise à jour le 23/12/2022 à 15h37

## CA DU BASSIN DE BRIVE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c9e2c5d634f412729e9e460/
- Volumétrie : 3 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `zip`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/04/2019 à 11h26
- Date de dernière mise à jour le 02/01/2023 à 10h07

## CA DU BOULONNAIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5970a76d88ee38216d112aa1/
- Volumétrie : 3 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `kml`, `gtfs-rt`, `gtfs`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 17/11/2017 à 10h12
- Date de dernière mise à jour le 21/03/2022 à 09h21

## CA DU COTENTIN

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 27 jeux de données

## CA DU DOUAISIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59ce086088ee3865cbc4328f/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 90
- Nombre de téléchargements (toutes ressources confondues) : 218
- Types de ressources disponibles : `zip`, `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 05/12/2017 à 18h19
- Date de dernière mise à jour le 29/03/2022 à 08h50

## CA DU GRAND BESANCON

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.grandbesancon.fr
- Volumétrie : 56 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59196e9088ee38471c78dac6
- Volumétrie : 37 jeux de données et 130 ressources
- Nombre de vues (tous jeux de données confondus) : 33
- Nombre de téléchargements (toutes ressources confondues) : 31
- Types de ressources disponibles : `html`, `xml`, `csv`, `shp`, `json`, `pdf`, `rss`, `gtfs`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`, `Other (Attribution)`
- Date de création : le 27/07/2018 à 14h01
- Date de dernière mise à jour le 11/01/2023 à 08h52

## CA DU GRAND CAHORS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/589d6e22c751df0ff0ae0a65
- Volumétrie : 23 jeux de données et 27 ressources
- Nombre de vues (tous jeux de données confondus) : 33
- Nombre de téléchargements (toutes ressources confondues) : 20
- Types de ressources disponibles : `csv`, `txt`, `xlsx`, `zip`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 09/02/2017 à 15h03
- Date de dernière mise à jour le 07/09/2022 à 10h02

## CA DU GRAND SENONAIS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/communaute-dagglomeration-du-grand-senonais
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/datasets/5c2f22d6634f416a515cb152/
- Volumétrie : aucun jeu de données

## CA DU PAYS AJACCIEN

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.ca-ajaccien.corsica/ouverture-des-donnees-publiques-du-pays-ajaccien/donnees/
- Volumétrie : 109 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5540eb53c751df7dafe96a3b
- Volumétrie : 14 jeux de données et 40 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 38
- Types de ressources disponibles : `zip`, `wms`, `wfs`, `json`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 12/05/2015 à 10h18
- Date de dernière mise à jour le 05/01/2023 à 16h44

## CA DU PAYS DE GRASSE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/ca-du-pays-de-grasse
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5afae4b088ee3854d32fa3d9/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 177
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/07/2018 à 11h39
- Date de dernière mise à jour le 05/10/2022 à 10h58

## CA DU PAYS DE SAINT MALO

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.stmalo-agglomeration.fr/explore/?refine.publisher=SAINT-MALO+AGGLOMERATION
- Volumétrie : 30 jeux de données et 4383 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1845
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`, `odc-odbl`
- Date de création : le 19/10/2015 à 12h09
- Date de dernière mise à jour le 02/01/2023 à 16h44


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5492db21c751df7a9c04805a
- Volumétrie : 8 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 26
- Types de ressources disponibles : `csv`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 16/08/2016 à 09h46
- Date de dernière mise à jour le 25/08/2016 à 09h50

## CA DU PUY EN VELAY

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.agglo-lepuyenvelay.fr/geonetwork/srv/fre/catalog.search#/search?facet.q=type%2Fdataset%26sourceCatalog%2F1c86ee62-a93b-4bf6-8f19-9e162a587920&resultType=details&sortBy=relevance&fast=index&_content_type=json&from=1&to=20
- Volumétrie : 103 jeux de données

## CA DU SAINT QUENTINOIS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://opendata.agglo-saintquentinois.fr/datasets
- Volumétrie : 523 jeux de données

## CA DURANCE LUBERON VERDON

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-durance-luberon-verdon
- Volumétrie : 6 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/es/organizations/5911d508c751df168e15a89f/
- Volumétrie : 21 jeux de données et 24 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `shp`, `csv`, `zip`, `gtfs`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 20/10/2020 à 10h12
- Date de dernière mise à jour le 11/01/2023 à 08h52

## CA EVREUX PORTES DE NORMANDIE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://epn-agglo.opendatasoft.com/explore/?sort=modified&refine.publisher=Evreux+Portes+de+Normandie+
- Volumétrie : aucun jeu de données et 15 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 22

## CA FECAMP CAUX LITTORAL AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr/
- Volumétrie : 6 jeux de données

## CA GAILLAC GRAULHET

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ee28a82a4726888290c20c4/
- Volumétrie : 92 jeux de données et 121 ressources
- Nombre de vues (tous jeux de données confondus) : 50
- Nombre de téléchargements (toutes ressources confondues) : 28
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 03/09/2020 à 16h27
- Date de dernière mise à jour le 03/01/2023 à 13h52

## CA GAP TALLARD DURANCE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-de-gap-tallard-durance
- Volumétrie : 1 jeux de données

## CA GRAND AVIGNON

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a7ad2b1c751df27e7d1190c/
- Volumétrie : 302 jeux de données et 903 ressources
- Nombre de vues (tous jeux de données confondus) : 197
- Nombre de téléchargements (toutes ressources confondues) : 618
- Types de ressources disponibles : `geojson`, `csv`, `shp`, `zip`, `.json`, `gtfs-rt`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 19/07/2019 à 10h12
- Date de dernière mise à jour le 11/01/2023 à 08h49

## CA GRAND BELFORT

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://belfort-cab.opendata.arcgis.com/search
- Volumétrie : 40 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5afd7ce888ee387d9f4e136f/
- Volumétrie : 214 jeux de données et 1499 ressources
- Nombre de vues (tous jeux de données confondus) : 116
- Nombre de téléchargements (toutes ressources confondues) : 31
- Types de ressources disponibles : `web page`, `kml`, `csv`, `geojson`, `zip`, `arcgis geoservices rest api`, `ogc wms`, `esri rest`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 06/08/2019 à 12h36
- Date de dernière mise à jour le 20/08/2022 à 02h04

## CA GRAND CHALON

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 3 jeux de données

## CA GRAND CHAMBERY AGGLOMERATION

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://donnees.grandchambery.fr/explore/?refine.publisher=Grand+Chamb%C3%A9ry
- Volumétrie : 45 jeux de données et 22853 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 37399
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 18/12/2018 à 10h33
- Date de dernière mise à jour le 10/01/2023 à 15h15


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ba24dc9634f41611dc498cc/
- Volumétrie : 46 jeux de données et 157 ressources
- Nombre de vues (tous jeux de données confondus) : 80
- Nombre de téléchargements (toutes ressources confondues) : 43
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 28/09/2018 à 22h16
- Date de dernière mise à jour le 14/12/2022 à 02h00

## CA GRAND DOLE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e2086a38b4c41778e8f75bc/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 16/01/2020 à 16h56
- Date de dernière mise à jour le 31/08/2022 à 14h01

## CA GRAND GUERET

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cc0c79b8b4c414c13d12d0a/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 24/06/2019 à 03h53
- Date de dernière mise à jour le 21/06/2022 à 14h35

## CA GRAND PARIS SUD SEINE ESSONNE SENART

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.grandparissud.fr/explore/?refine.publisher=Grand+Paris+Sud
- Volumétrie : 30 jeux de données et 276561 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 21504
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`, `Données sous licence gratuite de repartage`
- Date de création : le 06/10/2018 à 11h40
- Date de dernière mise à jour le 13/01/2023 à 03h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59133d14c751df2fa69bfb86/
- Volumétrie : 31 jeux de données et 81 ressources
- Nombre de vues (tous jeux de données confondus) : 47
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `csv`, ` json`, ` excel`, ` shp`, `kml`, `zip`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 29/05/2017 à 17h35
- Date de dernière mise à jour le 15/11/2022 à 13h21

## CA HAVRAISE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.agglo-lehavre.fr
- Volumétrie : 157 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59bfa079c751df6dad38dc0d/
- Volumétrie : 51 jeux de données et 247 ressources
- Nombre de vues (tous jeux de données confondus) : 202
- Nombre de téléchargements (toutes ressources confondues) : 146
- Types de ressources disponibles : `zip`, `csv`, `wfs`, `kmz`, `json`, `dxf`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 26/07/2018 à 09h19
- Date de dernière mise à jour le 03/01/2023 à 16h07

## CA HERAULT MEDITERRANEE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bac83c9634f4106bc21d00b/
- Volumétrie : 3 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `gtfs-rt`, `gtfs`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 20/07/2020 à 09h56
- Date de dernière mise à jour le 02/11/2021 à 09h46

## CA LA ROCHE SUR YON

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5f106ce48370b7ac5ed923f0/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `json`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 27/06/2021 à 18h13
- Date de dernière mise à jour le 28/06/2022 à 17h27

## CA LA ROCHELLE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata2.agglo-larochelle.fr/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(communaute_dagglomeration_de_la_rochelle)
- Volumétrie : 28 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae6b61b88ee382152cd61a9/
- Volumétrie : 39 jeux de données et 485 ressources
- Nombre de vues (tous jeux de données confondus) : 71
- Nombre de téléchargements (toutes ressources confondues) : 27
- Types de ressources disponibles : `csv`, `zip`, `json`, `geojson`, `txt`, `png`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2021 à 18h21
- Date de dernière mise à jour le 16/11/2022 à 01h09

## CA LANNION TREGOR COMMUNAUTE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 8 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/595254f888ee384d9d265130
- Volumétrie : 10 jeux de données et 53 ressources
- Nombre de vues (tous jeux de données confondus) : 62
- Nombre de téléchargements (toutes ressources confondues) : 23
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2017 à 09h33
- Date de dernière mise à jour le 15/02/2021 à 11h50

## CA LISIEUX NORMANDIE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 19 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ddcf096634f411f920b844d/
- Volumétrie : 34 jeux de données et 102 ressources
- Nombre de vues (tous jeux de données confondus) : 68
- Nombre de téléchargements (toutes ressources confondues) : 87
- Types de ressources disponibles : `json`, `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 26/11/2019 à 13h57
- Date de dernière mise à jour le 28/11/2019 à 15h36

## CA LOIRE FOREZ AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bed6ebf8b4c41329a5f7d1f/
- Volumétrie : 90 jeux de données et 91 ressources
- Nombre de vues (tous jeux de données confondus) : 27
- Nombre de téléchargements (toutes ressources confondues) : 19
- Types de ressources disponibles : `csv`, `pdf`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 21/03/2019 à 11h59
- Date de dernière mise à jour le 11/01/2023 à 08h49

## CA LORIENT AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55b8c85288ee3827203ca288
- Volumétrie : 194 jeux de données et 692 ressources
- Nombre de vues (tous jeux de données confondus) : 487
- Nombre de téléchargements (toutes ressources confondues) : 396
- Types de ressources disponibles : `zip`, `geojson`, `shapefile`, `csv`, `jpg`, `jpeg`, `ecw`, `dwg`, `json`, `shp`, `document`, `image`, `xls`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 03/11/2015 à 15h25
- Date de dernière mise à jour le 20/12/2022 à 01h05

## CA MACONNAIS BEAUJOLAIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c0f89d4634f41198d4a53e8/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 11/12/2018 à 11h08
- Date de dernière mise à jour le 15/11/2022 à 15h53

## CA MONT SAINT MICHEL NORMANDIE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 11 jeux de données

## CA MONTARGOISE ET RIVE DU LOING

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/agglomeration-montargoise-et-rives-du-loing/
- Volumétrie : 8 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `csv`, `zip`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 18/05/2021 à 10h07
- Date de dernière mise à jour le 11/01/2023 à 08h53

## CA MONTLUÇON COMMUNAUTE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ca763cd634f417b395627df/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 19/07/2022 à 18h57
- Date de dernière mise à jour le 29/08/2022 à 14h50

## CA MOULINS COMMUNAUTE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20Pays%20de%20Chevagnes%20en%20Sologne&from=0
- Volumétrie : 8 jeux de données

## CA MULHOUSE ALSACE AGGLOMERATION

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Mulhouse+Alsace+Agglom%C3%A9ration
- Volumétrie : 37 jeux de données et 176743 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 53929
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2017 à 14h56
- Date de dernière mise à jour le 13/01/2023 à 03h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be06c41634f4158c956b300/
- Volumétrie : 134 jeux de données et 503 ressources
- Nombre de vues (tous jeux de données confondus) : 199
- Nombre de téléchargements (toutes ressources confondues) : 162
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/11/2018 à 11h25
- Date de dernière mise à jour le 14/11/2022 à 17h47

## CA PARIS - VALLEE DE LA MARNE

*Communauté d'agglomération*
## CA PARIS SACLAY

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://opendata.paris-saclay.com/explore/?refine.publisher=Communaut%C3%A9+d%27agglom%C3%A9ration+Paris-Saclay
- Volumétrie : aucun jeu de données

## CA PAU BEARN PYRENEES

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.agglo-pau.fr/catal2.html
- Volumétrie : 37 jeux de données

## CA PAYS DE GEX

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5f199bd29460ebdaddd95b99/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Other (Public Domain)`
- Date de création : le 09/09/2020 à 09h57
- Date de dernière mise à jour le 05/01/2023 à 17h56

## CA PAYS DE SAINT OMER

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e848e61eed811f6b7cdc220/
- Volumétrie : 12 jeux de données et 17 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `xlsx`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`
- Date de création : le 01/09/2022 à 10h33
- Date de dernière mise à jour le 25/11/2022 à 10h42

## CA PAYS FOIX VARILHES

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bd9cd77634f413406c077fd/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 31/10/2018 à 16h58
- Date de dernière mise à jour le 21/12/2022 à 12h10

## CA PLAINE VALLEE

*Communauté d'agglomération*
## CA PORTE DE L'ISERE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c516739634f4121096513af/
- Volumétrie : 2 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `gtfsrt.json`, `gtfsrt.pb`, `gtfs`, `neptune.zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/01/2019 à 10h16
- Date de dernière mise à jour le 18/03/2022 à 11h44

## CA PRIVAS CENTRE ARDECHE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cadb863634f41514da8fa0c/
- Volumétrie : 3 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 19
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 15/04/2019 à 09h00
- Date de dernière mise à jour le 04/10/2022 à 10h00

## CA PROVENCE ALPES AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/provence-alpes-agglomeration
- Volumétrie : 3 jeux de données

## CA QUIMPER BRETAGNE OCCIDENTALE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a82e90b88ee387c99662abb/
- Volumétrie : 9 jeux de données et 24 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `zip`, `csv`, `json`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/01/2020 à 12h49
- Date de dernière mise à jour le 10/11/2022 à 14h48

## CA REDON AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59679365c751df12bcc44bba
- Volumétrie : 8 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 11
- Types de ressources disponibles : `zip`, `csv`, `xml`, `json`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 05/10/2017 à 09h41
- Date de dernière mise à jour le 23/11/2022 à 10h53

## CA REGION DE COMPIEGNE ET DE LA BASSE AUTOMNE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e2af3758b4c41693ce44aa3/
- Volumétrie : 2 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `zip`, `kml`, `json`, `xls`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 17/02/2020 à 15h38
- Date de dernière mise à jour le 23/12/2022 à 09h44

## CA ROANNAIS AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae0518e88ee3874bffdf173/
- Volumétrie : 4 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xls`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 25/04/2018 à 12h20
- Date de dernière mise à jour le 11/06/2018 à 11h49

## CA RODEZ AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bf2d970634f414ab5643cee/
- Volumétrie : 4 jeux de données et 41 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xml`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 16/03/2021 à 17h22
- Date de dernière mise à jour le 26/12/2022 à 17h52

## CA ROYAN ATLANTIQUE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.agglo-royan.fr/dataset
- Volumétrie : 8 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56ab6daa88ee3859ece82d6e
- Volumétrie : 9 jeux de données et 55 ressources
- Nombre de vues (tous jeux de données confondus) : 21
- Nombre de téléchargements (toutes ressources confondues) : 14
- Types de ressources disponibles : `zip`, `shape`, `csv`, `ods`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 16/03/2018 à 19h35
- Date de dernière mise à jour le 01/01/2023 à 04h00

## CA SAINT BRIEUC ARMOR AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5acb2f2088ee3813a476714d/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `gbfs`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 04/10/2019 à 18h09
- Date de dernière mise à jour le 19/12/2022 à 11h10

## CA SAINT LOUIS AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a7c19f5c751df3e55b0c321
- Volumétrie : 43 jeux de données et 136 ressources
- Nombre de vues (tous jeux de données confondus) : 77
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `xlsx`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/02/2018 à 11h35
- Date de dernière mise à jour le 28/10/2022 à 08h50

## CA SETE AGGLOPOLE MEDITERRANEE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://opendata.agglopole.fr
- Volumétrie : 25 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d1cacb18b4c416d9490f4a2/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/03/2021 à 10h41
- Date de dernière mise à jour le 08/03/2021 à 10h41

## CA SICOVAL

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.sicoval.fr/explore/?refine.publisher=Sicoval+-+communaut%C3%A9+d%27agglom%C3%A9ration+du+Sud-Est+toulousain
- Volumétrie : 81 jeux de données et 173735 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 52930
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`
- Date de création : le 30/06/2017 à 13h36
- Date de dernière mise à jour le 13/01/2023 à 02h14


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/586b79b3c751df75b32b7154
- Volumétrie : 26 jeux de données et 26 ressources
- Nombre de vues (tous jeux de données confondus) : 112
- Nombre de téléchargements (toutes ressources confondues) : 141
- Types de ressources disponibles : `xls`, `xlsx`, `zip`, `pdf`, `json`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 11/01/2017 à 16h17
- Date de dernière mise à jour le 14/03/2017 à 17h13

## CA SOPHIA ANTIPOLIS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-sophia-antipolis
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a71cf5fc751df19bcdb5d11/
- Volumétrie : 6 jeux de données et 37 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 26
- Types de ressources disponibles : `csv`, ` geojson`, ` txt`, `csv`, `gtfs`, `json`, `xml`, `ecw`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/06/2018 à 16h54
- Date de dernière mise à jour le 11/01/2023 à 08h44

## CA TERRITOIRES VENDOMOIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c067f178b4c410e40e88183/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 04/12/2018 à 16h12
- Date de dernière mise à jour le 02/01/2020 à 10h49

## CA VAL DE FENSCH

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d3193bb634f4103f485c9f9/
- Volumétrie : 18 jeux de données et 47 ressources
- Nombre de vues (tous jeux de données confondus) : 84
- Nombre de téléchargements (toutes ressources confondues) : 66
- Types de ressources disponibles : `zip`, `csv`, `dbf`, `prj`, `cpg`, `shx`, `xml`, `shp`, `sbx`, `sbn`, `.pdf`, `pdf`, `7z`, `oui`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 04/10/2019 à 10h10
- Date de dernière mise à jour le 28/09/2022 à 13h56

## CA VAR ESTEREL MEDITERRANEE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-var-esterel-mediterranee
- Volumétrie : 5 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5f57789537198fbe468ac537/
- Volumétrie : 8 jeux de données et 28 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xlsx`, `zip`, `html`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 28/09/2020 à 14h35
- Date de dernière mise à jour le 29/12/2022 à 01h03

## CA VENTOUX COMTAT VENAISSIN

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-ventoux-comtat-venaissin
- Volumétrie : 54 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c62d28e8b4c414299ae8286/
- Volumétrie : 66 jeux de données et 449 ressources
- Nombre de vues (tous jeux de données confondus) : 1781
- Nombre de téléchargements (toutes ressources confondues) : 231
- Types de ressources disponibles : `shp`, `csv`, `geojson`, `kml`, `html`, `xml`, `gpx`, `pdf`, `gtfs`, `tab`, `zip`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 25/06/2019 à 10h36
- Date de dernière mise à jour le 15/11/2022 à 00h00

## CA VERSAILLES GRAND PARC

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://www-cavgp.opendata.arcgis.com/datasets
- Volumétrie : 39 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58c5a537c751df2fb0fde3c9/
- Volumétrie : 39 jeux de données et 229 ressources
- Nombre de vues (tous jeux de données confondus) : 53
- Nombre de téléchargements (toutes ressources confondues) : 129
- Types de ressources disponibles : `web page`, `geojson`, `kml`, `csv`, `esri rest`, `zip`
- Date de création : le 05/09/2017 à 04h48
- Date de dernière mise à jour le 05/09/2017 à 04h48

## CA VIENNE CONDRIEU

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5f6af29ff7ae06ea64a234d1/
- Volumétrie : 1 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `gtfsrt.pb`, `gtfsrt.json`, `gtfs.zip`, `neptune.zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 23/09/2020 à 09h41
- Date de dernière mise à jour le 06/05/2022 à 08h02

## CACHAN

*Commune*

### Source **Plateforme territoriale**
- URL : https://cachan.opendatasoft.com/explore/?sort=explore.popularity_score&refine.publisher=Ville+de+Cachan
- Volumétrie : 30 jeux de données

## CAEN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d9b54b3634f4164bb0bec9d/
- Volumétrie : 12 jeux de données et 19 ressources
- Nombre de vues (tous jeux de données confondus) : 86
- Nombre de téléchargements (toutes ressources confondues) : 193
- Types de ressources disponibles : `7z`, `kml`, `geojson`, `csv`, `xlsx`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 31/01/2020 à 15h48
- Date de dernière mise à jour le 16/05/2022 à 12h33

## CALVADOS

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/departement-du-calvados/
- Volumétrie : 128 jeux de données et 463 ressources
- Nombre de vues (tous jeux de données confondus) : 220
- Nombre de téléchargements (toutes ressources confondues) : 263
- Types de ressources disponibles : `zip`, `ods`, `csv`, `xlsx`, `gpkg`, `geojson`, `xls`, `shp`, `geopackage`, `excel`, `tiff`, `shapefile`, `ecw`, `.ical`, `.ics`, `.xlsx`, `rss`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 12/12/2018 à 14h54
- Date de dernière mise à jour le 11/01/2023 à 08h50

## CAMOEL

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Camoël
- Volumétrie : 2 jeux de données et 164 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3837
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 23/07/2020 à 16h34
- Date de dernière mise à jour le 26/02/2021 à 05h00

## CANDE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.opendata49.fr/index.php?id=38
- Volumétrie : 3 jeux de données

## CANTAL

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff69a3a7292c64a77d94
- Volumétrie : 6 jeux de données et 14 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 61
- Types de ressources disponibles : `excel`, `zip`, `xls`, `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h47
- Date de dernière mise à jour le 27/09/2022 à 10h45

## CARO

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e6622e5634f417ab82d46ec/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/03/2020 à 12h28
- Date de dernière mise à jour le 08/12/2020 à 08h51

## CARPENTRAS

*Commune*
## CARRIERES-SOUS-POISSY

*Commune*

### Source **Plateforme territoriale**
- URL : https://gpseo.opendatasoft.com/explore/?disjunctive.theme&sort=explore.popularity_score&refine.publisher=Commune+de+Carrières-sous-Poissy
- Volumétrie : 6 jeux de données

## CASTANET TOLOSAN

*Commune*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Commune+de+Castanet-Tolosan
- Volumétrie : aucun jeu de données

## CASTELNAU D'ESTRETEFONDS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Commune+de+Castelnau-d%27Estr%C3%A9tefonds
- Volumétrie : aucun jeu de données

## CASTELNAU LE LEZ

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/castelnau-le-lez
- Volumétrie : 32 jeux de données

## CASTELNAUDARY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58d2805b88ee3830ab40dd02
- Volumétrie : 13 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 28
- Nombre de téléchargements (toutes ressources confondues) : 27
- Types de ressources disponibles : `xml`, `pdf`, `geojson`, `csv`, `kmz`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 22/03/2017 à 15h05
- Date de dernière mise à jour le 15/04/2021 à 15h55

## CASTRIES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/castries
- Volumétrie : 26 jeux de données

## CAZERES SUR GARONNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Commune+de+Caz%C3%A8res-sur-Garonne
- Volumétrie : aucun jeu de données

## CC  CHATEAUGIRON COMMUNAUTE

*Communauté de communes*
## CC AURE ET LOURON

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://opendata.ha-py.fr/pages/vallees-aure-louron/
- Volumétrie : 1 jeux de données

## CC BAUGEOIS VALLEE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/communaute-de-communes-vallee-des-baux-alpilles/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/01/2019 à 14h12
- Date de dernière mise à jour le 20/03/2019 à 15h54

## CC CAUSSES ET VALLEE DE LA DORDOGNE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb5cf6f634f413b3b296999/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 04/10/2018 à 10h53
- Date de dernière mise à jour le 27/05/2019 à 13h44

## CC CAUX ESTUAIRE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 23 jeux de données

## CC CHARENTE LIMOUSINE

*Communauté de communes*
## CC CLUSES ARVE ET MONTAGNE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5f5e6cf238617ce5be60d259/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 17/11/2021 à 16h11
- Date de dernière mise à jour le 12/10/2022 à 15h14

## CC COEUR COTE FLEURIE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://data.coeurcotefleurie.org/dataset
- Volumétrie : 47 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59f846d588ee380840aa2865/
- Volumétrie : 57 jeux de données et 309 ressources
- Nombre de vues (tous jeux de données confondus) : 72
- Nombre de téléchargements (toutes ressources confondues) : 100
- Types de ressources disponibles : `csv`, `xlsx`, `geojson`, `kml`, `shp`, `wms`, `ods`, `json`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 16/11/2017 à 15h35
- Date de dernière mise à jour le 16/11/2022 à 01h10

## CC COEUR D'OSTREVENT

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c937ac98b4c41415cbe68c8/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xls`, `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2019 à 13h17
- Date de dernière mise à jour le 21/03/2019 à 13h18

## CC COEUR DE GARONNE

*Communauté de communes*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Communaut%C3%A9+de+Communes+Coeur+de+Garonne
- Volumétrie : aucun jeu de données

## CC COMBRAILLES SIOULE ET MORGE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20Manzat%20Communaut%C3%A9&from=0
- Volumétrie : 3 jeux de données

## CC DE LACQ-ORTHEZ

*Communauté de communes*

### Source **OpenDataSoft**
- URL : https://opendata.cc-lacqorthez.fr/explore/?sort=modified&refine.publisher=CCLO
- Volumétrie : 14 jeux de données et 26222 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1003
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 15/11/2019 à 09h56
- Date de dernière mise à jour le 12/01/2023 à 19h00

## CC DU BASSIN DE PONT A MOUSSON

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c18eb5a634f41110ca05145/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/12/2018 à 13h55
- Date de dernière mise à jour le 18/12/2018 à 15h18

## CC DU BRIANCONNAIS

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-de-communes-du-brianconnais
- Volumétrie : 1 jeux de données

## CC DU FRONTONNAIS

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/583ef3d9c751df6abcc0bb7e
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/11/2016 à 17h21
- Date de dernière mise à jour le 30/11/2016 à 17h36

## CC DU GRAND AUTUNOIS MORVAN

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c46ef148b4c416a35762335/
- Volumétrie : aucun jeu de données

## CC DU PAYS DE LAPALISSE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20Pays%20de%20Lapalisse&from=0
- Volumétrie : 2 jeux de données

## CC DU PAYS DE SAINT ELOY

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20Coeur%20de%20Combrailles&from=0
- Volumétrie : 2 jeux de données

## CC DU PAYS DE SALERS

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20Pays%20de%20Salers&from=0
- Volumétrie : 7 jeux de données

## CC DU PAYS MORNANTAIS

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bbdad47634f410fb7e37ca7/
- Volumétrie : 10 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 10/10/2018 à 09h51
- Date de dernière mise à jour le 11/01/2023 à 08h42

## CC DU PAYS REUNI D'ORANGE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-de-communes-du-pays-reuni-dorange
- Volumétrie : 31 jeux de données

## CC DU PLATEAU PICARD

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/596c880888ee387e51f2c2f6/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/10/2019 à 14h49
- Date de dernière mise à jour le 21/10/2019 à 14h49

## CC DU VAL D'ILLE AUBIGNE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57daa20388ee385c305ff491
- Volumétrie : 15 jeux de données et 43 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `document`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 12/11/2020 à 10h42
- Date de dernière mise à jour le 20/12/2020 à 06h38

## CC DU VAL DE DROME EN BIOVALLEE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5dfcf3126f44410ff204c4a8/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 20/12/2019 à 17h33
- Date de dernière mise à jour le 20/12/2019 à 17h36

## CC FOREZ EST

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d1dba2d634f41226ef05b97/
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 04/07/2019 à 10h48
- Date de dernière mise à jour le 07/10/2022 à 15h40

## CC HAUTE-BIGORRE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://opendata.ha-py.fr/pages/haute-bigorre/
- Volumétrie : 10 jeux de données

## CC HAUTES TERRES COMMUNAUTE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20C%C3%A9zallier&from=0
- Volumétrie : 6 jeux de données

## CC LEFF ARMOR

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 5 jeux de données

## CC LOIRE LAYON AUBANCE

*Communauté de communes*
## CC MAREMNE ADOUR COTE-SUD (CCMACS)

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset?organization=cc_de_maremne_adour_cote_sud
- Volumétrie : 5 jeux de données

## CC MONFORT COMMUNAUTE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b18fb70c751df1da631a2da/
- Volumétrie : 4 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 18/09/2018 à 12h15
- Date de dernière mise à jour le 11/03/2020 à 17h00

## CC PAYS DE CHEVAGNES

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communauté%20de%20Communes%20du%20Pays%20de%20Chevagnes%20en%20Sologne&from=0&q=PAYS%20DE%20CHEVAGNES
- Volumétrie : 8 jeux de données

## CC PAYS DE FORCALQUIER

*Communauté de communes*
## CC PAYS DES SORGUES MONTS DE VAUCLUSE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/dataset?organization=communaute-de-communes-pays-des-sorgues-monts-de-vaucluse&_organization_limit=0
- Volumétrie : 6 jeux de données

## CC PLAINE LIMAGNE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20Limagne%20Bords%20Allier&from=0
- Volumétrie : 1 jeux de données

## CC PLATEAU DE LANNEMEZAN

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://opendata.ha-py.fr/pages/plateau-de-lannemezan/
- Volumétrie : 1 jeux de données

## CC POHER COMMUNAUTE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 1 jeux de données

## CC PORTE DES VOSGES MERIDIONALES

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d78beaf8b4c413699601db9/
- Volumétrie : 3 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xls`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 11/09/2019 à 11h47
- Date de dernière mise à jour le 23/09/2019 à 03h46

## CC PORTE DROME ARDECHE

*Communauté de communes*
## CC QUERCY VERT AVEYRON

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d838f9b6f444174735e1e0a/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 28/05/2020 à 10h37
- Date de dernière mise à jour le 05/07/2021 à 12h10

## CC SARREBOURG MOSELLE SUD

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bd9653a634f417672a9dd46/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 31/10/2018 à 14h57
- Date de dernière mise à jour le 07/04/2021 à 16h47

## CC VAL AIGO

*Communauté de communes*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Communaut%C3%A9+de+Communes+de+Val+A%C3%AFgo
- Volumétrie : aucun jeu de données

## CC VAL D'ESSONNE

*Communauté de communes*
## CC VAL'EYRIEUX

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://opendata.ardeche.fr/search/og_group_ref/67/type/dataset?sort_by=changed
- Volumétrie : 1 jeux de données

## CC VALLEE DES BAUX ALPILLES

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfbf1548b4c412a0cdc6c70/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `kml`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 29/11/2018 à 11h26
- Date de dernière mise à jour le 15/03/2022 à 09h41

## CC VITRY CHAMPAGNE ET DER

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a32371888ee387affdaa88d/
- Volumétrie : 1 jeux de données et 17 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `jp2`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 14/12/2017 à 16h47
- Date de dernière mise à jour le 18/04/2018 à 13h51

## CC VOLVESTRE

*Communauté de communes*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Communaut%C3%A9+de+Communes+du+Volvestre
- Volumétrie : aucun jeu de données

## CCAS BORDEAUX

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=CCAS+de+la+ville+de+Bordeaux
- Volumétrie : aucun jeu de données

## CENTRE VAL DE LOIRE

*Région*

### Source **OpenDataSoft**
- URL : https://data.centrevaldeloire.fr/explore/?sort=explore.popularity_score&refine.publisher=R%C3%A9gion+Centre-Val+de+Loire
- Volumétrie : 103 jeux de données et 558878 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 199272
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`, `Banatic mentions légales`, `Licence Ouverte (Etalab)`
- Date de création : le 30/10/2017 à 14h37
- Date de dernière mise à jour le 13/01/2023 à 02h02


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/region-centre-val-de-loire/
- Volumétrie : 233 jeux de données et 803 ressources
- Nombre de vues (tous jeux de données confondus) : 1158
- Nombre de téléchargements (toutes ressources confondues) : 976
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`
- Date de création : le 25/01/2019 à 11h28
- Date de dernière mise à jour le 10/01/2023 à 01h11

## CESSON

*Commune*

### Source **OpenDataSoft**
- URL : https://data.ville-cesson.fr/explore/?refine.publisher=Commune+de+Cesson
- Volumétrie : 1 jeux de données et 33 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 14
- Date de création : le 26/10/2018 à 13h46
- Date de dernière mise à jour le 26/10/2018 à 13h52

## CESSON SEVIGNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Cesson-S%C3%A9vign%C3%A9
- Volumétrie : 11 jeux de données et 1771 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 13409
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 15/01/2018 à 13h29
- Date de dernière mise à jour le 07/01/2020 à 14h56

## CHALONS EN CHAMPAGNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5badf0508b4c412b0839e0f4/
- Volumétrie : 15 jeux de données et 86 ressources
- Nombre de vues (tous jeux de données confondus) : 27
- Nombre de téléchargements (toutes ressources confondues) : 17
- Types de ressources disponibles : `geojson`, `kml`, `zip`, `web page`, `csv`, `esri rest`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 01/10/2018 à 17h11
- Date de dernière mise à jour le 01/10/2018 à 17h11

## CHAMBERY

*Commune*

### Source **OpenDataSoft**
- URL : https://donnees.grandchambery.fr/explore/?refine.publisher=Ville+de+Chamb%C3%A9ry
- Volumétrie : 17 jeux de données et 4434 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 13538
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 23/08/2018 à 15h06
- Date de dernière mise à jour le 12/12/2022 à 08h38

## CHAMBERY GRAND LAC ECONOMIE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bcf2a798b4c412f2198fab2/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 12/02/2019 à 10h59
- Date de dernière mise à jour le 28/05/2021 à 16h57

## CHAMPAGNE AU MONT D'OR

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Champagne-au-Mont-d%27Or
- Volumétrie : 6 jeux de données

## CHAMPAGNE SUR SEINE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5846e4ae88ee3811d6c65bb3/
- Volumétrie : 9 jeux de données et 29 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 50
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 16/03/2017 à 11h56
- Date de dernière mise à jour le 24/09/2020 à 11h53

## CHAMPCELLA

*Commune*
## CHAMPOLEON

*Commune*
## CHARENTE

*Département*

### Source **Plateforme territoriale**
- URL : https://data16.lacharente.fr/donnees/recherche/
- Volumétrie : 57 jeux de données

## CHARENTE MARITIME

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59b6449a88ee38073588ac9d
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 44
- Types de ressources disponibles : `geojson`, `pdf`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 25/10/2017 à 17h57
- Date de dernière mise à jour le 06/01/2022 à 15h01

## CHARENTE TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data16.lacharente.fr/donnees/recherche/
- Volumétrie : 16 jeux de données

## CHARTRES METROPOLE TRANSPORTS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c35c8518b4c416fe6cb42c9/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 19/07/2019 à 10h50
- Date de dernière mise à jour le 26/08/2022 à 17h19

## CHASSIEU

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Chassieu
- Volumétrie : 10 jeux de données

## CHATEAU-VILLE-VIEILLE

*Commune*
## CHATEAUGIRON

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5dc1b1c08b4c41677012618b/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 27/01/2020 à 09h55
- Date de dernière mise à jour le 27/01/2020 à 09h55

## CHATEAUNEUF-DU-FAOU

*Commune*
## CHATEAUROUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.chateauroux-metropole.fr/explore/?sort=title&refine.publisher=VILLE+DE+CHATEAUROUX
- Volumétrie : 12 jeux de données et 3915 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8776
- Date de création : le 22/01/2019 à 15h24
- Date de dernière mise à jour le 01/02/2021 à 17h44

## CHATOU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c9e2bfb634f41255724d4ca/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 29/03/2019 à 15h53
- Date de dernière mise à jour le 20/02/2020 à 13h02

## CHAVILLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://chaville-seineouest.opendatasoft.com/explore/?sort=title
- Volumétrie : 0 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5437e8c288ee387cb68f5e87
- Volumétrie : 4 jeux de données et 16 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 29/03/2021 à 12h08
- Date de dernière mise à jour le 14/11/2022 à 23h05

## CHAZELLES-SUR-LYON

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.data.gouv.fr/fr/organizations/ville-de-chazelles-sur-lyon/
- Volumétrie : 5 jeux de données

## CHELLES

*Commune*

### Source **Plateforme territoriale**
- URL : https://chelles-agglo-pvm.opendata.arcgis.com
- Volumétrie : 4 jeux de données

## CHEMERE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Ville+de+Chéméré
- Volumétrie : 4 jeux de données et 332 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 438
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 27/09/2015 à 00h00
- Date de dernière mise à jour le 14/01/2020 à 11h23

## CHENNEVIERES SUR MARNE

*Commune*

### Source **Plateforme territoriale**
- URL : https://chennevieres.data4citizen.com/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(chennevieres)
- Volumétrie : 5 jeux de données

## CITEDIA

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Cit%C3%A9dia
- Volumétrie : 2 jeux de données et 10 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2758009
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 07/09/2016 à 09h53
- Date de dernière mise à jour le 13/01/2023 à 03h15

## CLAPIERS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/clapiers
- Volumétrie : 36 jeux de données

## CLERMONT-FERRAND

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.clermontmetropole.eu/search?source=ville%20de%20clermont-ferrand
- Volumétrie : 13 jeux de données

## CLERMONT-FERRAND METROPOLE

*Métropole*

### Source **Plateforme territoriale**
- URL : https://opendata.clermontmetropole.eu/search?source=clermont%20auvergne%20métropole
- Volumétrie : 14 jeux de données

## COGOLIN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bac7fb68b4c414fe45d8cee/
- Volumétrie : 3 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 27/09/2018 à 09h38
- Date de dernière mise à jour le 11/01/2023 à 08h52

## COLOMIERS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Colomiers
- Volumétrie : 1 jeux de données et 37 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4339
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 04/06/2015 à 00h12
- Date de dernière mise à jour le 27/05/2021 à 13h12

## COMBS LA VILLE

*Commune*

### Source **OpenDataSoft**
- URL : https://combslaville-grandparissud.opendatasoft.com/explore/?refine.publisher=Mairie+de+Combs-la-Ville
- Volumétrie : 15 jeux de données et 2646 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3936
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 26/09/2018 à 10h00
- Date de dernière mise à jour le 03/08/2022 à 14h50

## COMINES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/593fb6b788ee3810680f7775
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 02/08/2018 à 16h27
- Date de dernière mise à jour le 19/05/2022 à 09h19

## COMITE REGIONAL DU TOURISME ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=Comit%C3%A9+r%C3%A9gional+du+tourisme
- Volumétrie : 8 jeux de données et 2216 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 41981
- Types de licences utilisées : `Licence ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 27/02/2014 à 10h40
- Date de dernière mise à jour le 20/05/2022 à 15h36

## COMMUNAUTE D'AGGLOMERATION DU CENTRE LITTORAL

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.cacl-guyane.fr/
- Volumétrie : 40 jeux de données

## COMPAGNIE DES TRANSPORTS STRASBOURGEOIS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae16ff988ee3848de337bb3/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 26/04/2018 à 08h28
- Date de dernière mise à jour le 03/05/2022 à 17h00

## CONDAMINE-CHATELARD

*Commune*
## CONFLANS SAINT HONORINE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.ville-conflans.fr/login/
- Volumétrie : 0 jeux de données

## CORSE

*Région*

### Source **OpenDataSoft**
- URL : https://www.data.corsica/explore/?refine.publisher=Collectivit%C3%A9+Territoriale+de+Corse
- Volumétrie : 100 jeux de données et 28246 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 104355
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 27/01/2014 à 13h41
- Date de dernière mise à jour le 01/10/2022 à 20h19


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/537d58eea3a72973a2dc026c
- Volumétrie : 655 jeux de données et 2168 ressources
- Nombre de vues (tous jeux de données confondus) : 1464
- Nombre de téléchargements (toutes ressources confondues) : 1367
- Types de ressources disponibles : `csv`, `json`, `shp`, `zip`, `xlb`, `docx`, `pdf`, `jpe`, `xml`, `png`, `webarchive`, `7z`, `txt`, `kml`, `geojson`, `xlsx`, `bat`, `a`, `xls`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 24/05/2014 à 12h12
- Date de dernière mise à jour le 01/12/2022 à 22h30

## CORSE COMPETENCES

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://www.data.corsica/explore/?refine.publisher=GIP+Corse+Comp%C3%A9tences
- Volumétrie : aucun jeu de données

## COTE D'OR

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/conseil-departemental-de-cote-dor
- Volumétrie : 3 jeux de données

## COTES D'ARMOR

*Département*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 133 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54aff788c751df39e138127d
- Volumétrie : 144 jeux de données et 801 ressources
- Nombre de vues (tous jeux de données confondus) : 419
- Nombre de téléchargements (toutes ressources confondues) : 320
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`, `geojson`, `com/archivistes75/registre_entrees`, `htm`, `xyz`, `aspx`, `fr/le-departement/fonctionnement-et-organisation/les-maisons-du-departement`, `com/planifier/sports-loisirs/randonnees/les-grands-itineraires/ev1-la-velodyssee`, `fr/data-presentation-ux/#/cg22/datasets/assmat/views/grid?primary-bg-color=@046d8b&primary-font-color=@fff&hidecontrolpanel=true`, `fr/`, `fr`, `pdf`, `fr/politiques-ministerielles/livre-et-lecture/bibliotheques/observatoire-de-la-lecture-publique/repondre-a-l-enquete/enquete-sur-les-bibliotheques-municipales`, `php?page=collections-photographiques`, `php?page=cartes-postales`, `org/wiki/classification_d%c3%a9cimale_de_dewey`, `fr/data-presentation-ux/#/cg22/datasets/assistant_maternel/views/grid`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 27/01/2016 à 10h26
- Date de dernière mise à jour le 15/12/2022 à 17h12

## COULOMMIERS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff6ba3a7292c64a77da2
- Volumétrie : 26 jeux de données et 26 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 86
- Types de ressources disponibles : `xlsx`, `doc`, `xls`, `txt`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h42
- Date de dernière mise à jour le 28/07/2015 à 18h19

## COURCUIRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c937c53634f414c0c84251d/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2019 à 15h47
- Date de dernière mise à jour le 21/03/2019 à 15h48

## COURNONSEC

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/cournonsec
- Volumétrie : 23 jeux de données

## COURNONTERRAL

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/cournonterral
- Volumétrie : 29 jeux de données

## COURTRY

*Commune*

### Source **Plateforme territoriale**
- URL : https://courtry-agglo-pvm.opendata.arcgis.com/search?collection=Dataset
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ee8ce78f450050a4fa6a1d3/
- Volumétrie : 3 jeux de données et 16 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `geojson`, `csv`, `arcgis geoservices rest api`, `web page`, `kml`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 20/08/2022 à 02h03
- Date de dernière mise à jour le 20/08/2022 à 02h03

## CROTS

*Commune*
## CRT BOURGOGNE FC

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/comite-regional-du-tourisme-bourgogne-franche-comte-tourisme
- Volumétrie : 2 jeux de données

## CRUIS

*Commune*
## CU DE DUNKERQUE

*Communauté urbaine*

### Source **OpenDataSoft**
- URL : https://data.dunkerque-agglo.fr/explore/?sort=modified&refine.publisher=Communauté+urbaine+de+Dunkerque
- Volumétrie : 84 jeux de données et 104210 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 27112
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 08/11/2018 à 14h44
- Date de dernière mise à jour le 13/01/2023 à 03h03


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c123930634f4179b683b998/
- Volumétrie : 1 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/07/2019 à 03h44
- Date de dernière mise à jour le 09/12/2022 à 11h05

## CU DU GRAND POITIERS

*Communauté urbaine*

### Source **OpenDataSoft**
- URL : https://data.grandpoitiers.fr/explore/?refine.publisher=Grand+Poitiers+
- Volumétrie : aucun jeu de données et 1629177 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1059644


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54da1f52c751df59f7467389
- Volumétrie : 417 jeux de données et 1482 ressources
- Nombre de vues (tous jeux de données confondus) : 825
- Nombre de téléchargements (toutes ressources confondues) : 667
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `png`, `zip`, `pdf`, `docx`, `ods`, `a`, `obj`, `xlsx`, `xls`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Other (Public Domain)`
- Date de création : le 03/07/2015 à 14h11
- Date de dernière mise à jour le 12/01/2023 à 05h02

## CU GRAND PARIS SEINE & OISE (GPSEO)

*Communauté urbaine*

### Source **Plateforme territoriale**
- URL : https://gpseo.opendatasoft.com/explore/?disjunctive.theme&sort=explore.popularity_score
- Volumétrie : 24 jeux de données

## CU GRAND REIMS

*Communauté urbaine*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aabc0ecc751df25cba8382b/
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `geojson`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 01/10/2021 à 17h09
- Date de dernière mise à jour le 11/01/2023 à 08h50

## CU LE HAVRE SEINE METROPOLE

*Communauté urbaine*

### Source **Plateforme territoriale**
- URL : https://data.lehavreseinemetropole.fr/#
- Volumétrie : 157 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59bfa079c751df6dad38dc0d/
- Volumétrie : aucun jeu de données

## CUGNAUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58d27ed688ee383022f3994b/
- Volumétrie : aucun jeu de données

## DDT TARN ET GARONNE

*SDE*
## DECAZEVILLE COMMUNAUTE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e2067046f444136d7dd0c1e/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/09/2020 à 15h11
- Date de dernière mise à jour le 02/01/2023 à 15h22

## DEUX SEVRES

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59ba4600c751df6bbdb72c11
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/03/2018 à 11h43
- Date de dernière mise à jour le 03/12/2020 à 15h15

## DEVILLE LES ROUEN

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.metropole-rouen-normandie.fr/explore/?stage_theme=true&sort=modified&refine.publisher=Ville+de+Déville-lès-Rouen
- Volumétrie : 1 jeux de données

## DIEPPE

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.adico.fr/fr/dataset?organization=commune-de-dieppe
- Volumétrie : 9 jeux de données

## DIGNE LES BAINS

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-de-digne-les-bains
- Volumétrie : 100 jeux de données

## DOLUS D'OLERON

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/dolus-doleron/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h16
- Date de dernière mise à jour le 14/03/2019 à 12h17

## DOMBROT SUR VAIR

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d3708eb634f411ba1f2be68/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 23/07/2019 à 15h30
- Date de dernière mise à jour le 07/05/2020 à 10h12

## DONCIERES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e32a7e86f4441203195aaa3/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 30/01/2020 à 11h25
- Date de dernière mise à jour le 30/01/2020 à 11h43

## DONGES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Donges
- Volumétrie : 3 jeux de données et 446 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3759
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/05/2019 à 14h59
- Date de dernière mise à jour le 12/01/2023 à 10h00

## DOUBS

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.doubs.fr/explore/?refine.publisher=CD25
- Volumétrie : 38 jeux de données et 246884 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 80054
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 20/03/2018 à 15h58
- Date de dernière mise à jour le 13/01/2023 à 03h10

## DOUBS TRES HAUT DEBIT

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://opendata.doubs.fr/explore/?refine.publisher=Syndicat+Mixte+Doubs+Tr%C3%A8s+Haut+Debit
- Volumétrie : 3 jeux de données et 1319 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6311
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 23/04/2018 à 12h15
- Date de dernière mise à jour le 12/01/2023 à 21h00

## DRAGUIGNAN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfd0fce634f416c97b0b0a9/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `*.pdf`, `.pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 19/12/2018 à 09h28
- Date de dernière mise à jour le 19/12/2018 à 09h39

## DREUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a82b75a88ee3827fadf1afb/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 11/01/2019 à 10h23
- Date de dernière mise à jour le 29/05/2020 à 15h08

## ECHIROLLES

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.metropolegrenoble.fr/ckan/dataset?organization=ville-d-echirolles
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e296fbc6f44414102ba0b85/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 06/07/2020 à 14h46
- Date de dernière mise à jour le 16/11/2022 à 01h10

## EGUISHEIM

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6ac6e58b4c417067851df5/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 18/02/2019 à 16h06
- Date de dernière mise à jour le 28/05/2021 à 16h57

## EMERAINVILLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://emerainville-agglo-pvm.opendata.arcgis.com/
- Volumétrie : 1 jeux de données

## ENCHASTRAYES

*Commune*
## ENNETIERES EN WEPPES

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?&refine.publisher=commune+Enneti%C3%A8res+en+Weppes
- Volumétrie : 5 jeux de données et 58 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 18839
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 21/06/2017 à 13h47
- Date de dernière mise à jour le 19/11/2021 à 12h18

## EPONE

*Commune*

### Source **Plateforme territoriale**
- URL : https://gpseo.opendatasoft.com/explore/?disjunctive.theme&sort=explore.popularity_score&refine.publisher=Commune+d%27Epône
- Volumétrie : 6 jeux de données

## EPT GRAND PARIS SEINE OUEST (GPSO - T3)

*Établissement public territorial*

### Source **OpenDataSoft**
- URL : https://data.seineouest.fr/explore/?sort=records_count&refine.publisher=Seine+Ouest
- Volumétrie : 124 jeux de données et 140523 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 528120
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 14/02/2019 à 09h57
- Date de dernière mise à jour le 13/01/2023 à 03h10


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/543e77eb88ee380c0d999949
- Volumétrie : 126 jeux de données et 420 ressources
- Nombre de vues (tous jeux de données confondus) : 319
- Nombre de téléchargements (toutes ressources confondues) : 218
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 16/04/2019 à 16h53
- Date de dernière mise à jour le 11/01/2023 à 01h02

## ESPARRON

*Commune*
## ESPES-UNDUREIN

*Commune*
## ETCHEBAR

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ef5cd7ae1f79fd615a5b369/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 29/06/2020 à 14h05
- Date de dernière mise à jour le 29/06/2020 à 14h09

## ETIOLLES

*Commune*

### Source **Plateforme territoriale**
- URL : https://etiolles-grandparissud.opendatasoft.com/explore/?sort=modified
- Volumétrie : 0 jeux de données

## ETP GRAND ORLY SEINE BIEVRE (T12)

*Établissement public territorial*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/grand-orly-seine-bievre/
- Volumétrie : 80 jeux de données et 243 ressources
- Nombre de vues (tous jeux de données confondus) : 164
- Nombre de téléchargements (toutes ressources confondues) : 122
- Types de ressources disponibles : `zip`, `json`, `shp`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/01/2020 à 12h48
- Date de dernière mise à jour le 24/01/2020 à 12h49

## ETP GRAND PARIS SUD EST AVENIR (GPSEA - T11)

*Établissement public territorial*

### Source **Plateforme territoriale**
- URL : https://data.sudestavenir.fr/
- Volumétrie : 34 jeux de données

## EURE

*Département*

### Source **Plateforme territoriale**
- URL : https://opendata.eure.fr/search
- Volumétrie : 32 jeux de données

## EURE ET LOIR

*Département*

### Source **OpenDataSoft**
- URL : https://data.eurelien.fr/explore/?refine.publisher=CD28
- Volumétrie : 32 jeux de données et 46146 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 55275
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 26/07/2017 à 14h05
- Date de dernière mise à jour le 13/01/2023 à 01h30


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58b3e7e588ee3814c598d150
- Volumétrie : 42 jeux de données et 262 ressources
- Nombre de vues (tous jeux de données confondus) : 76
- Nombre de téléchargements (toutes ressources confondues) : 39
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/03/2018 à 01h01
- Date de dernière mise à jour le 04/01/2023 à 01h09

## EUROMETROPOLE DE STRASBOURG

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.strasbourg.eu/explore/?sort=modified&refine.publisher=Eurom%C3%A9tropole+de+Strasbourg
- Volumétrie : 61 jeux de données et 236314 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 796009
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`
- Date de création : le 23/10/2018 à 13h27
- Date de dernière mise à jour le 13/01/2023 à 03h23


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59d2b2dc88ee3826a73899be
- Volumétrie : 21 jeux de données et 90 ressources
- Nombre de vues (tous jeux de données confondus) : 1000
- Nombre de téléchargements (toutes ressources confondues) : 34
- Types de ressources disponibles : `csv`, `json`, `shp`, `a`, `geojson`, `png`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 26/11/2019 à 01h00
- Date de dernière mise à jour le 11/01/2023 à 08h50

## EUSKAL HERRIKO LABORANTZA GANBARA

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset?organization=euskal_herriko_laborantza_ganbara
- Volumétrie : 3 jeux de données

## EVOLITY CA MONTBELIARD

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e5e5afe8b4c41422da2d227/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/10/2021 à 09h33
- Date de dernière mise à jour le 30/08/2022 à 14h55

## EVRY COURCOURONNES

*Commune*
## EZE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c388d5a8b4c415b415729d7/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `Open Data Commons Public Domain Dedication and Licence (PDDL)`
- Date de création : le 11/01/2019 à 15h14
- Date de dernière mise à jour le 11/01/2019 à 15h15

## FABREGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/fabr%C3%A8gues
- Volumétrie : 28 jeux de données

## FAUCON-DE-BARCELONNETTE

*Commune*
## FAUGERES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ad996ac751df5e2c97bae5/
- Volumétrie : 5 jeux de données et 61 ressources
- Nombre de vues (tous jeux de données confondus) : 16
- Nombre de téléchargements (toutes ressources confondues) : 28
- Types de ressources disponibles : `html`, `pdf`
- Types de licences utilisées : `Creative Commons Attribution Share-Alike`
- Date de création : le 12/08/2016 à 12h59
- Date de dernière mise à jour le 15/08/2016 à 20h20

## FEGREAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb35a75634f410c19c29451/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 02/10/2018 à 13h49
- Date de dernière mise à jour le 02/10/2018 à 13h50

## FEREL

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Férel
- Volumétrie : 3 jeux de données et 349 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1552
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 10/07/2020 à 08h32
- Date de dernière mise à jour le 13/01/2023 à 03h00

## FINISTERE

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.finistere.fr/explore/?refine.publisher=D%C3%A9partement+du+Finist%C3%A8re
- Volumétrie : 68 jeux de données et 63431 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 69760
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 15/10/2018 à 17h36
- Date de dernière mise à jour le 09/01/2023 à 08h21


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a86b30588ee38553adbaa6e/
- Volumétrie : 73 jeux de données et 211 ressources
- Nombre de vues (tous jeux de données confondus) : 110
- Nombre de téléchargements (toutes ressources confondues) : 116
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `xlsx`, `zip`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 27/11/2018 à 15h36
- Date de dernière mise à jour le 07/01/2023 à 01h04

## FIRMINY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d15b4d8634f4151ec5b1550/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Other (Public Domain)`
- Date de création : le 23/08/2022 à 17h06
- Date de dernière mise à jour le 24/08/2022 à 10h23

## FLEURY SUR ORNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.fleurysurorne.fr/explore/?refine.publisher=Ville+de+Fleury-sur-Orne
- Volumétrie : 46 jeux de données et 2692 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 743
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 29/05/2018 à 10h06
- Date de dernière mise à jour le 13/01/2023 à 03h04

## FLOURENS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Flourens
- Volumétrie : 1 jeux de données et 24 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3997
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 26/11/2015 à 09h23
- Date de dernière mise à jour le 27/05/2021 à 13h20

## FONSORBES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Commune+de+Fonsorbes
- Volumétrie : aucun jeu de données

## FORCALQUIER

*Commune*
## FOREST-SAINT-JULIEN

*Commune*
## FOS SUR MER

*Commune*

### Source **OpenDataSoft**
- URL : https://fos-sur-mer.opendatasoft.com/explore/?sort=modified&refine.publisher=Commune+de+Fos-Sur-Mer
- Volumétrie : 4 jeux de données et 1203 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1045
- Types de licences utilisées : `Open Database License (ODBL) V1.0`
- Date de création : le 17/06/2019 à 08h18
- Date de dernière mise à jour le 08/03/2021 à 15h02

## FRANQUEVILLE SAINT PIERRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.metropole-rouen-normandie.fr/explore/?stage_theme=true&sort=modified&refine.publisher=Ville+de+Franqueville-Saint-Pierre
- Volumétrie : 2 jeux de données

## FREISSINIERES

*Commune*
## FREJUS

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/dataset?_organization_limit=0&organization=ville-de-frejus
- Volumétrie : 6 jeux de données

## FUCLEM

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b34a85fc751df441182f347/
- Volumétrie : aucun jeu de données

## FURMEYER

*Commune*
## GABAT

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e53e62a634f41661560452d/
- Volumétrie : 2 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/02/2020 à 16h14
- Date de dernière mise à jour le 31/07/2020 à 15h35

## GAMARTHE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/gamarthe/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 26/06/2020 à 10h58
- Date de dernière mise à jour le 26/06/2020 à 11h01

## GANGES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c45a5c18b4c4104123ed6d5/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 31/01/2019 à 10h43
- Date de dernière mise à jour le 31/01/2019 à 12h12

## GAP

*Commune*
## GAREIN

*Commune*
## GARGES-LES-GONESSE

*Commune*

### Source **Plateforme territoriale**
- URL : https://garges.data4citizen.com/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(ville_de_gargeslesgonesse)
- Volumétrie : 2 jeux de données

## GEOVENDEE MAISON DES COMMUNES

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data-vendee-fr-paysdelaloire.opendatasoft.com/explore/?refine.publisher=Géo+Vendée
- Volumétrie : 0 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c18fe498b4c4115a1a7ee80/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/09/2021 à 15h08
- Date de dernière mise à jour le 09/09/2021 à 15h09

## GERS

*Département*

### Source **OpenDataSoft**
- URL : https://data.gers.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Département+du+Gers
- Volumétrie : 21 jeux de données et 194684 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 26067
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/02/2015 à 15h14
- Date de dernière mise à jour le 26/12/2022 à 14h07


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d2485a58b4c417f5f478a9f/
- Volumétrie : aucun jeu de données

## GIP LITTORAL AQUITAIN

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset/plages-nouvelle-aquitaine-covid
- Volumétrie : 1 jeux de données

## GIP SEINE AVAL

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://opendata.seine-aval.fr
- Volumétrie : 13 jeux de données

## GIRONDE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.datalocale.fr/dataset?organization=cd33
- Volumétrie : 80 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff67a3a7292c64a77d83/
- Volumétrie : 87 jeux de données et 207 ressources
- Nombre de vues (tous jeux de données confondus) : 122
- Nombre de téléchargements (toutes ressources confondues) : 2424
- Types de ressources disponibles : `shape`, `csv`, `geojson`, `html`, `kml`, `wms`, `xls`, `zip`, `kmz`, `ods`, `json`, `rdf`, `xml`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 18/09/2013 à 08h48

## GIRONDE TOURISME

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff62a3a7292c64a77d5b/
- Volumétrie : 30 jeux de données et 59 ressources
- Nombre de vues (tous jeux de données confondus) : 53
- Nombre de téléchargements (toutes ressources confondues) : 787
- Types de ressources disponibles : `csv`, `html`, `ods`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h47
- Date de dernière mise à jour le 18/09/2013 à 08h48

## GRABELS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/grabels
- Volumétrie : 23 jeux de données

## GRAND ANGOULEME

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://grandangouleme-data16.lacharente.fr/search?collection=Dataset
- Volumétrie : 9 jeux de données

## GRAND EST

*Région*

### Source **Plateforme territoriale**
- URL : http://data.grandest.fr/donnees
- Volumétrie : 39 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55df259f88ee386e57a46ec2
- Volumétrie : 1435 jeux de données et 3344 ressources
- Nombre de vues (tous jeux de données confondus) : 359
- Nombre de téléchargements (toutes ressources confondues) : 64165
- Types de ressources disponibles : `pdf`, `wms`, `wfs`, `xml`, `jpeg`, `csv`, `xlsx`, `zip`, `json`, `document`, `shp`, `gml`, `shapefile`, `docx`, `txt`, `kml`, `xls`
- Types de licences utilisées : `License Not Specified`, `Other (Attribution)`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 25/09/2015 à 11h13
- Date de dernière mise à jour le 07/01/2023 à 01h12

## GRAND PORT MARITIME DE ROUEN

*Délégataire de service public*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 3 jeux de données

## GRAND PORT MARITIME DU HAVRE

*Délégataire de service public*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 22 jeux de données

## GRANVILLE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6ab1d98b4c41356c6fa89a/
- Volumétrie : aucun jeu de données

## GRENOBLE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=grenoble
- Volumétrie : 57 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5733038988ee38472ed1b934/
- Volumétrie : 66 jeux de données et 255 ressources
- Nombre de vues (tous jeux de données confondus) : 230
- Nombre de téléchargements (toutes ressources confondues) : 396
- Types de ressources disponibles : `geojson`, `csv`, `json`, `png`, `kml`, `dxf`, `kmz`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/06/2016 à 16h15
- Date de dernière mise à jour le 24/12/2022 à 01h09

## GRENOBLE ALPES METROPOLE

*Métropole*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=grenoble-alpes-metropole
- Volumétrie : 12 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5732ff7788ee382b08d1b934
- Volumétrie : 33 jeux de données et 67 ressources
- Nombre de vues (tous jeux de données confondus) : 53
- Nombre de téléchargements (toutes ressources confondues) : 37
- Types de ressources disponibles : `csv`, `geojson`, `kml`, `json`, `kmz`, `xls`, `ods`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/06/2016 à 15h49
- Date de dernière mise à jour le 16/11/2022 à 01h02

## GUADELOUPE

*Région*

### Source **Plateforme territoriale**
- URL : https://regionguadeloupe.opendatasoft.com/explore/?sort=modified
- Volumétrie : 83 jeux de données

## GUEMENE PENFAO

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b8fe384634f414b25ea1d30/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/09/2018 à 17h25
- Date de dernière mise à jour le 05/09/2018 à 17h30

## GUERANDE

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Guérande
- Volumétrie : 9 jeux de données et 8834 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8612
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 05/10/2018 à 14h35
- Date de dernière mise à jour le 13/01/2023 à 03h00

## GUIDEL

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Guidel
- Volumétrie : 1 jeux de données et 19 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h31
- Date de dernière mise à jour le 07/04/2021 à 07h50

## GUINGUAMP

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 11 jeux de données

## GWAD'AIR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-gwadair.opendata.arcgis.com/datasets
- Volumétrie : 13 jeux de données

## HALSOU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e4fd909634f41282fea1334/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `geojson`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 21/02/2020 à 16h22
- Date de dernière mise à jour le 21/02/2020 à 16h30

## HAUTE GARONNE

*Département*

### Source **OpenDataSoft**
- URL : https://data.haute-garonne.fr/explore/?refine.publisher=Conseil+départemental+de+la+Haute-Garonne
- Volumétrie : 196 jeux de données et 168073 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 291462
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte (Etalab)`, `Open License v2.0`
- Date de création : le 03/12/2015 à 13h26
- Date de dernière mise à jour le 13/01/2023 à 03h15


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/582a045088ee3875cec65bb4
- Volumétrie : 335 jeux de données et 1192 ressources
- Nombre de vues (tous jeux de données confondus) : 1148
- Nombre de téléchargements (toutes ressources confondues) : 694
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `ods`, `svg`, `png`, `zip`, `jpe`, `a`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 17/11/2016 à 10h44
- Date de dernière mise à jour le 12/01/2023 à 05h02

## HAUTE GARONNE TOURISME

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.haute-garonne.fr/explore/?refine.publisher=Comit%C3%A9+D%C3%A9partemental+du+Tourisme+de+la+Haute-Garonne+(CDT31)
- Volumétrie : aucun jeu de données

## HAUTES ALPES

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-des-hautes-alpes
- Volumétrie : 61 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a98083188ee387ee20c3cdb/
- Volumétrie : 55 jeux de données et 278 ressources
- Nombre de vues (tous jeux de données confondus) : 136
- Nombre de téléchargements (toutes ressources confondues) : 54
- Types de ressources disponibles : `zip`, `json`, `shp`, `image`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/01/2020 à 12h49
- Date de dernière mise à jour le 24/01/2020 à 12h49

## HAUTES PYRENEES

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.ha-py.fr/explore/?refine.publisher=D%C3%A9partement+des+Hautes-Pyr%C3%A9n%C3%A9es
- Volumétrie : 46 jeux de données et 163625 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 60414
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 21/03/2018 à 15h47
- Date de dernière mise à jour le 13/01/2023 à 02h26


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c3f1ca2634f411d2aef4ddb/
- Volumétrie : 47 jeux de données et 164 ressources
- Nombre de vues (tous jeux de données confondus) : 56
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 25/01/2019 à 11h57
- Date de dernière mise à jour le 15/11/2022 à 06h40

## HAUTS DE FRANCE

*Région*

### Source **Plateforme territoriale**
- URL : http://opendata.hautsdefrance.fr/dataset
- Volumétrie : 167 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5df935d8634f414e69469492/
- Volumétrie : 444 jeux de données et 20467 ressources
- Nombre de vues (tous jeux de données confondus) : 616
- Nombre de téléchargements (toutes ressources confondues) : 751
- Types de ressources disponibles : `wms`, `wfs`, `html`, `csv`, `xlsx`, `png`, `zip`, `pdf`, `xls`, `geojson`, `arcgis`, `xml`, `qgis`, `application/rtf`, `excel`, `shp`, `rtf`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 09/01/2020 à 01h10
- Date de dernière mise à jour le 07/01/2023 à 01h12

## HAUTS DE SEINE

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?refine.publisher=D%C3%A9partement+des+Hauts-de-Seine
- Volumétrie : 145 jeux de données et 1072596 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 795349
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `CC by NC-ND`
- Date de création : le 16/12/2012 à 23h00
- Date de dernière mise à jour le 13/01/2023 à 02h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/544e1b0888ee38289ae1ed05
- Volumétrie : 303 jeux de données et 1393 ressources
- Nombre de vues (tous jeux de données confondus) : 12763
- Nombre de téléchargements (toutes ressources confondues) : 3364
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 06/04/2017 à 06h02
- Date de dernière mise à jour le 04/11/2022 à 09h37

## HAWA MAYOTTE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-hawa-mayotte.opendata.arcgis.com/datasets
- Volumétrie : 6 jeux de données

## HEMEVEZ

*Commune*
## HERAULT

*Département*

### Source **OpenDataSoft**
- URL : https://www.herault-data.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Conseil+D%C3%A9partemental+de+l%27H%C3%A9rault
- Volumétrie : 99 jeux de données et 1253360 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 189701
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 01/01/1991 à 08h56
- Date de dernière mise à jour le 13/01/2023 à 03h05

## HERAULT TOURISME

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://www.herault-data.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=H%C3%A9rault+Tourisme
- Volumétrie : aucun jeu de données

## HERBIGNAC

*Commune*

### Source **OpenDataSoft**
- URL : https://data.paysdelaloire.fr/explore/?disjunctive.diffuseur&disjunctive.gestionnaire&disjunctive.publisher&source=shared&sort=title&refine.publisher=Herbignac
- Volumétrie : aucun jeu de données

## HOSTA

*Commune*
## IAU ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://data.iau-idf.fr/datasets
- Volumétrie : 114 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff7da3a7292c64a77e23/
- Volumétrie : 53 jeux de données et 159 ressources
- Nombre de vues (tous jeux de données confondus) : 1165
- Nombre de téléchargements (toutes ressources confondues) : 1674
- Types de ressources disponibles : `html`, `csv`, `json`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h41
- Date de dernière mise à jour le 14/09/2013 à 19h42

## IFFENDIC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b32535988ee380dd6e257de/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h45
- Date de dernière mise à jour le 12/12/2022 à 12h23

## ILE DE FRANCE

*Région*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=R%C3%A9gion+%C3%8Ele-de-France
- Volumétrie : 356 jeux de données et 798229 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1488493
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `CC BY-NC-ND`
- Date de création : le 30/05/2013 à 01h41
- Date de dernière mise à jour le 13/01/2023 à 03h01


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa8a3a7292c64a780c8
- Volumétrie : 1314 jeux de données et 6678 ressources
- Nombre de vues (tous jeux de données confondus) : 17162
- Nombre de téléchargements (toutes ressources confondues) : 17633
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `zip`, `doc`, `xlb`, `docx`, `xlsx`, `a`, `png`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Other (Open)`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 16/12/2022 à 05h04

## ILE DE FRANCE MOBILITES

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://opendata.stif.info/explore/?refine.publisher=Ile-de-France+Mobilit%C3%A9s
- Volumétrie : 2 jeux de données et 28439 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 273
- Types de licences utilisées : `Licence ODbL Version Française`
- Date de création : le 15/06/2022 à 12h24
- Date de dernière mise à jour le 01/01/2023 à 09h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/568e5e9488ee38033aaf0bf4/
- Volumétrie : 146 jeux de données et 652 ressources
- Nombre de vues (tous jeux de données confondus) : 390
- Nombre de téléchargements (toutes ressources confondues) : 527
- Types de ressources disponibles : `csv`, `json`, `pdf`, `geojson`, `shp`, `netex`, `web`, `siri`, `gtfs`, `pptx`, `zip`, `bat`, `png`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 14/03/2018 à 06h58
- Date de dernière mise à jour le 11/01/2023 à 08h50

## ILE ET VILAINE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d15cbed8b4c411740bae4bc/
- Volumétrie : 120 jeux de données et 323 ressources
- Nombre de vues (tous jeux de données confondus) : 100
- Nombre de téléchargements (toutes ressources confondues) : 38
- Types de ressources disponibles : `csv`, `xlsx`, `geojson`, `shape`, `json`, `xls`, `shz`, `html`, `pdf`, `zip`, `ods`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/01/2020 à 12h46
- Date de dernière mise à jour le 11/10/2022 à 00h00

## INDRE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a82f34188ee380c10423e48/
- Volumétrie : 9 jeux de données et 192 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 26
- Types de ressources disponibles : `csv`, `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 13/02/2018 à 16h17
- Date de dernière mise à jour le 03/01/2023 à 13h21

## INDRE ET LOIRE

*Département*

### Source **Plateforme territoriale**
- URL : https://data-cd37.opendata.arcgis.com/search
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5733002788ee384aabd1b934/
- Volumétrie : 27 jeux de données et 200 ressources
- Nombre de vues (tous jeux de données confondus) : 54
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `csv`, `arcgis geoservices rest api`, `geojson`, `web page`, `kml`, `esri rest`, `ogc wms`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/05/2019 à 00h43
- Date de dernière mise à jour le 09/11/2022 à 01h01

## INFOCOM'94

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data.infocom94.fr/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(infocom94)
- Volumétrie : 14 jeux de données

## ISERE

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.isere.fr/explore/?sort=modified&refine.publisher=D%C3%A9partement+de+l%27Is%C3%A8re
- Volumétrie : 58 jeux de données et 112619 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 109874
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 30/01/2019 à 08h23
- Date de dernière mise à jour le 13/01/2023 à 03h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb1c7e4634f412a95dff6b9/
- Volumétrie : 89 jeux de données et 301 ressources
- Nombre de vues (tous jeux de données confondus) : 401
- Nombre de téléchargements (toutes ressources confondues) : 136
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `gtfsrt.pb`, `gtfsrt.json`, `neptune.zip`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/10/2018 à 09h26
- Date de dernière mise à jour le 16/11/2022 à 00h00

## ISSY LES MOULINEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.issy.com/explore/?disjunctive.features&disjunctive.theme&disjunctive.keyword&disjunctive.publisher&sort=modified&refine.publisher=Ville+d%27Issy-les-Moulineaux
- Volumétrie : 206 jeux de données et 93936 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 409256
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`, `Domaine public`
- Date de création : le 05/12/2014 à 17h03
- Date de dernière mise à jour le 13/01/2023 à 03h29


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb7a3a7292c64a78145
- Volumétrie : 458 jeux de données et 1931 ressources
- Nombre de vues (tous jeux de données confondus) : 6459
- Nombre de téléchargements (toutes ressources confondues) : 2187
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `jpe`, `zip`, `xlb`, `pdf`, `csv`, ` json`, ` excel`, ` shp`, `xml`, `rss`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h40
- Date de dernière mise à jour le 22/12/2022 à 05h01

## ISTRES

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.istres.fr/index.php?id=618
- Volumétrie : 33 jeux de données

## ISTURITS

*Commune*
## ITXASSOU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/itxassou/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 15/10/2019 à 15h49
- Date de dernière mise à jour le 10/02/2020 à 23h09

## JACOU

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/jacou
- Volumétrie : 26 jeux de données

## JARJAYES

*Commune*
## JAXU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/jaxu/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/02/2020 à 14h28
- Date de dernière mise à jour le 30/07/2021 à 13h04

## JURA

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/departement-du-jura
- Volumétrie : 2 jeux de données

## JUVIGNAC

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/juvignac
- Volumétrie : 29 jeux de données

## KEOLIS AGEN

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfe67b8634f4121765f58f3/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 02/01/2023 à 10h16
- Date de dernière mise à jour le 02/01/2023 à 10h18

## KEOLIS BUS VERTS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d13756e8b4c415ad792a83e/
- Volumétrie : 1 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `zip`, `7z`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 26/06/2019 à 15h46
- Date de dernière mise à jour le 12/08/2020 à 17h36

## KEOLIS CAEN

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c09189f8b4c414a8d02298e/
- Volumétrie : 27 jeux de données et 98 ressources
- Nombre de vues (tous jeux de données confondus) : 22
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `zip`, `pdf`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/11/2020 à 09h35
- Date de dernière mise à jour le 22/12/2022 à 01h03

## KEOLIS CHAUMONT

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c5063ec8b4c412d63742601/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 29/01/2019 à 15h58
- Date de dernière mise à jour le 29/01/2019 à 15h58

## KEOLIS MENTON RIVIERA

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ea19ef9a0a58ece59bee631/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/10/2021 à 17h11
- Date de dernière mise à jour le 24/10/2022 à 09h49

## KEOLIS RENNES

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.explore.star.fr/explore/?refine.publisher=STAR
- Volumétrie : 35 jeux de données et 28695 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 29458208
- Types de licences utilisées : `Open Database License (ODbL)`, `CC BY-ND`
- Date de création : le 14/11/2014 à 10h12
- Date de dernière mise à jour le 13/01/2023 à 03h29

## KICEO CTGMVA

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a6f24a4c751df613f36c422/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `gbfs`, `gtfs-rt`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 22/01/2019 à 18h33
- Date de dernière mise à jour le 04/08/2022 à 09h37

## LA BATIE-VIEILLE

*Commune*
## LA BAULE ESCOUBLAC

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=La+Baule-Escoublac
- Volumétrie : 14 jeux de données et 8944 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 11088
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 04/09/2018 à 07h48
- Date de dernière mise à jour le 13/01/2023 à 03h00

## LA CHAPELLE D'ARMENTIERES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b518bb188ee3863608f3fac/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 20/07/2018 à 09h25
- Date de dernière mise à jour le 20/07/2018 à 09h25

## LA CHAPELLE DES MARAIS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=La+Chapelle+des+Marais
- Volumétrie : 2 jeux de données et 432 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2189
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/03/2021 à 15h58
- Date de dernière mise à jour le 12/01/2023 à 14h10

## LA FARE-EN-CHAMPSAUR

*Commune*
## LA GAUDE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ba9f0bb634f416762d56c0c/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 08/10/2018 à 14h56
- Date de dernière mise à jour le 12/10/2018 à 17h13

## LA NOUAYE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3495d188ee382f97297bdc/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h34
- Date de dernière mise à jour le 21/05/2019 à 11h34

## LA REUNION

*Région*

### Source **Plateforme territoriale**
- URL : https://data.regionreunion.com/explore/?disjunctive.theme&disjunctive.modified&disjunctive.publisher&sort=modified&refine.publisher=Conseil+Régional+La+Réunion
- Volumétrie : 21 jeux de données

## LA ROCHE SUR YON

*Commune*

### Source **OpenDataSoft**
- URL : https://data.larochesuryon.fr/explore/?refine.publisher=Ville+de+la+Roche-sur-Yon
- Volumétrie : 49 jeux de données et 19701 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 64197
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 22/07/2016 à 00h00
- Date de dernière mise à jour le 08/01/2023 à 23h01

## LA ROCHELLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.larochelle.fr/dataset/?metakey=dataset_property&metaval=140
- Volumétrie : 374 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5821930b88ee386862c65bb3/
- Volumétrie : 232 jeux de données et 312 ressources
- Nombre de vues (tous jeux de données confondus) : 2869
- Nombre de téléchargements (toutes ressources confondues) : 177
- Types de ressources disponibles : `csv`, `zip`, `json`, `geojson`, `api`, `xml`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`
- Date de création : le 28/04/2021 à 02h04
- Date de dernière mise à jour le 19/11/2022 à 01h03

## LA SEYNE SUR MER

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-la-seyne-sur-mer
- Volumétrie : 1 jeux de données

## LA TREMBLADE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2ec6634f411c5580a72b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h13
- Date de dernière mise à jour le 14/03/2019 à 12h13

## LA VICOMTE SUR RANCE

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 2 jeux de données

## LACARRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/lacarre/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 19/03/2020 à 15h11
- Date de dernière mise à jour le 19/03/2020 à 15h14

## LACRABE

*Commune*
## LAMARCHE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d6629786f444132f99e032c/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 28/08/2019 à 03h20
- Date de dernière mise à jour le 28/08/2019 à 04h30

## LANCIEUX

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/donnees
- Volumétrie : 1 jeux de données

## LANESTER

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Lanester
- Volumétrie : 17 jeux de données et 10448 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1525
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h35
- Date de dernière mise à jour le 19/06/2022 à 18h19


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59d6526ac751df274a175d35/
- Volumétrie : 2 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 20
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 21/12/2017 à 14h30
- Date de dernière mise à jour le 29/12/2017 à 14h30

## LANNION

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 7 jeux de données

## LANTABAT

*Commune*
## LARDIER-ET-VALENÇA

*Commune*
## LATTES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/lattes
- Volumétrie : 28 jeux de données

## LAVERUNE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/lav%C3%A9rune
- Volumétrie : 31 jeux de données

## LE COUDRAY MONTCEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.coudray-montceaux.fr/explore/?refine.publisher=Mairie+du+Coudray-Montceaux
- Volumétrie : 7 jeux de données et 65 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 7315
- Date de création : le 05/10/2018 à 09h33
- Date de dernière mise à jour le 08/10/2018 à 09h17

## LE CRES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/le-cr%C3%A8s
- Volumétrie : 29 jeux de données

## LE CROISIC

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Le+Croisic
- Volumétrie : 1 jeux de données et 116 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1390
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/07/2020 à 04h00
- Date de dernière mise à jour le 26/02/2021 à 05h00

## LE DEVOLUY

*Commune*
## LE GLAIZIL

*Commune*
## LE HAVRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.lehavreseinemetropole.fr/#
- Volumétrie : 157 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a3a7cd8c751df3092aea45a/
- Volumétrie : 61 jeux de données et 305 ressources
- Nombre de vues (tous jeux de données confondus) : 135
- Nombre de téléchargements (toutes ressources confondues) : 103
- Types de ressources disponibles : `wfs`, `kmz`, `json`, `dxf`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/07/2018 à 09h19
- Date de dernière mise à jour le 26/07/2018 à 09h19

## LE MANS

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.lemans.fr/citoyen/la-collectivite/lopen-data/les-contenus-en-open-data/
- Volumétrie : 24 jeux de données

## LE MOTIF - OBSERVATOIRE DU LIVRE ET DE L'ECRIT

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=MOTif
- Volumétrie : 8 jeux de données et 4615 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 47153
- Types de licences utilisées : `Open Database License (ODbL)`, `ODbL`, `Licence ouverte (Etalab)`
- Date de création : le 18/03/2014 à 09h10
- Date de dernière mise à jour le 24/11/2021 à 17h08

## LE PASSAGE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab24b9188ee385d43b29616/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2018 à 13h19
- Date de dernière mise à jour le 21/03/2018 à 14h00

## LE PELLERIN

*Commune*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Le+Pellerin
- Volumétrie : 4 jeux de données et 2183 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3878
- Types de licences utilisées : `Open Database License-ODBL`
- Date de création : le 26/03/2017 à 22h00
- Date de dernière mise à jour le 24/08/2022 à 13h48

## LE PERREUX SUR MARNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff85a3a7292c64a77ea9
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 15
- Types de ressources disponibles : `ods`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h47
- Date de dernière mise à jour le 07/01/2014 à 14h54

## LE POINCONNET

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.centrevaldeloire.fr/explore/?q=le+poin%C3%A7onnet&sort=explore.popularity_score
- Volumétrie : 3 jeux de données

## LE POULIGUEN

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Le+Pouliguen
- Volumétrie : 1 jeux de données et 476 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1274
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 02/12/2022 à 11h16
- Date de dernière mise à jour le 02/12/2022 à 11h16

## LE PRADET

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58f5d64788ee3847b91e4f32
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 40
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `xlsx`, `ods`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/04/2017 à 11h36
- Date de dernière mise à jour le 18/04/2017 à 11h36

## LE SAUZE-DU-LAC

*Commune*
## LES LILAS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff86a3a7292c64a77eab
- Volumétrie : 32 jeux de données et 1644 ressources
- Nombre de vues (tous jeux de données confondus) : 42
- Nombre de téléchargements (toutes ressources confondues) : 24
- Types de ressources disponibles : `pdf`, `csv`, `xlsx`, `docx`, `xml`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 01/07/2019 à 08h28
- Date de dernière mise à jour le 11/01/2023 à 10h09

## LES ORRES

*Commune*
## LES THUILES

*Commune*
## LES VIGNEAUX

*Commune*
## LEVALLOIS

*Commune*
## LIBOURNE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.libourne.fr/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(la-ville-de-libourne)
- Volumétrie : 19 jeux de données

## LIEUSAINT

*Commune*

### Source **OpenDataSoft**
- URL : https://data.ville-lieusaint.fr/explore/?sort=title&refine.publisher=Mairie+de+Lieusaint
- Volumétrie : 7 jeux de données et 250 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 65
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 27/09/2018 à 10h21
- Date de dernière mise à jour le 08/04/2020 à 16h04

## LIG'AIR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-ligair.opendata.arcgis.com/datasets
- Volumétrie : 70 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be949a9634f415deb46af0e/
- Volumétrie : 627 jeux de données et 3340 ressources
- Nombre de vues (tous jeux de données confondus) : 222
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `csv`, `kml`, `geojson`, `zip`, `web page`, `arcgis geoservices rest api`, `esri rest`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Creative Commons Attribution Share-Alike`
- Date de création : le 18/11/2018 à 23h49
- Date de dernière mise à jour le 25/08/2022 à 02h09

## LILA PRESQU'ILE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Lila+Presqu%27ile
- Volumétrie : 6 jeux de données et 1657 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 9812
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 27/11/2020 à 09h30
- Date de dernière mise à jour le 26/09/2022 à 09h58

## LILLE

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+de+Lille
- Volumétrie : 76 jeux de données et 1270883 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 81155
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 04/12/2019 à 14h29
- Date de dernière mise à jour le 13/01/2023 à 02h30

## LIMANS

*Commune*
## LIMOGES

*Commune*

### Source **Plateforme territoriale**
- URL : https://sig-ville-limoges.opendata.arcgis.com
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54acf7fcc751df4851de6536
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/09/2021 à 14h58
- Date de dernière mise à jour le 17/04/2022 à 09h16

## LIMONEST

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Commune%20de%20Limonest
- Volumétrie : 1 jeux de données

## LISIEUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57835411c751df7f2ef3855e
- Volumétrie : 13 jeux de données et 34 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 11
- Types de ressources disponibles : `pdf`, `xml`, `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 01/02/2019 à 10h31
- Date de dernière mise à jour le 07/08/2020 à 14h27

## LISSES

*Commune*

### Source **Plateforme territoriale**
- URL : https://lisses-grandparissud.opendatasoft.com/explore/?sort=modified&refine.publisher=Ville+de+Lisses
- Volumétrie : 2 jeux de données

## LOIR ET CHER

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff68a3a7292c64a77d8a
- Volumétrie : 70 jeux de données et 70 ressources
- Nombre de vues (tous jeux de données confondus) : 194
- Nombre de téléchargements (toutes ressources confondues) : 495
- Types de ressources disponibles : `csv`, `shp`, `xls`, `json`, `autre`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 01/02/2021 à 12h48

## LOIRE ATLANTIQUE

*Département*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Département+de+Loire-Atlantique
- Volumétrie : 271 jeux de données et 1379735 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 370418
- Types de licences utilisées : `Licence Ouverte-LO`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/05/2009 à 00h00
- Date de dernière mise à jour le 13/01/2023 à 03h33


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff6fa3a7292c64a77dc1/
- Volumétrie : 331 jeux de données et 1130 ressources
- Nombre de vues (tous jeux de données confondus) : 845
- Nombre de téléchargements (toutes ressources confondues) : 84
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `ai`, `zip`, `pdf`, `a`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 31/07/2018 à 15h25
- Date de dernière mise à jour le 08/11/2022 à 11h02

## LOIRE ATLANTIQUE DEVELOPPEMENT

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Loire-Atlantique+Développement
- Volumétrie : 17 jeux de données et 606 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3048
- Types de licences utilisées : `Open Database License-ODBL`
- Date de création : le 31/10/2012 à 00h00
- Date de dernière mise à jour le 02/01/2023 à 12h38


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff87a3a7292c64a77eb0/
- Volumétrie : 20 jeux de données et 80 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 246
- Types de ressources disponibles : `json`, `csv`, `xml`, `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 10/12/2013 à 14h47

## LOIRE ATLANTIQUE TOURISME

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=e-SPRIT
- Volumétrie : 13 jeux de données et 7470 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6672
- Types de licences utilisées : `Licence Ouverte-LO`
- Date de création : le 08/11/2022 à 13h14
- Date de dernière mise à jour le 13/01/2023 à 02h18

## LOIRET

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/departement-du-loiret/
- Volumétrie : 90 jeux de données et 171 ressources
- Nombre de vues (tous jeux de données confondus) : 102
- Nombre de téléchargements (toutes ressources confondues) : 31
- Types de ressources disponibles : `zip`, `xls`, `csv`, `ods`, `xlsx`, `json`, `esri rest`, `web page`, `geojson`, `kml`, `.json`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 17/09/2018 à 08h52
- Date de dernière mise à jour le 11/01/2023 à 08h52

## LONGJUMEAU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff88a3a7292c64a77eb2
- Volumétrie : 16 jeux de données et 14 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 60
- Types de ressources disponibles : `csv`, `rtf`, `autre`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h49
- Date de dernière mise à jour le 04/01/2019 à 09h02

## LOOS

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?sort=modified&refine.publisher=Ville+de+Loos
- Volumétrie : 6 jeux de données et 1591 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 31534
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 03/04/2019 à 13h19
- Date de dernière mise à jour le 08/07/2022 à 09h33

## LORIENT

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Ville+de+Lorient
- Volumétrie : 27 jeux de données et 86428 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 55879
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/01/2019 à 13h39
- Date de dernière mise à jour le 01/01/2023 à 07h01


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59e05c6588ee386ef4f4f86d/
- Volumétrie : 30 jeux de données et 102 ressources
- Nombre de vues (tous jeux de données confondus) : 224
- Nombre de téléchargements (toutes ressources confondues) : 143
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 16/02/2019 à 16h41
- Date de dernière mise à jour le 03/11/2022 à 15h02

## LOT

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ac5c34488ee386e5ad2bb40/
- Volumétrie : 10 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 10
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 13/04/2018 à 08h23
- Date de dernière mise à jour le 29/11/2022 à 09h53

## LOUDEAC COMMUNAUTE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/donnees
- Volumétrie : 2 jeux de données

## LOUVIERS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53806fe2a3a7297e4d35d6c6
- Volumétrie : 3 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 45
- Nombre de téléchargements (toutes ressources confondues) : 24
- Types de ressources disponibles : `html`, `xls`, `csv`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 24/05/2014 à 13h08
- Date de dernière mise à jour le 24/05/2014 à 13h55

## LUBBON

*Commune*
## LYON

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Lyon
- Volumétrie : 16 jeux de données

## MADININAIR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-madininair.opendata.arcgis.com/datasets
- Volumétrie : 26 jeux de données

## MAHINA

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/552d6a09c751df1ed737dddb
- Volumétrie : 2 jeux de données et 22 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `kml`, `geojson`, `csv`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 23/07/2015 à 23h35
- Date de dernière mise à jour le 25/07/2015 à 05h43

## MAINE ET LOIRE

*Département*

### Source **Plateforme territoriale**
- URL : https://data.maine-et-loire.fr/explore/?q=&disjunctive.diffuseur&disjunctive.publisher&source=shared&sort=modified&geonav=world%2Fworld_fr%2Ffr_40_52%2Ffr_60_49&geonav-asc&refine.publisher=D%C3%A9partement+de+Maine-et-Loire
- Volumétrie : 88 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e32cef58b4c411011bd3bab/
- Volumétrie : 134 jeux de données et 423 ressources
- Nombre de vues (tous jeux de données confondus) : 207
- Nombre de téléchargements (toutes ressources confondues) : 30
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 31/01/2020 à 10h27
- Date de dernière mise à jour le 15/11/2022 à 09h17

## MAISON DE L'ESTUAIRE DE LA SEINE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data.maisondelestuaire.org
- Volumétrie : 19 jeux de données

## MALO AGGLO TRANSPORTS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ef0721566bea174e2d08b2c/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `gtfs-rt`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 15/02/2021 à 09h48
- Date de dernière mise à jour le 24/02/2022 à 11h02

## MANCHE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 50 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff67a3a7292c64a77d87
- Volumétrie : 35 jeux de données et 69 ressources
- Nombre de vues (tous jeux de données confondus) : 69
- Nombre de téléchargements (toutes ressources confondues) : 109
- Types de ressources disponibles : `html`, `odata`, `fr/tjeune/`, `pdf`, `fr/accueil`, `aspx#recherche`, `fr`, `csv`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h43
- Date de dernière mise à jour le 22/07/2016 à 11h22

## MANCHE NUMERIQUE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 30 jeux de données

## MANDRE LES ROSES

*Commune*

### Source **Plateforme territoriale**
- URL : https://mandres.data4citizen.com/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(mandres)
- Volumétrie : 7 jeux de données

## MARNES LA COQUETTE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55094e4cc751df601a882846
- Volumétrie : 9 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/03/2015 à 11h10
- Date de dernière mise à jour le 18/03/2015 à 11h22

## MARSEILLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-de-marseille
- Volumétrie : 35 jeux de données

## MARTIGUES

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-de-martigues
- Volumétrie : 15 jeux de données

## MAUGES COMMUNAUTE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.paysdelaloire.fr/explore/?disjunctive.publisher&sort=modified&refine.gestionnaire=Mauges+Communauté+
- Volumétrie : 8 jeux de données et 532 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4884
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 07/02/2019 à 16h13
- Date de dernière mise à jour le 20/08/2020 à 17h11

## MAULEON-LICHARRE

*Commune*
## MAYENNE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aeab860c751df3bca98b623/
- Volumétrie : 294 jeux de données et 853 ressources
- Nombre de vues (tous jeux de données confondus) : 55
- Nombre de téléchargements (toutes ressources confondues) : 16
- Types de ressources disponibles : `xlsx`, `document`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 23/07/2018 à 10h10
- Date de dernière mise à jour le 18/01/2022 à 18h57

## MAZINGARBE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb345d18b4c414c173fc6a5/
- Volumétrie : 8 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 21
- Nombre de téléchargements (toutes ressources confondues) : 15
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 09/10/2018 à 10h25
- Date de dernière mise à jour le 26/10/2018 à 13h07

## MEHARIN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/meharin/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/02/2020 à 14h01
- Date de dernière mise à jour le 31/07/2020 à 15h42

## MENDIONDE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/mendionde/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 16/06/2020 à 09h34
- Date de dernière mise à jour le 26/06/2020 à 11h05

## MENTON

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/commune-de-menton
- Volumétrie : 1 jeux de données

## MEOLANS-REVEL

*Commune*
## MERVILLE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Commune+de+Merville
- Volumétrie : aucun jeu de données

## METROPOLE AIX MARSEILLE PROVENCE

*Métropole*

### Source **Plateforme territoriale**
- URL : https://data.ampmetropole.fr/
- Volumétrie : 57 jeux de données

## METROPOLE DE LYON

*Métropole*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=M%C3%A9tropole%20de%20Lyon
- Volumétrie : 631 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff7aa3a7292c64a77e0c
- Volumétrie : 534 jeux de données et 2033 ressources
- Nombre de vues (tous jeux de données confondus) : 943
- Nombre de téléchargements (toutes ressources confondues) : 2487
- Types de ressources disponibles : `shp`, `geojson`, `csv`, `json`, `tif`, `zip`, `pdf`, `do?id=jorftext000036040528`, `xml`, `fr/se-deplacer/infos-trafic-accessibilite`, `net/`, `fr`, `fr/`, `com/o7q3o5skisezd5lk/arcgis/services/alrt3j_aura/wfsserver?service=wfs&request=getcapabilities`, `fr/geoserver/ind_aura/wfs?request=getcapabilities`, `org/fr/referentiel-technique-national`, `com/o7q3o5skisezd5lk/arcgis/services/mes_aura_horaire_poll_princ/wfsserver?service=wfs&request=getcapabilities`, `com/o7q3o5skisezd5lk/arcgis/services/stations_api/wfsserver?service=wfs&request=getcapabilities`, `html?appid=1412cc2ee1124dce97f85f98b9d6ac5c`, `com/o7q3o5skisezd5lk/arcgis/services/mes_aura_journalier_poll_princ/wfsserver?service=wfs&request=getcapabilities`, `fr/alerte-pollens`, `html`, `com`, `odt`, `pvoprevisiontraficjournee/`, `com/ws/grandlyon/`, `ecw`, `fr/cs/satellite?c=carto&cid=1278510477563&friendlyurl=null&pagename=lyon%2flayoutlyoncarto&equipement=null`, `xlsx`, `xls`, `ods`, `fr/comptabilite-des-spic-m4-1`, `fr/m14`, `fr/referentiel-budgetaire-et-comptable-m57`, `com/sos/bruit?request=getcapabilities&service=sos`, `org`, `do`, `com/sos/velov?request=getcapabilities&service=sos`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/06/2022 à 18h16
- Date de dernière mise à jour le 12/01/2023 à 00h06

## METROPOLE DU GRAND NANCY

*Métropole*

### Source **Plateforme territoriale**
- URL : http://opendata.grandnancy.eu/jeux-de-donnees/rechercher-un-jeu-de-donnees/
- Volumétrie : 50 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff65a3a7292c64a77d73
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 14/03/2018 à 17h13
- Date de dernière mise à jour le 06/12/2022 à 10h51

## METROPOLE EUROPEENNE DE LILLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=MEL
- Volumétrie : aucun jeu de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58b69c62c751df4b175dd7a4
- Volumétrie : 372 jeux de données et 1783 ressources
- Nombre de vues (tous jeux de données confondus) : 3687
- Nombre de téléchargements (toutes ressources confondues) : 2588
- Types de ressources disponibles : `csv`, `json`, `png`, `jpe`, `pdf`, `geojson`, `shp`, `xlsx`, `zip`, `xlb`, `ods`, `doc`, `gtfs-rt`, `docx`, `bmp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`, `Other (Public Domain)`
- Date de création : le 03/03/2017 à 05h00
- Date de dernière mise à jour le 08/12/2022 à 05h04

## METROPOLE NICE COTE D'AZUR

*Métropole*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/metropole-nice-cote-d-azur
- Volumétrie : 64 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59aea226c751df38f3f6834b/
- Volumétrie : 315 jeux de données et 2023 ressources
- Nombre de vues (tous jeux de données confondus) : 465
- Nombre de téléchargements (toutes ressources confondues) : 379
- Types de ressources disponibles : `geojson`, `csv`, `pdf`, `json`, `kmz`, `shp`, `xml`, `sbn`, `sbx`, `cpg`, `dbf`, `shx`, `prj`, `zip`, `dwg`, `ods`, `xls`, `xlsx`, `jpg`, `ecw`, `gpx`, `docx`, `odt`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 05/09/2017 à 15h32
- Date de dernière mise à jour le 16/11/2022 à 04h00

## METZ METROPOLE

*Métropole*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53fd1263a3a729390ba568a4
- Volumétrie : 58 jeux de données et 106 ressources
- Nombre de vues (tous jeux de données confondus) : 145
- Nombre de téléchargements (toutes ressources confondues) : 539
- Types de ressources disponibles : `csv`, `geojson`, `gtfs`, `shape`, `json`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`, `Other (Attribution)`
- Date de création : le 29/09/2014 à 16h15
- Date de dernière mise à jour le 11/01/2023 à 08h50

## MEUDON

*Commune*

### Source **OpenDataSoft**
- URL : https://data.meudon.fr/explore/?sort=title&refine.publisher=Ville+de+Meudon
- Volumétrie : 23 jeux de données et 1851 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 17396
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 03/04/2019 à 14h10
- Date de dernière mise à jour le 05/01/2023 à 09h53


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54d89442c751df1245467389
- Volumétrie : 23 jeux de données et 86 ressources
- Nombre de vues (tous jeux de données confondus) : 43
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 22/02/2021 à 09h56
- Date de dernière mise à jour le 23/09/2021 à 14h39

## MEURTHE ET MOSELLE

*Département*

### Source **Plateforme territoriale**
- URL : http://catalogue.infogeo54.fr/geonetwork/srv/fre/catalog.search#/home
- Volumétrie : 16 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bc74634634f4144ed36741c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/10/2018 à 21h41
- Date de dernière mise à jour le 11/01/2023 à 08h49

## MEZE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be01ff4634f4154a89ac27d/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/11/2018 à 15h53
- Date de dernière mise à jour le 08/08/2019 à 17h06

## MIONS

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Mions
- Volumétrie : 4 jeux de données

## MIREMONT

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d0a0c318b4c4108d1265df6/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `Other (Attribution)`
- Date de création : le 19/06/2019 à 14h18
- Date de dernière mise à jour le 19/06/2019 à 14h32

## MOGNENEINS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5662c125c751df42e605bd61
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 31/01/2017 à 20h43
- Date de dernière mise à jour le 08/01/2020 à 17h26

## MONACIA D'AULLENE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53ae7fc3a3a729709f56d4f4
- Volumétrie : 12 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 57
- Nombre de téléchargements (toutes ressources confondues) : 97
- Types de ressources disponibles : `xls`, `zip`, `pdf`, `csv`, `kmz`, `csv`, ` api`, `kml`, `gtfs`, `shp (wgs84)`, `geojson`, `xml`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Creative Commons CCZero`
- Date de création : le 28/06/2014 à 10h49
- Date de dernière mise à jour le 19/04/2017 à 17h15

## MONCAYOLLE-LARRORY-MENDIBIEU

*Commune*
## MONT DE MARSAN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e78cf325926be02a2e7473e/
- Volumétrie : aucun jeu de données

## MONTAUD

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montaud
- Volumétrie : 15 jeux de données

## MONTBELIARD

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c00089a8b4c414c75aed250/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/01/2019 à 14h41
- Date de dernière mise à jour le 05/05/2021 à 10h54

## MONTENDRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2ea6634f411541dbc634/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h17
- Date de dernière mise à jour le 14/03/2019 à 12h17

## MONTESQUIEU VOLVESTRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d1217448b4c4160c8be2597/
- Volumétrie : 9 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `txt`, `xml`, `xlsx`, `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 25/06/2019 à 15h00
- Date de dernière mise à jour le 21/04/2020 à 09h18

## MONTFERRIER SUR LEZ

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montferrier-sur-lez
- Volumétrie : 23 jeux de données

## MONTMAUR

*Commune*
## MONTOIR DE BRETAGNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Montoir+de+Bretagne
- Volumétrie : 2 jeux de données et 961 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2457
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 14/10/2021 à 08h32
- Date de dernière mise à jour le 12/01/2023 à 15h48

## MONTPELLIER

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montpellier
- Volumétrie : 105 jeux de données

## MONTPELLIER MEDITERRANEE METROPOLE

*Métropole*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montpellier-mediterran%C3%A9e-m%C3%A9tropole
- Volumétrie : 136 jeux de données

## MONTREUIL

*Commune*

### Source **OpenDataSoft**
- URL : https://data.montreuil.fr/explore/?refine.publisher=Ville+de+Montreuil
- Volumétrie : 96 jeux de données et 1438695 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 46824
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Domaine public`, `Open Database License (ODbL)`
- Date de création : le 25/04/2019 à 18h55
- Date de dernière mise à jour le 13/01/2023 à 03h02

## MORBIHAN

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c924c1c8b4c41159d78d937/
- Volumétrie : 20 jeux de données et 21 ressources
- Nombre de vues (tous jeux de données confondus) : 42
- Nombre de téléchargements (toutes ressources confondues) : 21
- Types de ressources disponibles : `pdf`, `php`, `csv`, `txt`, `shape`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 30/04/2019 à 17h01
- Date de dernière mise à jour le 18/01/2022 à 14h32

## MORBIHAN ENERGIES

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Morbihan+%C3%A9nergies
- Volumétrie : 7 jeux de données et 3507 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5418
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 08h15
- Date de dernière mise à jour le 08/01/2023 à 23h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/551d4b6cc751df4f1a0cd89f/
- Volumétrie : 7 jeux de données et 25 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 25/01/2019 à 12h01
- Date de dernière mise à jour le 11/01/2023 à 08h45

## MORBIHAN TOURISME

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56aa3aac88ee387ea98ffbc6/
- Volumétrie : aucun jeu de données

## MOSELLE

*Département*

### Source **Plateforme territoriale**
- URL : http://www.moselleopendata.fr/datas
- Volumétrie : 20 jeux de données

## MULHOUSE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Mulhouse
- Volumétrie : 46 jeux de données et 170330 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 72926
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 07/07/2017 à 09h02
- Date de dernière mise à jour le 05/01/2023 à 08h44

## MURVIEL LES MONTPELLIER

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/murviel-les-montpellier
- Volumétrie : 18 jeux de données

## MUSIQUE ET DANSE EN LOIRE ATLANTIQUE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Musique+et+Danse+en+Loire-Atlantique
- Volumétrie : 5 jeux de données et 413 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 944
- Types de licences utilisées : `Licence Ouverte-LO`
- Date de création : le 04/12/2012 à 00h00
- Date de dernière mise à jour le 08/11/2022 à 13h26


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff98a3a7292c64a78011/
- Volumétrie : 5 jeux de données et 20 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 258
- Types de ressources disponibles : `json`, `csv`, `xml`, `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h42
- Date de dernière mise à jour le 10/12/2013 à 14h47

## NANCY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a210bc888ee382bed1f31db/
- Volumétrie : 5 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 16
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/05/2018 à 09h35
- Date de dernière mise à jour le 06/01/2020 à 15h04

## NANDY

*Commune*

### Source **OpenDataSoft**
- URL : https://nandy-grandparissud.opendatasoft.com/explore/?sort=modified
- Volumétrie : 1 jeux de données et 1 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1046
- Date de création : le 10/10/2018 à 14h02
- Date de dernière mise à jour le 16/10/2018 à 14h12

## NANTERRE

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.nanterre.fr/1166-donnees-a-telecharger.htm
- Volumétrie : 9 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58ecb30388ee3855493567c6/
- Volumétrie : 9 jeux de données et 20 ressources
- Nombre de vues (tous jeux de données confondus) : 26
- Nombre de téléchargements (toutes ressources confondues) : 18
- Types de ressources disponibles : `zip`, `csv`, `pdf`, `png`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/04/2018 à 17h03
- Date de dernière mise à jour le 04/02/2021 à 15h27

## NANTES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Ville+de+Nantes
- Volumétrie : 222 jeux de données et 424154 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 148972
- Types de licences utilisées : `Licence Ouverte-LO`
- Date de création : le 13/12/2012 à 00h00
- Date de dernière mise à jour le 10/01/2023 à 08h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb9a3a7292c64a78159
- Volumétrie : 237 jeux de données et 574 ressources
- Nombre de vues (tous jeux de données confondus) : 580
- Nombre de téléchargements (toutes ressources confondues) : 591
- Types de ressources disponibles : `csv`, `json`, `pdf`, `geojson`, `shp`, `a`, `kml`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 18/07/2018 à 09h10
- Date de dernière mise à jour le 15/11/2022 à 08h00

## NANTES METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Nantes+Métropole
- Volumétrie : 190 jeux de données et 14983312 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6186412
- Types de licences utilisées : `Licence Ouverte-LO`, `Open Database License-ODBL`
- Date de création : le 15/11/2011 à 00h00
- Date de dernière mise à jour le 13/01/2023 à 03h38


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff98a3a7292c64a78012
- Volumétrie : 221 jeux de données et 789 ressources
- Nombre de vues (tous jeux de données confondus) : 2659
- Nombre de téléchargements (toutes ressources confondues) : 279
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `a`, `kml`, `zip`, `ods`, `htm`, `gtfs`, `kmz`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/07/2018 à 09h10
- Date de dernière mise à jour le 07/12/2022 à 01h13

## NEFFES

*Commune*
## NEUVILLE-SAINT-REMY

*Commune*
## NEVERS

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 11 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/594be11888ee38743d67051b
- Volumétrie : 16 jeux de données et 20 ressources
- Nombre de vues (tous jeux de données confondus) : 65
- Nombre de téléchargements (toutes ressources confondues) : 66
- Types de ressources disponibles : `csv`, `geojson`, `kml`, `zip`, `json`, `pdf`, `ods`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 23/06/2017 à 09h04
- Date de dernière mise à jour le 13/12/2022 à 15h07

## NEZEL

*Commune*

### Source **Plateforme territoriale**
- URL : https://gpseo.opendatasoft.com/explore/?disjunctive.theme&sort=explore.popularity_score&refine.publisher=Commune+de+Nézel
- Volumétrie : 3 jeux de données

## NICE

*Commune*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/ville-de-nice
- Volumétrie : 82 jeux de données

## NIEVRE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 38 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59367c42c751df5751fcc925
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 06/06/2017 à 12h07
- Date de dernière mise à jour le 10/01/2018 à 16h23

## NIMES METROPOLE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.nimes-metropole.fr/explore/?disjunctive.publisher&sort=explore.popularity_score
- Volumétrie : 34 jeux de données

## NIORT

*Commune*

### Source **Plateforme territoriale**
- URL : https://donneespubliques.niortagglo.fr/search?categories=opendata&collection=Dataset&sort=name&source=niort_agglo.data
- Volumétrie : 9 jeux de données

## NIORT AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://donneespubliques.niortagglo.fr/search?categories=opendata&collection=Dataset&sort=name&source=niort_agglo.data
- Volumétrie : 14 jeux de données

## NIOZELLES

*Commune*
## NOE

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.mairie-noe.fr?p=4260
- Volumétrie : 7 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ea3e8b88ee3877235ff490/
- Volumétrie : 7 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 21
- Types de ressources disponibles : `odt`, `doc`, `geojson`, `xls`, `docx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/10/2016 à 11h28
- Date de dernière mise à jour le 12/04/2017 à 23h18

## NOGENT SUR MARNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54ca2b22c751df5273467389
- Volumétrie : 10 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 03/03/2015 à 10h09
- Date de dernière mise à jour le 19/10/2018 à 10h55

## NOISIEL

*Commune*

### Source **Plateforme territoriale**
- URL : https://noisiel-agglo-pvm.opendata.arcgis.com
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/ville-de-noisiel/
- Volumétrie : 5 jeux de données et 24 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `web page`, `arcgis geoservices rest api`, `geojson`, `csv`, `zip`, `kml`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 20/08/2022 à 02h06
- Date de dernière mise à jour le 20/08/2022 à 02h06

## NORD

*Département*

### Source **Plateforme territoriale**
- URL : https://geonord-lenord.opendata.arcgis.com
- Volumétrie : 7 jeux de données

## NORMANDIE

*Région*

### Source **Plateforme territoriale**
- URL : https://opendata.normandie.fr/search
- Volumétrie : 38 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/region-normandie/
- Volumétrie : 29 jeux de données et 46 ressources
- Nombre de vues (tous jeux de données confondus) : 213
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 19/11/2021 à 10h15
- Date de dernière mise à jour le 15/10/2022 à 02h01

## NOUMEA

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.gouv.nc/explore/?disjunctive.theme&disjunctive.publisher&disjunctive.keyword&disjunctive.attributions&disjunctive.license&sort=explore.popularity_score&refine.publisher=Ville+de+Nouméa
- Volumétrie : 14 jeux de données

## NOUVELLE AQUITAINE

*Région*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff6aa3a7292c64a77d95
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 10
- Types de ressources disponibles : `xml`, `csv`
- Types de licences utilisées : `Creative Commons Attribution Share-Alike`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 18/09/2013 à 08h47

## NOUVELLE AQUITAINE PIGMA

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset?organization=pigma
- Volumétrie : 255 jeux de données

## NOUVELLE AQUITAINE TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.datalocale.fr/dataset?organization=sirtaqui
- Volumétrie : 87 jeux de données

## NOUVELLE CALEDONIE GOUVERNEMENT

*Région*

### Source **Plateforme territoriale**
- URL : https://data.gouv.nc/explore/?disjunctive.theme&disjunctive.publisher&disjunctive.keyword&disjunctive.attributions&disjunctive.license&sort=explore.popularity_score&refine.publisher=Gouvernement+de+la+Nouvelle-Calédonie
- Volumétrie : 156 jeux de données

## NOUVELLE-AQUITAINE MOBILITES

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset?organization=nouvelle_aquitaine_mobilit_s
- Volumétrie : 37 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c52f0cc8b4c410915918eee/
- Volumétrie : 79 jeux de données et 156 ressources
- Nombre de vues (tous jeux de données confondus) : 125
- Nombre de téléchargements (toutes ressources confondues) : 144
- Types de ressources disponibles : `http-link`, `netex`, `gtfs`, `site internet`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`
- Date de création : le 25/05/2019 à 02h01
- Date de dernière mise à jour le 31/03/2022 à 00h00

## NOYAL CHATILLON SUR SEICHE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Noyal-Ch%C3%A2tillon-sur-Seiche
- Volumétrie : 6 jeux de données et 132 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5262
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 16/01/2018 à 14h33
- Date de dernière mise à jour le 19/02/2021 à 08h53

## NOYAL PONTIVY

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Noyal-Pontivy
- Volumétrie : 1 jeux de données et 10 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 22
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h46
- Date de dernière mise à jour le 07/04/2021 à 07h50


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a5cb495c751df2cdac7334e/
- Volumétrie : 1 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 15/01/2018 à 15h42
- Date de dernière mise à jour le 15/01/2018 à 15h42

## OCCITANIE

*Région*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?refine.publisher=R%C3%A9gion+Occitanie
- Volumétrie : 133 jeux de données et 948637 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 454109
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte (Etalab)`
- Date de création : le 31/12/2016 à 10h13
- Date de dernière mise à jour le 13/01/2023 à 02h58

## OCTEVILLE SUR MER

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.agglo-lehavre.fr
- Volumétrie : 3 jeux de données

## OFFICE DE TOURISME METROPOLITAIN NICE COTE D'AZUR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/office-de-tourisme-metropolitain-nice-cote-d-azur
- Volumétrie : 10 jeux de données

## OISE

*Département*

### Source **Plateforme territoriale**
- URL : http://opendata.oise.fr/donnees/
- Volumétrie : 125 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff67a3a7292c64a77d7d
- Volumétrie : 54 jeux de données et 181 ressources
- Nombre de vues (tous jeux de données confondus) : 105
- Nombre de téléchargements (toutes ressources confondues) : 561
- Types de ressources disponibles : `pdf`, `doc`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 30/05/2014 à 05h57

## ONDEA LAC DU BOURGET

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be2dd80634f41472fc9de00/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `gtfs-rt`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 17/02/2021 à 11h13
- Date de dernière mise à jour le 14/03/2022 à 10h03

## ONET LE CHATEAU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aec4c3e88ee381426c8df86/
- Volumétrie : 7 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 04/05/2018 à 14h14
- Date de dernière mise à jour le 15/11/2018 à 09h54

## ORCIERES

*Commune*
## ORECAN (OFF. REG. ENRG. CLIM. AIR NORM.)

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data.metropole-rouen-normandie.fr/explore/?stage_theme=true&sort=modified&refine.publisher=ORECAN
- Volumétrie : 1 jeux de données

## ORLEANS METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.orleans-metropole.fr/explore/?refine.publisher=Orl%C3%A9ans+M%C3%A9tropole
- Volumétrie : 95 jeux de données et 545919 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 211262
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 14/09/2018 à 14h20
- Date de dernière mise à jour le 13/01/2023 à 03h05


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ed2fe9c751df236179df72
- Volumétrie : 334 jeux de données et 1202 ressources
- Nombre de vues (tous jeux de données confondus) : 1399
- Nombre de téléchargements (toutes ressources confondues) : 297
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `a`, `xlsx`, `htm`, `pdf`, `bat`, `xlb`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`, `Other (Public Domain)`
- Date de création : le 10/01/2019 à 18h42
- Date de dernière mise à jour le 05/01/2023 à 01h05

## ORSANCO

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/commune-dorsanco/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 07/06/2019 à 15h43
- Date de dernière mise à jour le 30/07/2020 à 11h16

## ORVAULT

*Commune*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Orvault
- Volumétrie : aucun jeu de données

## OSSERAIN-RIVAREYTE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/osserain-rivareyte/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/06/2019 à 15h49
- Date de dernière mise à jour le 30/07/2020 à 11h19

## OTTMARSHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Comme+d%27Ottmarsheim
- Volumétrie : 1 jeux de données et 1025 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 482
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 31/01/2018 à 16h04
- Date de dernière mise à jour le 12/09/2019 à 21h48

## OZE

*Commune*
## PALMBUS CA CANNES PAYS DE LERINS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c65a43a634f41156b2de37e/
- Volumétrie : 2 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `gtfs-rt`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/11/2021 à 17h54
- Date de dernière mise à jour le 11/01/2023 à 08h50

## PARIS

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.paris.fr/explore/?sort=modified
- Volumétrie : aucun jeu de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff89a3a7292c64a77eb7
- Volumétrie : 751 jeux de données et 3126 ressources
- Nombre de vues (tous jeux de données confondus) : 8067
- Nombre de téléchargements (toutes ressources confondues) : 7814
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `xlsx`, `pdf`, `zip`, `xlb`, `gbfs`, `htm`, `html`, `docx`, `obj`, `a`, `xls`, `doc`, `ods`, `map`, `odt`, `txt`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h36
- Date de dernière mise à jour le 12/01/2023 à 05h02

## PARIS VALLEE DE MARNE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data-agglo-pvm.opendata.arcgis.com
- Volumétrie : 6 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/communaute-dagglomeration-paris-vallee-de-la-marne/
- Volumétrie : 15 jeux de données et 64 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `geojson`, `arcgis geoservices rest api`, `web page`, `kml`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 22/07/2022 à 02h03
- Date de dernière mise à jour le 24/11/2022 à 10h49

## PAS DE CALAIS TOURISME

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://tourisme62.opendatasoft.com/explore/?refine.publisher=Comité+départemental+de+tourisme+du+Pas-de-Calais
- Volumétrie : 44 jeux de données et 116432 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 155186
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/04/2015 à 11h47
- Date de dernière mise à jour le 13/01/2023 à 03h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa2a3a7292c64a7808d/
- Volumétrie : 30 jeux de données et 117 ressources
- Nombre de vues (tous jeux de données confondus) : 18
- Nombre de téléchargements (toutes ressources confondues) : 88
- Types de ressources disponibles : `csv`, `json`, `xls`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 04h09
- Date de dernière mise à jour le 07/01/2014 à 14h54

## PAYS DE LA LOIRE

*Région*

### Source **OpenDataSoft**
- URL : https://data.paysdelaloire.fr/explore/?refine.publisher=Région+des+Pays+de+la+Loire
- Volumétrie : 57 jeux de données et 106759 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 333948
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 04/10/2017 à 00h00
- Date de dernière mise à jour le 12/01/2023 à 23h04


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa7a3a7292c64a780c5
- Volumétrie : 432 jeux de données et 1802 ressources
- Nombre de vues (tous jeux de données confondus) : 759
- Nombre de téléchargements (toutes ressources confondues) : 353
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `.zip`, `pdf`, `docx`, `zip`, `gtfs.zip`, `.gtfs`, `xlsx`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 16/07/2018 à 19h18
- Date de dernière mise à jour le 21/12/2022 à 01h06

## PAYS DE SAINT BRIEUC

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 25 jeux de données

## PAYS SERRE-PONCON UBAYE DURANCE

*Communauté de communes*
## PEILLAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b7ea6c3634f4119e42b781b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Other (Public Domain)`
- Date de création : le 24/08/2018 à 08h46
- Date de dernière mise à jour le 24/08/2018 à 08h47

## PEROLS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/p%C3%A9rols
- Volumétrie : 28 jeux de données

## PESSAC

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Ville+de+Pessac
- Volumétrie : 2 jeux de données et 516 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 27
- Types de licences utilisées : `Licence Ouverte`
- Date de création : le 04/09/2019 à 13h58
- Date de dernière mise à jour le 19/01/2021 à 17h53

## PETIT COURONNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b90dc4d634f415369256e17/
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `jpg`, `pdf`, `docx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 06/09/2018 à 12h11
- Date de dernière mise à jour le 06/09/2018 à 15h30

## PETR PAYS MIDI QUERCY

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/594d23e688ee380a6acd18af/
- Volumétrie : 8 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 23
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `xls`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 23/06/2017 à 16h02
- Date de dernière mise à jour le 19/07/2017 à 16h07

## PICARDIE NATURE

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54e3c4f1c751df7e3e467389/
- Volumétrie : 6 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 33
- Nombre de téléchargements (toutes ressources confondues) : 25
- Types de ressources disponibles : `csv`, `kml`, `html`
- Types de licences utilisées : `Creative Commons Attribution`, `Creative Commons Attribution Share-Alike`
- Date de création : le 21/04/2016 à 10h04
- Date de dernière mise à jour le 30/06/2016 à 17h55

## PIGNAN

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/pignan
- Volumétrie : 27 jeux de données

## PIRAE

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.pirae.pf/opendata/
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56bac12ec751df6c539f9dea
- Volumétrie : 16 jeux de données et 1106 ressources
- Nombre de vues (tous jeux de données confondus) : 101
- Nombre de téléchargements (toutes ressources confondues) : 90
- Types de ressources disponibles : `csv`, `pdf`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 24/10/2016 à 04h44
- Date de dernière mise à jour le 08/12/2019 à 21h26

## PLAINTEL

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/588f0a98c751df12c6ae0a66
- Volumétrie : 2 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 31/01/2017 à 15h59
- Date de dernière mise à jour le 08/02/2017 à 11h48

## PLERIN

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 6 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/584ed32c88ee38733fc65bb3
- Volumétrie : 6 jeux de données et 30 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 20/02/2017 à 11h53
- Date de dernière mise à jour le 09/10/2018 à 17h30

## PLESSE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1fde6d88ee384f0ff312b0
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 30/11/2017 à 13h59
- Date de dernière mise à jour le 30/11/2017 à 13h59

## PLESSIS TREVISE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb4ae00634f4155bc71f71f/
- Volumétrie : 7 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 09/10/2018 à 17h17
- Date de dernière mise à jour le 19/02/2020 à 08h47

## PLEUMELEUC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b1a788188ee3843648b36b6/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 16h58
- Date de dernière mise à jour le 27/10/2020 à 08h25

## PLOEUC L'HERMITAGE

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/donnees
- Volumétrie : 1 jeux de données

## PLOEZAL

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 5 jeux de données

## PLOUFRAGAN

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/donnees
- Volumétrie : 7 jeux de données

## PNR DU MORVAN

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/sm-du-parc-naturel-regional-du-morvan
- Volumétrie : 17 jeux de données

## PNR MILLEVACHES LIMOUSIN

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset?organization=pnr_millevaches_en_limousin
- Volumétrie : 8 jeux de données

## POITIERS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.grandpoitiers.fr/explore/?refine.publisher=Ville+de+Poitiers
- Volumétrie : 56 jeux de données et 134203 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 63868
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/08/2016 à 14h50
- Date de dernière mise à jour le 13/01/2023 à 03h41

## POLE METROPOLITAIN DE L'ESTUAIRE DE LA SEINE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 5 jeux de données

## POLE METROPOLITAIN DU PAYS DE BREST

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://opendata.brest-metropole.fr/explore/?sort=modified&refine.publisher=Pôle+métropolitain+du+Pays+de+Brest
- Volumétrie : 25 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5adda58dc751df45e10ca319/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `shp`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 23/04/2018 à 11h36
- Date de dernière mise à jour le 14/09/2022 à 09h24

## POLIGNY (05)

*Commune*
## PONT DE CLAIX

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.metropolegrenoble.fr/ckan/dataset?organization=ville-de-pont-de-claix
- Volumétrie : 3 jeux de données

## PONT L'ABBE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58da32bac751df47f26f37b1
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/03/2017 à 16h06
- Date de dernière mise à jour le 25/04/2018 à 11h42

## PONTAULT-COMBAULT

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.pontault-combault.fr
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/ville-de-pontault-combault/
- Volumétrie : 7 jeux de données et 32 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `web page`, `arcgis geoservices rest api`, `geojson`, `kml`, `zip`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 20/08/2022 à 02h06
- Date de dernière mise à jour le 20/08/2022 à 02h06

## PORNICHET

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?sort=modified&disjunctive.gestionnaire&disjunctive.publisher&disjunctive.theme&disjunctive.keyword&disjunctive.license&disjunctive.features&disjunctive.diffuseur&refine.dcat.spatial=CARENE&disjunctive.dcat.spatial&refine.publisher=Pornichet
- Volumétrie : 2 jeux de données et 4069 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1106
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 27/11/2020 à 07h20
- Date de dernière mise à jour le 12/01/2023 à 16h29

## PRADES LE LEZ

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/prades-le-lez
- Volumétrie : 25 jeux de données

## PROVENCE TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/provence-tourisme
- Volumétrie : 9 jeux de données

## PROVINCE DES ILES LOYAUTE - NOUVELLE CALEDONIE

*Département*

### Source **Plateforme territoriale**
- URL : https://data.gouv.nc/explore/?disjunctive.theme&disjunctive.publisher&disjunctive.keyword&disjunctive.attributions&disjunctive.license&sort=explore.popularity_score&refine.publisher=Province+des+Iles+Loyauté
- Volumétrie : 3 jeux de données

## PROVINCE NORD - NOUVELLE CALEDONIE

*Département*

### Source **Plateforme territoriale**
- URL : https://data.gouv.nc/explore/?disjunctive.theme&disjunctive.publisher&disjunctive.keyword&disjunctive.attributions&disjunctive.license&sort=explore.popularity_score&refine.publisher=Province+Nord
- Volumétrie : 6 jeux de données

## PROVINCE SUD - NOUVELLE CALEDONIE

*Département*

### Source **Plateforme territoriale**
- URL : https://data.gouv.nc/explore/?disjunctive.theme&disjunctive.publisher&disjunctive.keyword&disjunctive.attributions&disjunctive.license&sort=explore.popularity_score&refine.publisher=Province+Sud
- Volumétrie : 13 jeux de données

## PUGET VILLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-puget-ville
- Volumétrie : 8 jeux de données

## PUTEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?sort=modified&refine.publisher=Ville+de+Puteaux
- Volumétrie : 2 jeux de données et 100 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 11599
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 19/09/2019 à 08h46
- Date de dernière mise à jour le 25/09/2019 à 08h42

## PUY DE DOME

*Département*

### Source **Plateforme territoriale**
- URL : http://www.puy-de-dome.fr/opendata.html
- Volumétrie : 24 jeux de données

## PUY SANIERES

*Commune*
## PUY-SAINT-EUSEBE

*Commune*
## PYRENEES ATLANTIQUES

*Département*

### Source **OpenDataSoft**
- URL : https://data.le64.fr/explore/?refine.publisher=D%C3%A9partement+des+Pyr%C3%A9n%C3%A9es-Atlantiques
- Volumétrie : 39 jeux de données et 62580 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 23881
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 04/07/2017 à 14h48
- Date de dernière mise à jour le 12/01/2023 à 07h00

## PYRENEES ORIENTALES

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab9f242c751df2f936031d6
- Volumétrie : 14 jeux de données et 22 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 49
- Types de ressources disponibles : `zip`, `csv`, `ods`, `pdf`
- Types de licences utilisées : `License Not Specified`, `Other (Public Domain)`
- Date de création : le 27/03/2018 à 10h31
- Date de dernière mise à jour le 12/07/2022 à 11h48

## QUALITAIR CORSE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-qualitaircorse.opendata.arcgis.com/datasets
- Volumétrie : 106 jeux de données

## QUIBERON

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Ville+de+Quiberon
- Volumétrie : aucun jeu de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5dc56e0e8b4c412e7cd726a2/
- Volumétrie : 2 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/11/2019 à 16h50
- Date de dernière mise à jour le 15/12/2020 à 14h56

## RAMBOUILLET

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c40559d8b4c4151b275a7b0/
- Volumétrie : 7 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/04/2019 à 15h24
- Date de dernière mise à jour le 20/01/2020 à 10h51

## RANGECOURT

*Commune*
## RATP

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.ratp.fr/explore/?refine.publisher=RATP
- Volumétrie : 19 jeux de données et 285343 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 98908
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence RATP http://data.ratp.fr/page/cgu_ratp`, `Licence ODbL Version Française`
- Date de création : le 03/02/2015 à 11h48
- Date de dernière mise à jour le 10/01/2023 à 08h50


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa6a3a7292c64a780b7/
- Volumétrie : 46 jeux de données et 157 ressources
- Nombre de vues (tous jeux de données confondus) : 596
- Nombre de téléchargements (toutes ressources confondues) : 920
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `htm`, `zip`, `xls`, `autre`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 03/12/2022 à 05h00

## REDON

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1e73b0c751df0d9952bd2d
- Volumétrie : 5 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/11/2017 à 14h11
- Date de dernière mise à jour le 20/04/2018 à 15h50

## REGIE DES TRANSPORTS CARCASONNE AGGLO

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c17b4d7634f41734de371f7/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 29/06/2022 à 12h25
- Date de dernière mise à jour le 09/01/2023 à 09h44

## REGIE DES TRANSPORTS DU TERRITOIRE DE BELFORT

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b11425c88ee387510d9dcfb/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `json`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 24/12/2018 à 09h18
- Date de dernière mise à jour le 14/12/2022 à 10h07

## REGIE LIGNES D'AZUR

*Délégataire de service public*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/regie-ligne-d-azur
- Volumétrie : 5 jeux de données

## REIMS

*Commune*
## RENNES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Rennes
- Volumétrie : 152 jeux de données et 504131 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 273502
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence ODbL 1.0`, `Licence ODbL 1.0
http://www.data.rennes-metropole.fr/notre-demarche/licence-odbl-ville-de-rennes-et-rennes-metropole/`
- Date de création : le 05/04/2011 à 09h22
- Date de dernière mise à jour le 13/01/2023 à 01h01


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb9a3a7292c64a7815d
- Volumétrie : 40 jeux de données et 82 ressources
- Nombre de vues (tous jeux de données confondus) : 18
- Nombre de téléchargements (toutes ressources confondues) : 227
- Types de ressources disponibles : `xls`, `txt`, `csv`, `api`, `html`, `rss`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Other (Attribution)`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 14/11/2013 à 13h32

## RENNES METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Rennes+M%C3%A9tropole
- Volumétrie : 181 jeux de données et 1929325 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3814624
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence ODbL 1.0`, `Licence ODbL 1.0
http://www.data.rennes-metropole.fr/notre-demarche/licence-odbl-ville-de-rennes-et-rennes-metropole/`, `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte https://www.etalab.gouv.fr/licence-ouverte-open-licence`
- Date de création : le 01/05/2005 à 10h00
- Date de dernière mise à jour le 13/01/2023 à 03h43


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffaaa3a7292c64a780dd
- Volumétrie : 66 jeux de données et 143 ressources
- Nombre de vues (tous jeux de données confondus) : 495
- Nombre de téléchargements (toutes ressources confondues) : 524
- Types de ressources disponibles : `json`, `shp`, `gpkg`, `csv`, `xls`, `txt`, `xml`, `rtf`, `api`, `html`, `rss`, `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `Other (Attribution)`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 20/01/2021 à 06h38

## RESTINCLIERES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/restincli%C3%A8res
- Volumétrie : 13 jeux de données

## RETJONS

*Commune*
## RGD SAVOIE MONT BLANC

*Organisme associé de collectivité territoriale*
## RIEDISHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Riedisheim
- Volumétrie : 3 jeux de données et 9938 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2291
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/11/2017 à 08h26
- Date de dernière mise à jour le 12/09/2019 à 21h48

## RILLIEUX LA PAPE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Rillieux-la-Pape
- Volumétrie : 5 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d1b06b46f44410e40f553ac/
- Volumétrie : 7 jeux de données et 26 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `shp`, `geojson`, `csv`, `json`, `fr/`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 20/12/2021 à 15h44
- Date de dernière mise à jour le 12/01/2023 à 00h05

## RIS ORANGIS

*Commune*

### Source **Plateforme territoriale**
- URL : https://risorangis-grandparissud.opendatasoft.com/explore/?sort=modified
- Volumétrie : 5 jeux de données

## RIXHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Rixheim
- Volumétrie : 5 jeux de données et 128 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3616
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/01/2018 à 10h27
- Date de dernière mise à jour le 12/09/2019 à 21h48

## ROCHEBRUNE

*Commune*
## ROCHETTE(05)

*Commune*
## RODEZ

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.ville-rodez.fr/fr/outils/catalogue-open-data.php
- Volumétrie : 7 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ff946788ee384fa45ff490
- Volumétrie : 7 jeux de données et 40 ressources
- Nombre de vues (tous jeux de données confondus) : 115
- Nombre de téléchargements (toutes ressources confondues) : 114
- Types de ressources disponibles : `csv`, `json`, `xls`, `pdf`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 13/10/2016 à 16h38
- Date de dernière mise à jour le 11/06/2018 à 15h37

## ROISSY-EN-BRIE

*Commune*
## ROQUEBRUNE CAP MARTIN

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-roquebrune-cap-martin
- Volumétrie : 3 jeux de données

## ROQUES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Commune+de+Roques
- Volumétrie : aucun jeu de données

## ROSCOFF

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab12b6ec751df128414f914/
- Volumétrie : 3 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 21/03/2018 à 14h44
- Date de dernière mise à jour le 21/09/2018 à 19h28

## ROSNY SOUS BOIS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b28f5b288ee38047e62143e/
- Volumétrie : 6 jeux de données et 84 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 19/07/2018 à 15h16
- Date de dernière mise à jour le 25/11/2022 à 11h16

## ROSNY-SUR-SEINE

*Commune*

### Source **Plateforme territoriale**
- URL : https://gpseo.opendatasoft.com/explore/?disjunctive.theme&sort=explore.popularity_score&refine.publisher=Commune+de+Rosny-sur-Seine
- Volumétrie : 5 jeux de données

## ROUBAIX

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.roubaix.fr/explore/?refine.publisher=Ville+de+Roubaix
- Volumétrie : 117 jeux de données et 260253 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 290860
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 26/07/2016 à 12h56
- Date de dernière mise à jour le 12/01/2023 à 16h35


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be53f55634f4119ca26ec72/
- Volumétrie : 173 jeux de données et 666 ressources
- Nombre de vues (tous jeux de données confondus) : 42494
- Nombre de téléchargements (toutes ressources confondues) : 211
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`, `png`, `xlsx`, `zip`, `doc`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 13/11/2018 à 12h32
- Date de dernière mise à jour le 24/12/2022 à 01h12

## ROUEN

*Commune*

### Source **OpenDataSoft**
- URL : https://data.metropole-rouen-normandie.fr/explore/?stage_theme=true&sort=modified&refine.publisher=Ville+de+Rouen
- Volumétrie : 5 jeux de données et 1121 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 81
- Types de licences utilisées : `Licence Ouverte v2.0`
- Date de création : le 13/06/2022 à 08h35
- Date de dernière mise à jour le 11/01/2023 à 06h28

## ROUEN NORMANDIE METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.metropole-rouen-normandie.fr/explore/?stage_theme=true&sort=modified&refine.publisher=Métropole+Rouen+Normandie
- Volumétrie : 43 jeux de données et 678689 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6600
- Types de licences utilisées : `Licence Ouverte v2.0`, `lov2`
- Date de création : le 13/06/2022 à 07h42
- Date de dernière mise à jour le 12/01/2023 à 14h00

## RUEIL MALMAISON

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?sort=modified&refine.publisher=Ville+de+Rueil-Malmaison
- Volumétrie : 24 jeux de données et 776 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 60673
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 06/01/2020 à 08h52
- Date de dernière mise à jour le 13/01/2023 à 03h44

## SAEMES

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://opendata.saemes.fr/explore/?refine.publisher=Saemes
- Volumétrie : 13 jeux de données et 891 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 7495210
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 08/03/2017 à 15h16
- Date de dernière mise à jour le 13/01/2023 à 03h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56f54580c751df65ebc485cb/
- Volumétrie : 18 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 91
- Nombre de téléchargements (toutes ressources confondues) : 38
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/03/2018 à 07h05
- Date de dernière mise à jour le 25/10/2022 à 13h41

## SAINT ANDRE DE CUBZAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff66a3a7292c64a77d79
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 26
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 04h22
- Date de dernière mise à jour le 18/09/2013 à 08h47

## SAINT ANDRE DES EAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint-André+des+Eaux
- Volumétrie : 2 jeux de données et 360 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3937
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/05/2019 à 14h13
- Date de dernière mise à jour le 12/01/2023 à 11h05

## SAINT APOLLINAIRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aec1fe488ee384cbcf3befe/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 04/05/2018 à 11h11
- Date de dernière mise à jour le 04/05/2018 à 11h22

## SAINT AUBAN D'OZE

*Commune*
## SAINT AUBIN LES ELBEUF

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.metropole-rouen-normandie.fr/explore/?stage_theme=true&sort=modified&refine.publisher=Ville+de+Saint-Aubin-lès-Elbeuf
- Volumétrie : 5 jeux de données

## SAINT AVE

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Saint-Av%C3%A9
- Volumétrie : 8 jeux de données et 2409 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 220
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h49
- Date de dernière mise à jour le 07/04/2021 à 08h56


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aa79c9fc751df144f78f40c/
- Volumétrie : 12 jeux de données et 26 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `xlsx`, `csv`, `kml`, `html`, `odata`, `xml`, `json`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 28/03/2018 à 17h23
- Date de dernière mise à jour le 16/03/2022 à 17h45

## SAINT BONNET LE BOURG

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c59b6498b4c4157a35c3880/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 05/02/2019 à 17h25
- Date de dernière mise à jour le 05/02/2019 à 17h25

## SAINT BRES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-br%C3%A8s
- Volumétrie : 16 jeux de données

## SAINT BRIEUC

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 9 jeux de données

## SAINT CLAUDE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 7 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b06810bc751df6e997fd802/
- Volumétrie : 27 jeux de données et 22 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `xlsx`, `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 10/08/2018 à 09h51
- Date de dernière mise à jour le 15/02/2022 à 15h42

## SAINT CYR AU MONT D'OR

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Saint-Cyr-au-Mont-d%27Or
- Volumétrie : 4 jeux de données

## SAINT CYR SUR MER

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c4973cf8b4c410839f61188/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 06/02/2019 à 18h06
- Date de dernière mise à jour le 06/02/2019 à 18h07

## SAINT DENIS DE LA REUNION

*Commune*

### Source **Plateforme territoriale**
- URL : http://opendata-sig.saintdenis.re/datasets
- Volumétrie : 54 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c050cfb8b4c413be36cb1c3/
- Volumétrie : 275 jeux de données et 2139 ressources
- Nombre de vues (tous jeux de données confondus) : 319
- Nombre de téléchargements (toutes ressources confondues) : 31
- Types de ressources disponibles : `web page`, `arcgis geoservices rest api`, `geojson`, `csv`, `kml`, `zip`, `esri rest`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/01/2019 à 13h14
- Date de dernière mise à jour le 20/12/2022 à 01h11

## SAINT DIDIER AU MONT D'OR

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Saint-Didier-au-Mont-d%27Or
- Volumétrie : 6 jeux de données

## SAINT DREZERY

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-dr%C3%A9z%C3%A9ry
- Volumétrie : 21 jeux de données

## SAINT EGREVE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=ville-de-saint-egreve
- Volumétrie : 10 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c002da8634f4111d7393309/
- Volumétrie : 10 jeux de données et 42 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 15
- Types de ressources disponibles : `csv`, `json`, `kml`, `shp`, `pdf`, `geojson`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/05/2018 à 09h09
- Date de dernière mise à jour le 16/11/2022 à 01h01

## SAINT ESTEBEN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ddfeff56f44411ac532190f/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/11/2019 à 17h11
- Date de dernière mise à jour le 30/07/2020 à 11h47

## SAINT ETIENNE DE BAIGORRY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e81f70c1dd3db2b3165ec8d/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 30/03/2020 à 15h47
- Date de dernière mise à jour le 30/03/2020 à 15h50

## SAINT ETIENNE METROPOLE

*Métropole*

### Source **Plateforme territoriale**
- URL : https://data.saint-etienne-metropole.fr/portail/catalogue
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c34c63a8b4c41718c8b236f/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 26
- Nombre de téléchargements (toutes ressources confondues) : 14
- Types de ressources disponibles : `geojson`, `json`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 17/11/2020 à 10h25
- Date de dernière mise à jour le 26/08/2022 à 16h52

## SAINT GENIES DES MOURGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-geni%C3%A8s-des-mourgues
- Volumétrie : 19 jeux de données

## SAINT GEORGES D'ORQUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-georges-dorques
- Volumétrie : 21 jeux de données

## SAINT GEORGES DE DIDONNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2ec7634f411c56990637/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h13
- Date de dernière mise à jour le 14/03/2019 à 12h13

## SAINT GERMAIN EN LAYE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d123361634f4145bb4131c8/
- Volumétrie : 9 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 20
- Nombre de téléchargements (toutes ressources confondues) : 18
- Types de ressources disponibles : `csv`, `pdf`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 26/06/2019 à 11h14
- Date de dernière mise à jour le 11/01/2023 à 08h42

## SAINT GONLAY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3494ef88ee382f8e126555/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h41
- Date de dernière mise à jour le 28/06/2018 à 11h17

## SAINT JACQUES DE LA LANDE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Saint-Jacques+de+la+Lande
- Volumétrie : 11 jeux de données et 2003 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 15883
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 01/02/2018 à 15h23
- Date de dernière mise à jour le 17/05/2019 à 13h51

## SAINT JEAN DE VEDAS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-jean-de-vedas
- Volumétrie : 23 jeux de données

## SAINT JOACHIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint-Joachim
- Volumétrie : 2 jeux de données et 846 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3206
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 03/06/2021 à 14h42
- Date de dernière mise à jour le 12/01/2023 à 13h00

## SAINT JULIEN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e1708dd634f416746ce7892/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 09/01/2020 à 12h21
- Date de dernière mise à jour le 09/01/2020 à 12h26

## SAINT JUST LUZAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2e8e634f4118ee8aeee2/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h11
- Date de dernière mise à jour le 14/03/2019 à 12h14

## SAINT LAURENT DE MURE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae1cc2588ee385be7b19c67/
- Volumétrie : 2 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `pdf`, `xlsx`, `xls`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 26/04/2018 à 16h59
- Date de dernière mise à jour le 26/12/2022 à 10h09

## SAINT LOUBES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6686958b4c4112712e3f6c/
- Volumétrie : 4 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `open data gironde numérique`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 03/04/2019 à 11h29
- Date de dernière mise à jour le 07/05/2021 à 18h19

## SAINT MALO

*Commune*

### Source **OpenDataSoft**
- URL : https://data.stmalo-agglomeration.fr/explore/?refine.publisher=Ville+de+Saint-Malo
- Volumétrie : 52 jeux de données et 13604 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 58370
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/10/2014 à 10h10
- Date de dernière mise à jour le 10/01/2023 à 23h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53ba4599a3a729219b7beaca
- Volumétrie : 50 jeux de données et 131 ressources
- Nombre de vues (tous jeux de données confondus) : 62297
- Nombre de téléchargements (toutes ressources confondues) : 2267
- Types de ressources disponibles : `csv`, `shp`, `zip`, `kml`, `shp (l93)`, `gpx`, `ods`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/07/2014 à 00h11
- Date de dernière mise à jour le 31/08/2016 à 10h02

## SAINT MALO DE GUERSAC

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint+Malo+de+Guersac
- Volumétrie : 2 jeux de données et 524 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3656
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/07/2020 à 15h07
- Date de dernière mise à jour le 12/01/2023 à 15h00

## SAINT MANDE

*Commune*

### Source **OpenDataSoft**
- URL : https://saintmande.opendatasoft.com/explore/?refine.publisher=Ville+de+Saint-Mand%C3%A9
- Volumétrie : 3 jeux de données et 340 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 62
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 05/10/2017 à 09h39
- Date de dernière mise à jour le 08/11/2021 à 11h24

## SAINT MARCEL

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bab855f634f417eb0f01da8/
- Volumétrie : 3 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 28/09/2018 à 11h00
- Date de dernière mise à jour le 03/12/2019 à 10h20

## SAINT MARTIN D'HERES

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.metropolegrenoble.fr/ckan/dataset?organization=ville-de-saintmartindheres
- Volumétrie : 14 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e2972146f444147e0a30177/
- Volumétrie : 24 jeux de données et 74 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `csv`, `json`, `kml`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/09/2020 à 14h53
- Date de dernière mise à jour le 16/11/2022 à 01h14

## SAINT MAUR DES FOSSES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffada3a7292c64a780f9
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `kml`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 04h04
- Date de dernière mise à jour le 23/11/2013 à 10h56

## SAINT MOLF

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Saint+Molf
- Volumétrie : 1 jeux de données et 36 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1386
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/07/2020 à 04h00
- Date de dernière mise à jour le 26/02/2021 à 05h00

## SAINT NAZAIRE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint-Nazaire
- Volumétrie : 22 jeux de données et 39731 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 26377
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 22/08/2018 à 07h31
- Date de dernière mise à jour le 12/01/2023 à 23h00

## SAINT NICOLAS DE REDON

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2023c388ee383e1dea3b3f
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/11/2017 à 16h34
- Date de dernière mise à jour le 30/11/2017 à 16h35

## SAINT PALAIS SUR MER

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2e8f8b4c41248f2542f5/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h12
- Date de dernière mise à jour le 14/03/2019 à 12h12

## SAINT PAUL LES DAX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6303468b4c414d6ae2ebcd/
- Volumétrie : 45 jeux de données et 131 ressources
- Nombre de vues (tous jeux de données confondus) : 15158
- Nombre de téléchargements (toutes ressources confondues) : 108
- Types de ressources disponibles : `csv`, `xlsx`, `pdf`, `geojson`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/03/2019 à 16h17
- Date de dernière mise à jour le 18/01/2020 à 14h59

## SAINT PERREUX

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Saint-Perreux
- Volumétrie : 2 jeux de données et 267 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 08h12
- Date de dernière mise à jour le 07/04/2021 à 07h50

## SAINT PIERRE DU MONT

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/datanouvelleaquitaine/dataset?organization=commune_de_saint_pierre_du_mont
- Volumétrie : 3 jeux de données

## SAINT PIERRE DU PERRAY

*Commune*

### Source **Plateforme territoriale**
- URL : https://stpdp-grandparissud.opendatasoft.com/explore/?sort=modified
- Volumétrie : 7 jeux de données

## SAINT PRIEST

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Saint-Priest
- Volumétrie : 1 jeux de données

## SAINT QUENTIN

*Commune*

### Source **Plateforme territoriale**
- URL : http://open-data.saint-quentin-numerique.fr/datasets
- Volumétrie : 68 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffaea3a7292c64a780fb
- Volumétrie : 32 jeux de données et 39 ressources
- Nombre de vues (tous jeux de données confondus) : 51
- Nombre de téléchargements (toutes ressources confondues) : 167
- Types de ressources disponibles : `kml`, `kmz`, `zip`, `csv`, `shape`, `shp`, `xls`, `txt`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h45
- Date de dernière mise à jour le 22/10/2015 à 18h15

## SAINT VINCENT SUR OUST

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b90de3c8b4c412516d5511f/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xls`, `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 06/09/2018 à 12h51
- Date de dernière mise à jour le 13/09/2019 à 14h11

## SAINT-APOLLINAIRE

*Commune*
## SAINT-BONNET-EN-CHAMPSAUR

*Commune*
## SAINT-ETIENNE-LE-LAUS

*Commune*
## SAINT-FIRMIN

*Commune*
## SAINT-JEAN-SAINT-NICOLAS

*Commune*
## SAINT-JULIEN-EN-BEAUCHENE

*Commune*
## SAINT-JULIEN-EN-CHAMPSAUR

*Commune*
## SAINT-LAURENT-DU-CROS

*Commune*
## SAINT-LEGER-LES-MELEZES

*Commune*
## SAINT-MARTIN D'ARBEROUE

*Commune*
## SAINT-MAURICE-EN-VALGODEMAR

*Commune*
## SAINT-MICHEL-DE-CHAILLOL

*Commune*
## SAINT-PIERRE-D'ARGENÇON

*Commune*
## SAINT-PONS

*Commune*
## SAINTE ADRESSE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b110a9788ee38208c6bc159/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 01/06/2018 à 11h17
- Date de dernière mise à jour le 11/01/2023 à 08h49

## SAINTE MARIE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b90e0238b4c4126f11c4e06/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 06/09/2018 à 10h33
- Date de dernière mise à jour le 06/09/2018 à 10h34

## SAONE ET LOIRE

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/departement-de-saone-et-loire
- Volumétrie : 14 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff68a3a7292c64a77d8d
- Volumétrie : 65 jeux de données et 223 ressources
- Nombre de vues (tous jeux de données confondus) : 111
- Nombre de téléchargements (toutes ressources confondues) : 287
- Types de ressources disponibles : `csv`, `xlsx`, `xls`, `html`, `txt`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `Other (Attribution)`
- Date de création : le 07/05/2014 à 03h41
- Date de dernière mise à jour le 08/07/2022 à 16h25

## SARLAT LA CANEDA

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffaea3a7292c64a780fe
- Volumétrie : 12 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 49
- Types de ressources disponibles : `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 04h22
- Date de dernière mise à jour le 06/01/2014 à 10h47

## SARRIANS

*Commune*
## SARTHE

*Département*

### Source **OpenDataSoft**
- URL : https://data.sarthe.fr/explore/?refine.publisher=Conseil+D%C3%A9partemental+de+la+Sarthe
- Volumétrie : aucun jeu de données

## SARTHE DEVELOPPEMENT

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.sarthe.fr/explore/?refine.publisher=Sarthe+D%C3%A9veloppement
- Volumétrie : 4 jeux de données et 2768 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8397
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 24/07/2015 à 11h48
- Date de dernière mise à jour le 13/01/2023 à 03h00

## SAUSSAN

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saussan
- Volumétrie : 20 jeux de données

## SAVIGNY LE TEMPLE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.savigny-le-temple.fr/explore/?refine.publisher=Ville+de+Savigny-le-Temple
- Volumétrie : 4 jeux de données et 365 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1380
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 04/10/2018 à 12h01
- Date de dernière mise à jour le 07/02/2020 à 15h24

## SAVOIE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59f843bcc751df5a1b4776b6
- Volumétrie : 12 jeux de données et 54 ressources
- Nombre de vues (tous jeux de données confondus) : 19
- Nombre de téléchargements (toutes ressources confondues) : 31
- Types de ressources disponibles : `pdf`, `url`, `csv`, `kml`, `xml`
- Types de licences utilisées : `Creative Commons Attribution`, `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 17/05/2018 à 10h51
- Date de dernière mise à jour le 16/12/2022 à 13h13

## SCEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?sort=modified&refine.publisher=Ville+de+Sceaux
- Volumétrie : 4 jeux de données et 131 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4022
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 17/06/2019 à 08h23
- Date de dernière mise à jour le 07/06/2022 à 16h35

## SD ENERGIE ET DECHET HAUTE-MARNE

*Organisme associé de collectivité territoriale*
## SDE 04 ALPES HAUTE PROV

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2ad43988ee38214cb97308/
- Volumétrie : aucun jeu de données

## SDE 07 ARDECHE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://opendata.ardeche.fr/search/og_group_ref/105/type/dataset?sort_by=changed
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/583da740c751df06e1c0bb7e/
- Volumétrie : aucun jeu de données

## SDE 18 CHER

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56420fbac751df373faad371/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 09/12/2015 à 09h05
- Date de dernière mise à jour le 11/01/2023 à 08h44

## SDE 24 DORDOGNE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56c586aa88ee38669b585a59/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 18/02/2016 à 14h01
- Date de dernière mise à jour le 11/01/2023 à 08h49

## SDEA

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2005f888ee381180db9edc/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 30/11/2017 à 14h46
- Date de dernière mise à jour le 28/05/2021 à 16h57

## SDEC ENERGIE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.fleurysurorne.fr/explore/?refine.publisher=SDEC
- Volumétrie : 1 jeux de données et 1 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 17/06/2018 à 08h52
- Date de dernière mise à jour le 23/12/2020 à 22h44

## SDEI 36 INDRE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58c11791c751df5a3d517bbb/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 28/11/2022 à 14h55
- Date de dernière mise à jour le 11/01/2023 à 08h49

## SDESM

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/559f83e3c751df2775330fd6/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 20/12/2016 à 10h43
- Date de dernière mise à jour le 11/01/2023 à 08h49

## SDIS 17 CHARENTE MARITIME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://opendata.larochelle.fr/dataset/?metakey=dataset_property&metaval=134
- Volumétrie : 2 jeux de données

## SDIS 21 COTE D'OR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/service-departemental-dincendie-et-de-secours-sdis-21
- Volumétrie : 1 jeux de données

## SDIS 30 GARD

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5877b3b988ee38775fa6f717/
- Volumétrie : 5 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `zip`, `7z`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 19/01/2017 à 11h26
- Date de dernière mise à jour le 19/03/2019 à 14h46

## SDIS 33 GIRONDE

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aa8fccac751df716fffd696/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 15/03/2018 à 14h30
- Date de dernière mise à jour le 20/01/2020 à 08h33

## SDIS 39 JURA

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/service-departemental-dincendie-et-de-secours-du-jura-sdis-39
- Volumétrie : 1 jeux de données

## SDIS 44 LOIRE ATLANTIQUE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Service+Départemental+d%27Incendie+et+de+Secours+de+Loire-Atlantique
- Volumétrie : 4 jeux de données et 48040 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1347
- Types de licences utilisées : `Open Database License-ODBL`, `Open Database License (ODbL)`
- Date de création : le 18/08/2021 à 13h55
- Date de dernière mise à jour le 08/11/2022 à 13h04

## SDIS 49 MAINE ET LOIRE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.opendata49.fr/index.php?id=38
- Volumétrie : 2 jeux de données

## SDIS 58 NIEVRE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/service-departemental-dincendie-et-de-secours-sdis-58
- Volumétrie : 2 jeux de données

## SDIS 67 BAS RHIN

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58738bdc88ee385e8c0bfefe/
- Volumétrie : 3 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `document`, `csv`, `json`, `shp`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 24/01/2020 à 12h58
- Date de dernière mise à jour le 19/01/2022 à 15h17

## SDIS 72 SARTHE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.sarthe.fr/explore/?refine.publisher=SDIS72
- Volumétrie : 1 jeux de données et 74 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1504
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 13/04/2022 à 09h42
- Date de dernière mise à jour le 13/04/2022 à 09h42

## SDIS 81 TARN

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/551959f6c751df6682057c91/
- Volumétrie : 3 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/01/2020 à 13h32
- Date de dernière mise à jour le 06/04/2021 à 10h11

## SDIS 90 BELFORT

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/service-dincendie-et-de-secours-du-territoire-de-belfort-sdis-90
- Volumétrie : 1 jeux de données

## SDSI 71 SAONE ET LOIRE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/service-departemental-dincendie-et-de-secours-sdis-71
- Volumétrie : 2 jeux de données

## SEILH

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Seilh
- Volumétrie : 1 jeux de données et 2 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 7540
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 14/02/2013 à 23h00
- Date de dernière mise à jour le 27/05/2021 à 13h20

## SEINE ET MARNE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c13da1f8b4c41461f8a544c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 17/12/2018 à 10h07
- Date de dernière mise à jour le 17/12/2018 à 10h08

## SEINE MARITIME

*Département*

### Source **Plateforme territoriale**
- URL : https://www.opendata76.fr
- Volumétrie : 131 jeux de données

## SEINE SAINT DENIS

*Département*

### Source **Plateforme territoriale**
- URL : http://data.seine-saint-denis.fr/spip.php?page=data2_rubrique
- Volumétrie : 356 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5457bc26c751df7378225d2e
- Volumétrie : 275 jeux de données et 580 ressources
- Nombre de vues (tous jeux de données confondus) : 2177
- Nombre de téléchargements (toutes ressources confondues) : 821
- Types de ressources disponibles : `document`, `json`, `shp`, `xls`, `csv`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 25/03/2015 à 14h19
- Date de dernière mise à jour le 16/03/2021 à 10h53

## SEM BIBUS Brest

*Délégataire de service public*

### Source **Plateforme territoriale**
- URL : https://opendata.brest-metropole.fr/explore/?sort=modified&refine.publisher=BIBUS
- Volumétrie : 0 jeux de données

## SEM DES TRANSPORTS MONTALBANAIS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab217b188ee387f2fb8d2c9/
- Volumétrie : 3 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `7z`, `gtfs`, `txt`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/03/2018 à 11h44
- Date de dernière mise à jour le 11/10/2022 à 11h43

## SEMITAN

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Semitan
- Volumétrie : 4 jeux de données et 141 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 49817
- Types de licences utilisées : `Licence Ouverte-LO`, `Open Database License-ODBL`
- Date de création : le 04/04/2012 à 06h00
- Date de dernière mise à jour le 13/01/2023 à 03h50

## SENE

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Ville+de+Séné
- Volumétrie : 2 jeux de données et 94 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 64
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/03/2019 à 09h22
- Date de dernière mise à jour le 07/04/2021 à 07h50

## SETE

*Commune*

### Source **Plateforme territoriale**
- URL : http://opendata.agglopole.fr
- Volumétrie : 27 jeux de données

## SETRAM

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5acb71b4c751df234a194bdb/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 29/01/2020 à 12h11
- Date de dernière mise à jour le 04/01/2023 à 12h52

## SEVRES

*Commune*

### Source **OpenDataSoft**
- URL : https://sevres-seineouest.opendatasoft.com/explore/?disjunctive.theme&disjunctive.keyword&sort=modified&refine.publisher=Sèvres
- Volumétrie : 14 jeux de données et 1009 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 61688
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 01/07/2019 à 08h21
- Date de dernière mise à jour le 12/01/2023 à 06h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/549971c8c751df352b04805a
- Volumétrie : 14 jeux de données et 54 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/02/2021 à 17h44
- Date de dernière mise à jour le 15/11/2022 à 06h00

## SI TRANSPORTS URBAINS AGGLO DU CALAISIS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfc16c28b4c4169df72af31/
- Volumétrie : aucun jeu de données

## SIBRA ANNECY

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bd98168634f412ce5b52439/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 05/12/2022 à 17h15
- Date de dernière mise à jour le 11/01/2023 à 15h59

## SICAE OISE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d39a6f16f444171b133d7f4/
- Volumétrie : 3 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `kml`, `7z`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 23/01/2020 à 10h16
- Date de dernière mise à jour le 24/03/2022 à 15h32

## SICECO

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5afda2cbc751df60c8fd4696/
- Volumétrie : aucun jeu de données

## SICTIAM

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/sictiam
- Volumétrie : 22 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58da0f55c751df04af13c109/
- Volumétrie : 24 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 20
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `csv`, `pdf`, `json`, `odt`, `rar`, `ods`, `xlsx`, `xls`, `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/06/2017 à 17h33
- Date de dernière mise à jour le 15/09/2021 à 00h00

## SIEIL 37 INDRE ET LOIRE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58d138dd88ee387c51ab42b2/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2017 à 15h43
- Date de dernière mise à jour le 11/01/2023 à 08h44

## SIEL 42 LOIRE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5880827c88ee382b5f9b81a4/
- Volumétrie : aucun jeu de données

## SIXT SUR AFF

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5baa0e468b4c414416b6f7b2/
- Volumétrie : 34 jeux de données et 106 ressources
- Nombre de vues (tous jeux de données confondus) : 1131
- Nombre de téléchargements (toutes ressources confondues) : 288
- Types de ressources disponibles : `txt`, `pdf`, `csv`, `xlsx`, `xml`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 11/10/2018 à 12h48
- Date de dernière mise à jour le 12/08/2021 à 16h58

## SM STATION DE VALBERG

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e060c5f8b4c414519f7e6ec/
- Volumétrie : 4 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `txt`, `zip`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 27/12/2019 à 15h21
- Date de dernière mise à jour le 29/07/2020 à 14h34

## SM4CC

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be6f4838b4c415550fc0af6/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `gtfs-rt`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 05/12/2018 à 14h57
- Date de dernière mise à jour le 02/01/2023 à 17h30

## SMA OUEST CORNOUAILLE

*Organisme associé de collectivité territoriale*
## SMD CALITOM

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e38023e634f414e2d35b954/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/02/2020 à 12h43
- Date de dernière mise à jour le 03/02/2020 à 12h43

## SMEAU SIDESA

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e42c72d634f41223f26f4b9/
- Volumétrie : 1 jeux de données et 29 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 11/02/2020 à 16h29
- Date de dernière mise à jour le 11/02/2020 à 16h41

## SMED ALPES MARITIMIES

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/syndicat-mixte-d-elimination-des-dechets-menagers-du-moyen-pays-des-alpes-maritimes-smed
- Volumétrie : 1 jeux de données

## SMICA

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.smica.fr/fr/metiers-services/donnees/opendata
- Volumétrie : 0 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5963804188ee3874c56d9456/
- Volumétrie : 64 jeux de données et 63 ressources
- Nombre de vues (tous jeux de données confondus) : 41
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 31/05/2021 à 15h39
- Date de dernière mise à jour le 13/12/2022 à 10h58

## SMICVAL

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.datalocale.fr/dataset?organization=smicval
- Volumétrie : 1 jeux de données

## SMIEEOM VAL DE CHER

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.val2c.fr/explore/?disjunctive.theme&disjunctive.publisher&disjunctive.keyword&sort=modified&refine.publisher=SMIEEOM+Val-de-Cher
- Volumétrie : 5 jeux de données et 404 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 786
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 23/12/2019 à 13h10
- Date de dernière mise à jour le 12/03/2020 à 11h51

## SMN ANJOU NUMERIQUE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.paysdelaloire.fr/explore/?disjunctive.diffuseur&disjunctive.gestionnaire&disjunctive.publisher&source=shared&sort=title&refine.publisher=Syndicat+Mixte+Anjou+Num%C3%A9rique
- Volumétrie : aucun jeu de données

## SMO SUD THD

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/smo-sud-thd
- Volumétrie : 42 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59e60b8cc751df26bd5f4ceb/
- Volumétrie : 45 jeux de données et 84 ressources
- Nombre de vues (tous jeux de données confondus) : 79
- Nombre de téléchargements (toutes ressources confondues) : 10
- Types de ressources disponibles : `zip`, `shp`, `xlsx`, `json`, `image`, `document`
- Types de licences utilisées : `Other (Attribution)`, `Other (Open)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 16/10/2019 à 15h58
- Date de dernière mise à jour le 09/11/2020 à 00h00

## SMO VAL DE LOIRE NUMERIQUE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data.valdeloirenumerique.fr/explore/?sort=explore.popularity_score
- Volumétrie : 3 jeux de données

## SMOYS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b0e6478c751df08a2eb86eb/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 30/05/2018 à 12h10
- Date de dernière mise à jour le 11/01/2023 à 08h44

## SMTC AGGLOMERATION CLERMONTOISE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://opendata.clermontmetropole.eu/search?source=smtc-t2c
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ef49ae03ca058b476bded7d/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 25/06/2020 à 14h48
- Date de dernière mise à jour le 11/01/2023 à 08h50

## SMTC DU GRAND NOUMEA

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e0441b78b4c41242daa4ee7/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/01/2021 à 00h52
- Date de dernière mise à jour le 20/04/2022 à 06h40

## SMTC GRENOBLE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=smtc
- Volumétrie : 22 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/575fc7f0c751df0e1dac31a0/
- Volumétrie : 36 jeux de données et 101 ressources
- Nombre de vues (tous jeux de données confondus) : 134
- Nombre de téléchargements (toutes ressources confondues) : 161
- Types de ressources disponibles : `csv`, `geojson`, `other`, `gbfs`, `gtfs`, `zip`, `json`, `xls`, `ods`, `protobuf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/06/2016 à 15h50
- Date de dernière mise à jour le 16/11/2022 à 01h02

## SMTC OISE MOBILITE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5e874ace3af8a02435889f1b/
- Volumétrie : 8 jeux de données et 150 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 22
- Types de ressources disponibles : `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 06/05/2020 à 16h43
- Date de dernière mise à jour le 22/09/2022 à 17h24

## SMTC PAU BEARN MOBILITE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/pau-bearn-pyrenees-mobilites/
- Volumétrie : 19 jeux de données et 54 ressources
- Nombre de vues (tous jeux de données confondus) : 31
- Nombre de téléchargements (toutes ressources confondues) : 25
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 15/07/2020 à 10h48
- Date de dernière mise à jour le 15/11/2022 à 22h55

## SMTC SIMOUV

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d9c9b688b4c410f098b6b1c/
- Volumétrie : 1 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `gtfs-rt`, `siri`, `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 25/05/2021 à 14h43
- Date de dernière mise à jour le 04/03/2022 à 14h45

## SMTC TOUT'ENBUS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfd2c5e634f411d0ced2709/
- Volumétrie : aucun jeu de données

## SMTL FIBRE 64 PYR. ATL

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://lafibre64-data64.opendatasoft.com/explore/?sort=modified&refine.publisher=La+Fibre+64
- Volumétrie : 3 jeux de données et 3204 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 80
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 06/03/2020 à 23h28
- Date de dernière mise à jour le 22/09/2021 à 06h06

## SOCIETE DES TRANSPORTS URBAINS DE VIERZON

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bcf27078b4c4127749e259d/
- Volumétrie : 1 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `gtfs`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 31/10/2018 à 14h37
- Date de dernière mise à jour le 05/07/2021 à 16h33

## SOISSONS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.ville-soissons.fr/explore/?disjunctive.theme&sort=explore.popularity_score&refine.publisher=Ville+de+Soissons+
- Volumétrie : aucun jeu de données et 135763 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5924

## SOLEA

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Sol%C3%A9a
- Volumétrie : 1 jeux de données et 1 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 11704
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 03/01/2023 à 10h30
- Date de dernière mise à jour le 12/01/2023 à 15h36

## SOLURIS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58247d5888ee380f19c65bb3/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 03/10/2018 à 12h25
- Date de dernière mise à jour le 11/03/2021 à 14h36

## SOMME

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5919a313c751df1c22411168
- Volumétrie : 1 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 29/06/2017 à 16h39
- Date de dernière mise à jour le 28/10/2022 à 14h48

## SOURAIDE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/souraide/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 25/03/2020 à 16h18
- Date de dernière mise à jour le 25/03/2020 à 16h23

## SOYAUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b39e30a88ee38056c569d48/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 02/07/2018 à 11h17
- Date de dernière mise à jour le 02/07/2018 à 11h26

## SUCY EN BRIE

*Commune*

### Source **Plateforme territoriale**
- URL : https://sucy.data4citizen.com/portail?facet.field=[%22organization%22,%22tags%22,%22theme%22,%22features%22]&rows=10&start=0&fq=organization:(sucy)
- Volumétrie : 7 jeux de données

## SUD PROVENCE ALPES COTE D'AZUR

*Région*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/region-sud
- Volumétrie : 69 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa8a3a7292c64a780d2
- Volumétrie : 109 jeux de données et 667 ressources
- Nombre de vues (tous jeux de données confondus) : 661
- Nombre de téléchargements (toutes ressources confondues) : 542
- Types de ressources disponibles : `zip`, `pdf`, `gtfs`, `wfs`, `wms`, `shp`, `kml`, `geojson`, `csv`, `html`, `xlsx`, `ecw`, `xls`, `png`, `gpkg`, `ods`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`, `Creative Commons Attribution`
- Date de création : le 24/05/2018 à 15h54
- Date de dernière mise à jour le 07/01/2023 à 01h12

## SURESNES

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?refine.publisher=Ville+de+Suresnes
- Volumétrie : 25 jeux de données et 37465 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 28619
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 25/01/2018 à 09h04
- Date de dernière mise à jour le 09/08/2022 à 09h46

## SUSSARGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/sussargues
- Volumétrie : 21 jeux de données

## SYBLE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/syble
- Volumétrie : 12 jeux de données

## SYDESL 71 SAONE ET LOIRE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a4e516bc751df60325980d9/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/01/2018 à 16h33
- Date de dernière mise à jour le 28/05/2021 à 16h57

## SYDEV

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54b7c236c751df406d5fa5a2/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 61
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/01/2021 à 10h13
- Date de dernière mise à jour le 11/01/2023 à 08h44

## SYMIELEC VAR

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/symielecvar
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2ada2f88ee382b0adbccf3/
- Volumétrie : aucun jeu de données

## SYNDICAT DES EAUX DU BASSIN DE L'ARDECHE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab3c78ec751df61c594d38b/
- Volumétrie : 2 jeux de données et 16 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `zip`, `7z`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 22/03/2018 à 16h36
- Date de dernière mise à jour le 04/01/2022 à 12h58

## SYNDICAT DES MOBILITES PAYS BASQUE-ADOUR

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b9fe153634f415dcb10db19/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 10
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `xlsx`, `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 29/10/2019 à 10h47
- Date de dernière mise à jour le 09/01/2023 à 19h35

## SYTRAL

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=SYTRAL
- Volumétrie : 20 jeux de données

## TALANT

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3ddb3488ee3819894581db/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/07/2018 à 11h29
- Date de dernière mise à jour le 05/07/2018 à 11h41

## TALENCE

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Ville+de+Talence
- Volumétrie : 20 jeux de données et 16881 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 78786
- Types de licences utilisées : `Licence Ouverte`
- Date de création : le 19/07/2019 à 09h08
- Date de dernière mise à jour le 12/01/2023 à 23h03

## TALENSAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b1e91b3c751df2840ab5ca5/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 27/09/2018 à 11h31
- Date de dernière mise à jour le 04/03/2021 à 11h50

## TARBES

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.ha-py.fr/pages/mairie-de-tarbes/
- Volumétrie : 2 jeux de données

## TARBES LOURDES PYRENEES

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://opendata.ha-py.fr/pages/tarbes-lourdes-pyrenees/
- Volumétrie : 2 jeux de données

## TARN ET GARONNE NUMERIQUE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data.82numerique.fr/
- Volumétrie : 6 jeux de données

## THORIGNE FOUILLARD

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?sort=modified&refine.publisher=Ville+de+Thorigné-Fouillard
- Volumétrie : aucun jeu de données

## TIGEO

*Organisme associé de collectivité territoriale*
## TISSEO

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Tiss%C3%A9o
- Volumétrie : 6 jeux de données et 2112 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 101004
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 09/10/2015 à 10h23
- Date de dernière mise à jour le 11/01/2023 à 08h01

## TORCY

*Commune*

### Source **Plateforme territoriale**
- URL : https://torcy-agglo-pvm.opendata.arcgis.com/
- Volumétrie : 2 jeux de données

## TOULON PROVENCE MEDITERRANEE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.metropoletpm.fr/explore/?sort=title&refine.publisher=M%C3%A9tropole+Toulon+Provence+M%C3%A9diterran%C3%A9e
- Volumétrie : aucun jeu de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b6c128d634f41593028881d/
- Volumétrie : 57 jeux de données et 451 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `web page`, `arcgis geoservices rest api`, `kml`, `csv`, `zip`, `geojson`, `shapefile`, `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 09/08/2018 à 12h25
- Date de dernière mise à jour le 07/01/2023 à 01h11

## TOULOUSE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Toulouse
- Volumétrie : 239 jeux de données et 203802 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 888311
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 31/12/2014 à 23h00
- Date de dernière mise à jour le 13/01/2023 à 03h54


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff8aa3a7292c64a77ecd
- Volumétrie : 49 jeux de données et 49 ressources
- Nombre de vues (tous jeux de données confondus) : 77
- Nombre de téléchargements (toutes ressources confondues) : 284
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h43
- Date de dernière mise à jour le 20/11/2013 à 14h10

## TOULOUSE METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Toulouse+M%C3%A9tropole
- Volumétrie : 251 jeux de données et 5319875 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 986380
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 23/07/2013 à 22h00
- Date de dernière mise à jour le 13/01/2023 à 03h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb3a3a7292c64a78129
- Volumétrie : 765 jeux de données et 3053 ressources
- Nombre de vues (tous jeux de données confondus) : 47378
- Nombre de téléchargements (toutes ressources confondues) : 4565
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `bat`, `dot`, `pdf`, `ods`, `doc`, `ksh`, `odt`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h39
- Date de dernière mise à jour le 11/01/2023 à 08h50

## TOURCOING

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+de+Tourcoing
- Volumétrie : 19 jeux de données et 38171 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3788
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 09/01/2017 à 09h37
- Date de dernière mise à jour le 13/01/2023 à 02h19

## TOURS METROPOLE VAL DE LOIRE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.tours-metropole.fr/explore/?disjunctive.keyword&disjunctive.theme&sort=modified&refine.publisher=Tours+Métropole+Val+de+Loire
- Volumétrie : 74 jeux de données et 174699 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 142159
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 09/10/2017 à 15h55
- Date de dernière mise à jour le 13/01/2023 à 03h55


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cb6d4f78b4c4118e18f37d1/
- Volumétrie : 82 jeux de données et 314 ressources
- Nombre de vues (tous jeux de données confondus) : 162
- Nombre de téléchargements (toutes ressources confondues) : 86
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`
- Date de création : le 29/01/2020 à 11h25
- Date de dernière mise à jour le 11/01/2023 à 01h01

## TRANSDEV LE HAVRE

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cdd6a658b4c4129c70cbc44/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 02/01/2023 à 09h33
- Date de dernière mise à jour le 02/01/2023 à 09h33

## TRANSDEV MONT BLANC BUS

*Délégataire de service public*
## TRANSILIEN

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.sncf.com/explore/?refine.publisher=Transilien
- Volumétrie : aucun jeu de données

## TRANSP'OR CA PAYS DE L'OR

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c0f9f618b4c41745fb43104/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 31/08/2022 à 19h09
- Date de dernière mise à jour le 02/01/2023 à 21h57

## TRANSPORTS EN COMMUN DE L'AGGLOMERATION TROYENNE

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c0932458b4c4175cacf2f77/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/09/2022 à 10h04
- Date de dernière mise à jour le 10/01/2023 à 14h57

## TREGUEUX

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 1 jeux de données

## TRIGNAC

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?disjunctive.publisher&disjunctive.gestionnaire&disjunctive.theme&disjunctive.keyword&disjunctive.license&disjunctive.features&disjunctive.diffuseur&disjunctive.dcat.spatial&source=shared&sort=modified&geonav=world%2Fworld_fr%2Ffr_40_52%2Ffr_60_44%2Ffr_72_244400644&refine.publisher=Trignac
- Volumétrie : 1 jeux de données et 509 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1054
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/01/2020 à 15h56
- Date de dernière mise à jour le 12/01/2023 à 14h11

## TULLINS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c61a7448b4c410deeac953d/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 14/02/2019 à 13h37
- Date de dernière mise à jour le 14/02/2019 à 13h37

## UBAYE-SERRE-PONÇON

*Commune*
## UHART-MIXE

*Commune*
## UREPEL

*Commune*
## USEDA

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/576cfdce88ee386c6bab6512/
- Volumétrie : 2 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 14
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 23/01/2017 à 08h15
- Date de dernière mise à jour le 11/01/2023 à 08h44

## VAL D'ORONAYE

*Commune*
## VAL DE CHER CONTROIS

*Communauté de communes*

### Source **OpenDataSoft**
- URL : https://data.val2c.fr/explore/?disjunctive.theme&disjunctive.publisher&disjunctive.keyword&sort=modified&refine.publisher=Communaut%C3%A9+de+communes+Val+de+Cher+Controis
- Volumétrie : 25 jeux de données et 1957 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3542
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 13/01/2020 à 12h57
- Date de dernière mise à jour le 12/01/2023 à 15h49

## VALLOUISE-PELVOUX

*Commune*
## VANVES

*Commune*

### Source **OpenDataSoft**
- URL : https://vanves-seineouest.opendatasoft.com/explore/?sort=records_count&refine.publisher=Vanves
- Volumétrie : 3 jeux de données et 404 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1148
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 19/04/2019 à 12h21
- Date de dernière mise à jour le 31/12/2022 à 23h10


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54884a24c751df7226a3fc16
- Volumétrie : 3 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/11/2021 à 14h56
- Date de dernière mise à jour le 07/06/2022 à 11h14

## VAR

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-du-var
- Volumétrie : 9 jeux de données

## VARENNES JARCY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c2f5acc634f4124ff9f6ba2/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 16/04/2020 à 12h46
- Date de dernière mise à jour le 05/11/2020 à 16h04

## VAUCLUSE

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-de-vaucluse
- Volumétrie : 21 jeux de données

## VAULX EN VELIN

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Vaulx-en-Velin
- Volumétrie : 6 jeux de données

## VECTALIA PERPIGNAN MEDITERRANEE

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c18fad8634f4130048f1ea1/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 15/06/2022 à 08h08
- Date de dernière mise à jour le 12/12/2022 à 15h07

## VENDARGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/vendargues
- Volumétrie : 27 jeux de données

## VENTEROL

*Commune*
## VIENNE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59bfe09988ee3809d96507cb/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 25/09/2017 à 11h36
- Date de dernière mise à jour le 18/11/2022 à 11h21

## VILLE D'AVRAY

*Commune*

### Source **OpenDataSoft**
- URL : https://villedavray-seineouest.opendatasoft.com/explore/?sort=records_count&refine.publisher=Mairie+de+Ville-d%27Avray
- Volumétrie : 19 jeux de données et 2305 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4658
- Date de création : le 26/07/2019 à 12h31
- Date de dernière mise à jour le 09/01/2023 à 19h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/548b19c6c751df1e6e4120e7
- Volumétrie : 19 jeux de données et 50 ressources
- Nombre de vues (tous jeux de données confondus) : 29
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `csv`, `json`, `geojson`, `shp`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 08/03/2021 à 09h01
- Date de dernière mise à jour le 27/06/2021 à 20h06

## VILLEFRANCHE SUR MER

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/dataset?_organization_limit=0&organization=mairie-de-villefranche-sur-mer
- Volumétrie : 1 jeux de données

## VILLEMOMBLE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffbaa3a7292c64a78162
- Volumétrie : 10 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 19
- Nombre de téléchargements (toutes ressources confondues) : 77
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h58
- Date de dernière mise à jour le 06/01/2014 à 10h48

## VILLENEUVE D'ASCQ

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+de+Villeneuve+d%27Ascq
- Volumétrie : 7 jeux de données et 3384 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 18863
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 21/06/2017 à 13h50
- Date de dernière mise à jour le 22/06/2022 à 14h38

## VILLENEUVE LES MAGUELONE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/villeneuve-les-maguelone
- Volumétrie : 29 jeux de données

## VILLENEUVE LOUBET

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-villeneuve-loubet
- Volumétrie : 2 jeux de données

## VILLEURBANNE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.grandlyon.com/recherche?responsibleParty.organisationName=Ville%20de%20Villeurbanne
- Volumétrie : 3 jeux de données

## VILLIERS LE BEL

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5965d3a888ee3839fc58d95e/
- Volumétrie : 3 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/01/2019 à 12h19
- Date de dernière mise à jour le 29/06/2022 à 08h46

## VINCENNES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bc75fbe8b4c417d0a07c480/
- Volumétrie : aucun jeu de données

## VITALIS

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.grandpoitiers.fr/explore/?refine.publisher=R%C3%A9gie+des+transports+Grand+Poitiers+(VITALIS)
- Volumétrie : 2 jeux de données et 1 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10335
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte (Etalab)`
- Date de création : le 08/10/2019 à 09h06
- Date de dernière mise à jour le 12/01/2023 à 23h00

## WAMBRECHIES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bd875ac634f41794e19695f/
- Volumétrie : 5 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 19
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `pdf`, `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 31/10/2018 à 08h45
- Date de dernière mise à jour le 25/01/2019 à 10h31

## WITTELSHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Wittelsheim
- Volumétrie : 7 jeux de données et 1647 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5362
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`
- Date de création : le 25/10/2017 à 16h50
- Date de dernière mise à jour le 12/09/2019 à 21h48

## YFFINIAC

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 8 jeux de données

## YONNE

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.ternum-bfc.fr/organization/conseil-departemental-de-lyonne
- Volumétrie : 0 jeux de données

## ZILLISHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Commune+de+Zillisheim
- Volumétrie : 3 jeux de données et 205 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2989
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 25/10/2017 à 16h51
- Date de dernière mise à jour le 12/09/2019 à 21h48

