# observatoire-data

<img src="http://www.observatoire-opendata.fr/assets/img/france0.svg" alt="logo" width="400"/>

Ces données ont vocation à informer l'[Observatoire _open data_ des territoires](http://www.observatoire-opendata.fr/) visant à mieux connaître l'avancement de l'ouverture des données publiques en France. Elles sont le fruit du travail de recherche d'[OpenDataFrance](http://www.opendatafrance.net/) avec le soutien technique de [Jailbreak](https://www.jailbreak.paris/).

Tous les fichiers sont générés chaque matin par [les scripts du projet ](https://git.opendatafrance.net/observatoire/observatoire-scripts) et archivés quotidiennement sur ce dépôt.

L'objectif est de constituer un référentiel sur :

- les acteurs de l'_open data_ en France ➡ ️[organizations.csv](/organizations.csv),
- les sites où sont publiés les jeux de données de ces acteurs (hors data.gouv.fr) ➡ [websites.csv](/websites.csv),
- les jeux de données publiés ➡ datasets.csv (en cours d'élaboration).

## Méthodologie

La [méthodologie générale](http://www.observatoire-opendata.fr/methodologie/) de l'Observatoire open data des territoires a été définie par OpenDataFrance en 2018.

En ce qui concerne les données produites pour l'Observatoire, elles ont pour principale source le référencement maintenu par OpenDataFrance (cf. sources ci-dessous) qui resense les numéro SIREN des collectivités territoriales ayant publié au moins un jeu de données. Ces organisations sont réparties en différentes catégories utilisées par OpenDataFrance :

- AGCT : autres groupements de collectivités territoriales
- CA : communauté d'agglomération
- CC : communauté de commune
- COM : commune
- CU : communauté urbaine
- DEP : département
- DSPT : délégataire de service public territorial
- EPT : établissement public territorial
- MET : métropole
- OACT : organisme associé de collectivité territoriale
- REG : région
- SDE : service déconcentré de l'État

## Organisation du projet

### Référentiels utilisés

API Sirene, ADMIN-EXPRESS-COG, COG et données de l'Institut Paris Région, [détail documenté dans le dépôt `observatoire-scripts`](https://git.opendatafrance.net/observatoire/observatoire-scripts/-/tree/master/rsc).

### Données sources

Téléchargées et traitées quotidiennement

Dossier `sources`

- `opendatafrance` : [tableur GoogleSheet](https://docs.google.com/spreadsheets/d/1iNHCRV4mfu4P0-VeiMFV6VVJvqQS8yf0sHNwkVcmGiQ/edit) maintenu par [OpenDataFrance](http://www.opendatafrance.net/)
  - organisations.csv : export CSV de la feuille `Copie Organisations`
  - plateformes.csv : export CSV de la feuille `Copie Plateformes`
- `datagouv` : dumps CSV issus de la plateforme [data.gouv.fr](https://www.data.gouv.fr/fr/) filtrés sur les organisations recensées par OpenDataFrance :
  - organizations.csv
  - datasets.csv
  - resources.csv
- `opendatasoft` : dumps CSV issus des plateformes OpenDataSoft référencées par OpenDataFrance :
  - ods_catalog.csv
  - ods_monitoring.csv

### Données produites

Dossier `exports`

- `geojson` : export GeoJSON des organisations par type pour affichage cartographique
- `markdown` : détail des organisations

À la racine :

- `organizations.csv` : fichier des organisations avec une catégorisation par région et département
- `websites.csv` : fichier des plateformes limité aux seules colonnes pertinentes
- `datasets-datagouv.csv` : fichier des jeux de données de data.gouv.fr indexés par siren d'organisations
